<?php

class DefaultController extends Controller
{
    public $prev;
    public $debug = false;

    public function actionIndex()
    {
        $date_helper = new date_helper();
        $dateRange = $date_helper->createDateRangeArray();

        //Get a list of the days for which calls have been entered by this user.
        $daysWithCalls = array();
        foreach ($dateRange as $date) {
            $dates = Timesheets::model()->findAll('date=:date AND user_id=:user_id',
                array(':date' => $date, ':user_id' => Yii::app()->user->id));
            //die(var_dump($dates));
            foreach ($dates as $call) {
                array_push($daysWithCalls, $call['date']);
            }
        }
        //Now we have an array holding all the dates of calls entered by the user for the given month.
        //Now we need to make the dates unique.
        $daysWithCalls = array_unique($daysWithCalls);
        //Now we have an array of only unique days that have calls entered for.
        //Now to attach an array of the calls for each day to the day itself.
        foreach ($daysWithCalls as $day) {
            $fltarray = array('user_id' => Yii::app()->user->id, 'date' => $day);
            $conditionArr = array('order' => 'start_time ASC');
            $calls = Timesheets::model()->findAllByAttributes($fltarray, $conditionArr);
            $days[$day] = $calls;
        }
        //Now we have an array carrying only the calls for the days the user has entered
        //We can generate the mileage and store it or if a record exists then we update instead
        /*if (isset($days)) {
            foreach ($days as $day) {
                $from_array = array();
                $to_array = array();
                foreach ($day as $call) {
                    array_push($from_array, $call);
                    array_push($to_array, $call);
                }
                array_shift($to_array); // Remove first element of the to_array.
                array_pop($from_array); // Remove last element of the from_array.
*/
        /**
         * New Calc Method Start
         */
        //Now farmed to helper to reports and mileage both sync and changes only need making to helper.
        $calc = new mileage_calculator();
        $calc->calculateMileage($days);

        /**
         * New Calc Method End
         */
        /*
                        //So now we create a single array using all the data we need to do the referencing.
                        $mileage_call_data_array = array();
                        for ($i = 0; $i < count($to_array); $i++) {
                            $data = array();
                            $data['from'] = $from_array[$i]->client_id;
                            $data['to'] = $to_array[$i]->client_id;
                            $data['user_id'] = $to_array[$i]->user_id;
                            $data['call_ref'] = $to_array[$i]->id;
                            $data['mileage'] = 0.0;
                            $data['date'] = $to_array[$i]->date;
                            $mileage_call_data_array[$i] = $data;
                            unset($data);
                            //die(var_dump($mileage_call_data_array));
                            //Now perform mileage lookup.
                            $C2cMileageRef_model = Call_To_Call_Mileage_Reference::model()->findByAttributes
                                (array('from_client_id' => $mileage_call_data_array[$i]['from'],
                                    'to_client_id' => $mileage_call_data_array[$i]['to']));
                            if ($C2cMileageRef_model == null) {
                                $mileage_call_data_array[$i]['mileage'] = 0.0;
                            } else {
                                $mileage_call_data_array[$i]['mileage'] = $C2cMileageRef_model->mileage;
                            }

                            $Mileage_model = Mileage::model()->findByAttributes
                                (array('user_id' => $mileage_call_data_array[$i]['user_id'],
                                    'call_ref' => $mileage_call_data_array[$i]['call_ref']));

                            if ($Mileage_model == null) {
                                //Insert
                                $model = new Mileage();
                                $model->user_id = $mileage_call_data_array[$i]['user_id'];
                                $model->call_ref = $mileage_call_data_array[$i]['call_ref'];
                                $model->c2c_miles = $mileage_call_data_array[$i]['mileage'];
                                $model->date = $mileage_call_data_array[$i]['date'];
                                $model->save();
                            } else {
                                //Update
                                $Mileage_model->c2c_miles = $mileage_call_data_array[$i]['mileage'];
                                $Mileage_model->date = $mileage_call_data_array[$i]['date'];
                                $Mileage_model->save();
                            }
                        }
                    }
                }
                unset($from_array, $to_array);
        */
        $data = array();
        //Right, Now we need to pull all mileage for the dates we have and total C2C and SS for initial display.
        foreach ($daysWithCalls as $day) {
            //fetch all mileage for this day, for this user, and total the C2C miles and SS miles
            //and store them.
            $callMileage = Mileage::model()->findAll('date=:date AND user_id=:user_id',
                array(':date' => $day, ':user_id' => Yii::app()->user->id));
            //$callMileage now contains all related mileage to this day for this user,
            //now to total them and add all to the $data array.
            $c2c = 0.0;
            foreach ($callMileage as $entry) {
                $c2c += $entry['c2c_miles'];
            }
            array_push($data, array('date' => $day, 'c2c' => $c2c));
        }
        //Now $data contains an array of arrays with our data in and we can pass it to view
        $this->render('index', array('days' => $data));
    }


    public function actionBackOneMonth()
    {
        if (isset(Yii::app()->session['month_offset'])) {
            Yii::app()->session['month_offset'] = Yii::app()->session['month_offset'] - 1;
            if (Yii::app()->session['month_offset'] < -6) {
                Yii::app()->session['month_offset'] = -6;
            }
            $this->redirect(array('index'));
        }
    }

    public function actionForwardOneMonth()
    {
        if (isset(Yii::app()->session['month_offset'])) {
            Yii::app()->session['month_offset'] = Yii::app()->session['month_offset'] + 1;
            if (Yii::app()->session['month_offset'] > 0) {
                Yii::app()->session['month_offset'] = 0;
            }
            $this->redirect(array('index'));
        }
    }

    public function actionZero($call_ref, $date)
    {
        if ($call_ref != null) {
            $c2c = C2cExtraMileage::model()->findByAttributes(array('call_ref' => $call_ref));
            if ($c2c != null)
                $c2c->delete();
            $ss = SocialSupportMileage::model()->findByAttributes(array('call_ref' => $call_ref));
            if ($ss != null)
                $ss->delete();
        }
        $this->redirect(array('mileage/calls', 'date' => $date, array('menu' => $this->menu)));
    }
}