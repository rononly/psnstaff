<?php
/* @var $this DefaultController */
/* @var $model AdminNews */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'adminnews-form',
        'enableAjaxValidation'=>false,
    )); ?>

    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->hiddenField($model, 'id'); ?>
    <?php echo $form->hiddenField($model, 'date'); ?>
    <table class="centre" style="width: 600px;">
        <tr class="border_bottom">
            <td><?php echo $form->labelEx($model,'html'); ?></td>
        </tr>
        <tr>
            <td><?php $this->widget('ext.editMe.widgets.ExtEditMe', array(
                'model'=>$model,
                'attribute'=>'html',
                'width'=> 600,
                'height'=> 270,
            ));?></td>
        </tr>

        <tr class="border_bottom">
            <td><?php echo $form->error($model,'html'); ?></td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Add News' : 'Update News'); ?>
            </td>
        </tr>
    </table>

    <?php $this->endWidget(); ?>

</div><!-- form -->