<?php
/* @var $this MileageController */
/* @var $data Mileage */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('call_ref')); ?>:</b>
	<?php echo CHtml::encode($data->call_ref); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_initials_1')); ?>:</b>
	<?php echo CHtml::encode($data->client_initials_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_postcode_1')); ?>:</b>
	<?php echo CHtml::encode($data->client_postcode_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_initials_2')); ?>:</b>
	<?php echo CHtml::encode($data->client_initials_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('client_postcode_2')); ?>:</b>
	<?php echo CHtml::encode($data->client_postcode_2); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('c2c_miles')); ?>:</b>
	<?php echo CHtml::encode($data->c2c_miles); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ss_miles')); ?>:</b>
	<?php echo CHtml::encode($data->ss_miles); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ss_description')); ?>:</b>
	<?php echo CHtml::encode($data->ss_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approved')); ?>:</b>
	<?php echo CHtml::encode($data->approved); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('approved_by')); ?>:</b>
	<?php echo CHtml::encode($data->approved_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	*/ ?>

</div>