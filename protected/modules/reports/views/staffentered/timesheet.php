<?php
/* @var $this StaffenteredController */

$this->breadcrumbs=array(
	'Staff-Entered'=>array('/reports/staffentered'),
	'Time Sheet Report',
);
?>
<?php $this->renderPartial('_time_sheet_form', array('model' => $model)); ?>
