<?php
/* @var $this StaffenteredController */

$this->breadcrumbs=array(
    'Staff-Entered'=>array('/reports/staffentered'),
    'Entry Report',
);
?>
<?php $this->renderPartial('_unsubmit_form', array('model' => $model)); ?>