<?php

class ExtraController extends Controller
{
    public function actionCreateExtra()
    {
        $model=new C2cExtraMileage;
        if(isset($_GET['call_ref']))
        {
            $model->call_ref = $_GET['call_ref'];
        }
        else
        {
            $this->redirect(array('mileage/default/index'));
        }

        // uncomment the following code to enable ajax-based validation
        /*
        if(isset($_POST['ajax']) && $_POST['ajax']==='c2c-extra-mileage-_form-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        */

        if(isset($_POST['C2cExtraMileage']))
        {
            $model->attributes=$_POST['C2cExtraMileage'];
            if($model->validate())
            {
                $Timesheet = Timesheets::model()->findByPk($model->call_ref);
                $model->date = $Timesheet->date;
                $model->added = date('Y-m-d H:i:s', time());
                $model->save();

                $this->redirect(array('mileage/calls', 'date' => $Timesheet->date));
            }
        }
        $Timesheet = Timesheets::model()->findByPk($model->call_ref);
        $this->render('CreateExtra',array('model'=>$model, 'timesheet'=>$Timesheet));
    }
    public function fixMissingDates()
    {
        $models = C2cExtraMileage::model()->findAll();
        foreach($models as &$model)
        {
            if($model->date === NULL)
            {
                $call = Timesheets::model()->findByPk($model->call_ref);
                $model->date = $call->date;
            }
        }
        foreach($models as $model)
        {
            $model->save();
        }
    }
    public function actionUpdateExtra()
    {
        $model = new C2cExtraMileage();
        if(isset($_GET['id']))
        {
            $model = C2cExtraMileage::model()->findByPk($_GET['id']);
            $Timesheet = Timesheets::model()->findByPk($model->call_ref);
        }
        else
        {
            $this->redirect(array('mileage/default/index'));
        }
        if(isset($_POST['C2cExtraMileage']))
        {
            $model->attributes = $_POST['C2cExtraMileage'];
            $model->updated = date('Y-m-d H:i:s', time());
            if($model->validate())
            {
                if(!$model->save())
                {
                    throw new Exception('Unable to save details');
                }
                $this->redirect(array('mileage/calls', 'date' => $Timesheet->date));
            }
        }
        $this->render('UpdateExtra',array('model'=>$model, 'timesheet'=>$Timesheet));
    }


    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}