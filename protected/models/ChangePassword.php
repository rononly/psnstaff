<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ChangePassword
 *
 * @author Ron
 */
class ChangePassword extends CFormModel
{

    public $password;
    public $verifyPassword;
    public $currentPassword;
    public $storedPassword;

    public function rules()
    {
        $myAccount = new myaccount_helpers();
        $pass = $myAccount->getPassword(Yii::app()->user->id);

        return array(
            array('currentPassword', 'safe'),
            array('currentPassword, password, verifyPassword', 'required'),
            array('currentPassword', 'passwordMatch', 'match' => $pass),
            array('verifyPassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Retyped password is incorrect'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'password' => 'New password',
            'verifyPassword' => 'Confirm new password',
            'currentPassword' => 'Your current password',
        );
    }


    public function passwordMatch($attribute, $params)
    {

        $bcrypt = new bCrypt();
        if (!$bcrypt->verify($this->$attribute, $params['match']))
            $this->addError($attribute, 'You have entered an incorrect current password!');
    }

}
?>
