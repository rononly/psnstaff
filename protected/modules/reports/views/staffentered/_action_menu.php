<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 02/01/14
 * Time: 01:13
 */
?>
<style>
    .padout {

        text-align: center;
    }
</style>
<p class="padout">
<?php
echo CHtml::Button('Email Report',
    array('submit'=>$this->createUrl('Staffentered/EmailReport')));

echo CHtml::Button('Print Report',
    array('submit'=>$this->createUrl('Staffentered/PrintReport')));

echo CHtml::Button('Download Report',
    array('submit'=>$this->createUrl('Staffentered/DownloadReport')));

echo CHtml::Button('Download Csv File',
    array('submit'=>$this->createUrl('Staffentered/DownloadCsvFile')));
?>
</p>