<?php
/* @var $this ReportsController */

$this->breadcrumbs=array(
	'Reports'=>array('/administration/reports'),
	'ByDateRange',
);

//we need to display a date range selector then the timesheets for carers that have sed the system

?>
<?php echo $this->renderPartial('reports/_form_date_range', array('model' => $model)); ?>