<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 25/08/13
 * Time: 12:03
 * To change this template use File | Settings | File Templates.
 */

class EmailReport extends CFormModel {

    public $email_to;
    public $submit_calls;


    public function rules()
    {
        return array (
            array('email_to', 'required'),
            array('email_to', 'email'),
            array('submit_calls', 'yes_no')
        );
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'email_to' => 'Email Address',
            'submit_calls' => 'Submit Calls',
        );
    }
    public function yes_no($attribute)
    {
        if($this->$attribute === 'Y')
        {

        }
        if($this->$attribute === 'N')
        {

        }
    }
}