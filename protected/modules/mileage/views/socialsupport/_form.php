<?php
/* @var $this SocialSupportController */
/* @var $model SocialSupportMileage */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'social-support-mileage-_form-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>


    <table class="centre" border="1" style="width: 600px; padding-top: 10px;"><tbody>
    <tr>
        <td>Fields with <span class="required">*</span> are required.</td>
    </tr>
    <tr>
        <td>Tell us where you went, what you did.</td>
    </tr>
    <tr>
        <td><?php echo $form->errorSummary($model); ?></td>
    </tr>
    <tr>
		<td style="width: 100px; text-align: left;">
        <?php echo $form->labelEx($model,'mileage'); ?>
		<?php echo $form->textField($model,'mileage', array('size' => 5)); ?>
		<?php echo $form->error($model,'mileage'); ?>
        </td>
    <tr>
        <td>
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textArea($model,'description', array('rows'=>6,'cols'=>50)); ?>
		<?php echo $form->error($model,'description'); ?>
        </td>
</tr><tr>
        <td>
            <?php echo CHtml::submitButton('Save'); ?>
        </td>
    </tr>
    </tbody>
    </table>





<?php $this->endWidget(); ?>

</div><!-- form -->