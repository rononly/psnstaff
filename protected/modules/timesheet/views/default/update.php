<?php
/* @var $this TimesheetsController */
/* @var $model Timesheets */
/* @var $idents Client Ids for dropdown */

$this->breadcrumbs = array(
    'Time Sheets' => array('selecttimesheetday'),
    'View By Day' => array('selecttimesheetday'),
    'Update',
);
$formatter = new formatDateFor_helper();
?>

<h3><?php echo $formatter->Viewing($date) . '    (' . date("l jS F Y", strtotime($date)) . ')' ?></h3>
<?php echo $this->renderPartial('_day_form', array('model' => $model, 'date' => $date, 'idents' => $idents)); ?>