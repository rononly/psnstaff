<?php

/**
 * This is the model class for table "client".
 *
 * The followings are the available columns in table 'client':
 * @property integer $id
 * @property string $initials
 * @property string $surname
 * @property integer $property_number
 * @property string $postcode
 * @property integer $active
 * @property string $added
 * @property string $updated
 */
class Client extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'client';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('initials, surname, postcode, added', 'required'),
			array('property_number, active', 'numerical', 'integerOnly'=>true),
			array('initials', 'length', 'max'=>3),
			array('surname', 'length', 'max'=>30),
			array('postcode', 'length', 'max'=>7),
			array('updated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, initials, surname, property_number, postcode, active, added, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'initials' => 'Initials',
			'surname' => 'Surname',
			'property_number' => 'Property Number',
			'postcode' => 'Postcode',
			'active' => 'Active',
			'added' => 'Added',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('initials',$this->initials,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('property_number',$this->property_number);
		$criteria->compare('postcode',$this->postcode,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('added',$this->added,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}