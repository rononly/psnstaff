<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of getWeeksCommencingForUser_helper
 *
 * @author Ron
 */
class getWeeksCommencingForUser_helper
{
    public function getWeeksCommencingForUser()
    {
        $formatDate = new formatDateFor_helper();
        $weeks_commencingForUser = Yii::app()->db->createCommand()
            ->select('week_commencing')
            ->from('timesheets')
            ->where('user_id =:user_id', array(':user_id' => Yii::app()->user->id))
            ->order('week_commencing ASC')
            ->queryAll();
        //Now we must ensure that there is only one entry for each week commencing
        foreach ($weeks_commencingForUser as &$week) {
            $week['week_commencing'] =
                $formatDate->Viewing($week['week_commencing']);
        }
        return $weeks_commencingForUser;
    }
}