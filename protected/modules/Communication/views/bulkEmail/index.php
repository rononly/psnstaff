<?php
/* @var $this BulkEmailController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bulk Emails',
);

$this->menu=array(
	array('label'=>'Create BulkEmail', 'url'=>array('create')),
	array('label'=>'Manage BulkEmail', 'url'=>array('admin')),
);
?>

<h1>Bulk Emails</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
