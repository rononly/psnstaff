<?php

class SocialsupportController extends Controller
{

    public function actionCreateSocialSupport()
    {
        $model=new SocialSupportMileage();
        if(isset($_GET['call_ref']))
        {
            $model->call_ref = $_GET['call_ref'];
        }
        else
        {
            $this->redirect(array('mileage/default/index'));
        }

        // uncomment the following code to enable ajax-based validation
        /*
        if(isset($_POST['ajax']) && $_POST['ajax']==='c2c-extra-mileage-_form-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        */

        if(isset($_POST['SocialSupportMileage']))
        {
            $model->attributes=$_POST['SocialSupportMileage'];
            if($model->validate())
            {
                $Timesheet = Timesheets::model()->findByPk($model->call_ref);
                $model->date = $Timesheet->date;
                $model->added = date('Y-m-d H:i:s', time());
                $model->save();
                $this->redirect(array('mileage/calls', 'date' => $Timesheet->date));
            }
        }
        $Timesheet = Timesheets::model()->findByPk($model->call_ref);
        $this->render('CreateSocialSupport',array('model'=>$model, 'timesheet'=>$Timesheet));
    }

    public function actionUpdateSocialSupport()
    {
        $model = new SocialSupportMileage();
        if(isset($_GET['id']))
        {
            $model = SocialSupportMileage::model()->findByPk($_GET['id']);
            $Timesheet = Timesheets::model()->findByPk($model->call_ref);
        }
        else
        {
            $this->redirect(array('mileage/default/index'));
        }
        if(isset($_POST['SocialSupportMileage']))
        {
            $model->attributes = $_POST['SocialSupportMileage'];
            $model->updated = date('Y-m-d H:i:s', time());
            if($model->validate())
            {
                if(!$model->save())
                {
                    throw new Exception('Unable to save details');
                }
                $this->redirect(array('mileage/calls', 'date' => $Timesheet->date));
            }
        }
        $this->render('UpdateSocialSupport',array('model'=>$model, 'timesheet'=>$Timesheet));
    }

    public function fixMissingDates()
    {
        $models = SocialSupportMileage::model()->findAll();
        foreach($models as &$model)
        {
            if($model->date === NULL)
            {
                $call = Timesheets::model()->findByPk($model->call_ref);
                $model->date = $call->date;
            }
        }
        foreach($models as $model)
        {
            $model->save();
        }
    }
    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}