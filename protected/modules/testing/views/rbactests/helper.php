<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 22/04/14
 * Time: 19:10
 */
?>
<h1>RBAC Tests</h1>
<h3>RBAC_helper Tests</h3>
<h4>Is Tests</h4>
<?php
$rh = new RBAC_helper();
echo '<p>Authority: '.var_export($rh->is('Authority')).'</p>';
echo '<p>Administrator: '.var_export($rh->is('Administrator')).'</p>';
echo '<p>Manager: '.var_export($rh->is('Manager')).'</p>';
echo '<p>Team Leader: '.var_export($rh->is('Team Leader')).'</p>';
echo '<p>Coordinator: '.var_export($rh->is('Coordinator')).'</p>';
echo '<p>Office Admin: '.var_export($rh->is('Office Admin')).'</p>';
echo '<p>Activities Organizer: '.var_export($rh->is('Activities Organizer')).'</p>';
echo '<p>Support Worker: '.var_export($rh->is('Support Worker')).'</p>';