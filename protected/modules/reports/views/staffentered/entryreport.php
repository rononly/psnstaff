<?php
/* @var $this StaffenteredController */

$this->breadcrumbs=array(
    'Staff-Entered'=>array('/reports/staffentered'),
    'Entry Report',
);
?>
<?php $this->renderPartial('_entry_report_form', array('model' => $model, 'entries'=>$entries)); ?>