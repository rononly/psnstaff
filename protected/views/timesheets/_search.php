<?php
/* @var $this TimesheetsController */
/* @var $model Timesheets */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'week_commencing'); ?>
		<?php echo $form->textField($model,'week_commencing'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'client_initials'); ?>
		<?php echo $form->textField($model,'client_initials',array('size'=>3,'maxlength'=>3)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'client_postcode'); ?>
		<?php echo $form->textField($model,'client_postcode',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'call_type_id'); ?>
		<?php echo $form->textField($model,'call_type_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'minutes'); ?>
		<?php echo $form->textField($model,'minutes'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->