<?php
/* @var $this JobAssignmentsController */
/* @var $model JobAssignments */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'job-assignments-form',
        'enableAjaxValidation' => false,
    ));
    $getUsername = new getUserName_helper();
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->hiddenField($model, 'user_id', array('value' => $getUsername->lookUpID(Yii::app()->session['relevant_user']))); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'job_role_id'); ?>
        <?php echo $form->textField($model, 'job_role_id'); ?>
        <?php echo $form->error($model, 'job_role_id'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->