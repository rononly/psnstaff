<?php
/* @var $this TimesheetsController */
/* @var $model Timesheets */
/* @var $form CActiveForm */
//Helpers
$date_helper = new date_helper();
$getCallTypes = new getCallTypes_helper();
?>
<style type="text/css" media="screen">
    table.round {
        -moz-border-radius: 15px;
        border-radius: 15px;
        background-color: #c9e0ed;
        padding-left: 35px;
        padding-top: 10px;
        padding-bottom: 10px;
        -moz-box-shadow: 10px 10px 5px #888;
        -webkit-box-shadow: 10px 10px 5px #888;
        box-shadow: 10px 10px 5px #888;
    }
    div.round {
        -moz-border-radius: 15px;
        border-radius: 15px;
        background-color: #c9e0ed;
        padding-left: 17px;
        padding-top: 10px;
        padding-bottom: 10px;
        vertical-align: bottom;
        -moz-box-shadow: 10px 10px 5px #888;
        -webkit-box-shadow: 10px 10px 5px #888;
        box-shadow: 10px 10px 5px #888;
    }
</style>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'timesheets-form',
        'enableAjaxValidation' => false,
    ));
    ?>


    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <?php echo $form->errorSummary($model); ?>
    <table class="round" border="1" style="width: 420px;">
        <tbody>
        <tr>
            <?php echo $form->hiddenField($model,'date',array('value' => $date)); ?>
            <?php echo $form->hiddenField($model,'user_id',array('value' => Yii::app()->user->id)); ?>

            <td width="30px">
                <?php echo $form->labelEx($model, 'client_initials'); ?>
                <?php echo $form->textField($model, 'client_initials', array('size' => 3, 'maxlength' => 3)); ?>
                <?php echo $form->error($model, 'client_initials'); ?></td>

            <td width="100px">
                <?php echo $form->labelEx($model, 'client_postcode'); ?>
                <?php echo $form->textField($model, 'client_postcode', array('size' => 8, 'maxlength' => 8)); ?>
                <?php echo $form->error($model, 'client_postcode'); ?></td>
            <td>
                <?php echo $form->labelEx($model, 'call_type_id'); ?>
                <?php echo $form->dropDownList($model, 'call_type_id', CHtml::listData($getCallTypes->getCallTypes(), 'id', 'call_type'));
                ?>
                <?php echo $form->error($model, 'call_type_id'); ?></td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'hours'); ?>
                <?php
                echo $form->dropDownList($model, 'hours', array('0' => 'No Hours', '60' => '1 Hour', '120' => '2 Hours',
                    '180' => '3 Hours', '240' => '4 Hours', '300' => '5 Hours', '360' => '6 Hours', '420' => '7 Hours',
                    '480' => '8 Hours', '540' => '9 Hours', '600' => '10 Hours', '660' => '11 Hours', '720' => '12 Hours',
                    '780' => '13 Hours', '840' => '14 Hours', '900' => '15 Hours', '960' => '16 Hours',
                    '1020' => '17 Hours', '1080' => '18 Hours', '1140' => '19 Hours', '1200' => '20 Hours',
                    '1260' => '21 Hours', '1320' => '22 Hours', '1380' => '23 Hours', '1440' => '24 Hours'));

                ?>
                <?php echo $form->error($model, 'hours'); ?>

            </td>
            <td>
                <?php echo $form->labelEx($model, 'minutes'); ?>
                <?php
                echo $form->dropDownList($model, 'minutes', array('0' => 'No Minutes', '15' => '15 Minutes', '20' => '20 Minutes',
                    '30' => '30 Minutes', '45' => '45 Minutes'));
                ?>
                <?php echo $form->error($model, 'minutes'); ?></td>
            <td style="vertical-align: bottom">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Add Entry To Sheet' : 'Save'); ?></td>
        </tr>
        </tbody>
    </table>

    <?php $this->endWidget(); ?>

</div><!-- form -->

    <?php
    $select = new timesheet_helpers();
    $data = $select->getDisplaySelectByCall($date, Yii::app()->user->id);
    $formatter = new formatDateFor_helper();
    $callTypeLU = new getCallTypes_helper();

    $editImage = CHtml::image('images/live_journal_sml.png', 'Edit This Call');
    $deleteImage = CHtml::image('images/remove.png', 'Delete Call');

if(!$data === null)
{
    ?>
    <div class="round" id="Sheet_Display">
        <?php
}


    //die(var_dump($data));
    if ($data == null) {
        if (strpos(Yii::app()->request->urlReferrer, 'timesheets/update') !== false) {
            $this->redirect(array('/timesheets/selecttimesheetday'));
        }
    } else {
        echo '</br></br>';
        echo '<table border="1" cellpadding="1" cellspacing="1" style="width: 600px; padding-top: 10px;"><tbody>';
        echo '<tr><td>Date</td><td>Initials + Postcode<td>Call Type</td><td>
                        Hours</td><td>Total</td><td>Action</td></tr>';
        $totalHours = 0;
        foreach ($data as $call) {
            echo '<tr>';
            echo '<td>' . CHtml::link($formatter->Viewing($call['date']), array('timesheets/update',
                'id' => $call['id'], 'date' => $call['date'])) . '</td>';
            echo '<td>' . $call['client_initials'] . ' ' . $call['client_postcode'] . '</td>';
            echo '<td>' . $callTypeLU->lookUp($call['call_type_id']) . '</td>';
            echo '<td>' . ($call['minutes'] / 60) . '</td>';
            $totalHours += $call['minutes'];
            echo '<td>' . $totalHours / 60 . '</td>';
            $editLink = CHtml::link($editImage, array('timesheets/update',
                'id' => $call['id'], 'date' => $date));
            $deleteLink = CHtml::link($deleteImage, '#',
                array("submit" => array('delete', 'id' => $call['id'], 'date' => $date), 'confirm' => 'Are you sure?'));
            echo '<td>' . $editLink . $deleteLink . '</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
    if(!$data === null)
    {
        ?>
    </div>
<?php
    }
?>