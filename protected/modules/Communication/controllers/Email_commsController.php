<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 20/08/13
 * Time: 16:13
 * To change this template use File | Settings | File Templates.
 */

class Email_commsController extends Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionBulk()
    {
        $model = new BulkEmail();


        $this->render('bulk', array('model' => $model));
    }

    public function actionCreateBulk()
    {
        if (isset($_GET['id'])) {
            $this->loadModel($_GET['id']);
        } else {

            $model = new BulkEmail();
        }
        if (isset($_POST['BulkEmail'])) {
            $model->attributes = $_POST['BulkEmail'];
            if ($model->validate()) {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success', "Data1 saved!");
                    $this->redirect(array('createbulk', 'id' => $model->id));
                }
            }
        }
        $this->render('bulkemail', array('model' => $model));
    }

    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
}