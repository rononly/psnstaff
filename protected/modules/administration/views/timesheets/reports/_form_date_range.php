<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 22/08/13
 * Time: 12:33
 * To change this template use File | Settings | File Templates.
 */

$format_date = new formatDateFor_helper();
$date_helper = new date_helper();
$date_range = $date_helper->createDateRangeArray();
foreach($date_range as &$date)
{
    $date = $format_date->Viewing($date);
}
unset($date);
?>
<style media="screen" type="text/css">
    tr.border_bottom td {
        border-bottom: 2pt solid #e5eCf9;
        text-align: center;
    }
    tr.topbar td {
        text-align: center;
        font-family: Roboto;
        font-size: 16px;
        font-weight: bold;
        width: 80px;
    }
    tr.forms td {
        text-align: center;
        font-family: Roboto;
    }

    .centered
    {
        text-align:center;
    }
    .right
    {
        text-align:right;
    }

    tr.highlight td {
        background-color: lightgreen;
        border-bottom: 2pt solid #e5eCf9;
        text-align: center;
    }
    tr.header td {
        font-size: larger;
        font-weight: bold;
        border-bottom: 2pt solid #e5eCf9;
        text-align: center;
    }
    table.centre {
        margin-left: auto;
        margin-right: auto;
        -moz-border-radius: 15px;
        border-radius: 15px;
        background-color: #c9e0ed;
        padding-left: 20px;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-right: 0px;
        -moz-box-shadow: 10px 10px 5px #888;
        -webkit-box-shadow: 10px 10px 5px #888;
        box-shadow: 10px 10px 5px #888;
    }
</style>



    <table class="centre" border="1" cellpadding="1" cellspacing="1" style="width: 550px;">
        <tbody>
        <tr class="topbar">
            <td>Back a Month</td>
            <td>Select Dates</td>
            <td>Forward a Month</td>
        </tr>
        <tr>
            <td class="centered"><form action="index.php?r=administration/timesheets/backonemonth" method="post">
                    <?php echo CHtml::imageButton('images/arrow-left.png'); ?>
                </form></td>
            <td class="centered">
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'daterange-form',
                        'enableAjaxValidation' => false,
                    ));
                    ?>

                    <?php echo $form->errorSummary($model); ?>
                    <table border="1" cellpadding="1" cellspacing="1" style="margin-left: auto; margin-right: auto;">
                        <tbody>
                        <tr>
                            <td class="centered">
                                <?php echo $form->labelEx($model, 'start'); ?>
                                <?php echo $form->dropDownList($model, 'start', $date_range);
                                ?>
                                <?php echo $form->error($model, 'start'); ?></td>
                        </tr>
                        <tr>
                            <td class="centered">
                                <?php echo $form->labelEx($model, 'end'); ?>
                                <?php echo $form->dropDownList($model, 'end', $date_range);
                                ?>
                                <?php echo $form->error($model, 'end'); ?></td>
                        </tr>
                        <tr>
                            <td class="centered">
                                <?php echo CHtml::submitButton(' Show Calls '); ?></td>
                        </tr>
                        </tbody>
                    </table>

                    <?php $this->endWidget(); ?>
            </td>
            <td class="centered"><form action="index.php?r=administration/timesheets/forwardonemonth" method="post">
                    <?php echo CHtml::imageButton('images/arrow-right.png'); ?>
                </form></td>
        </tr>
        </tbody>
    </table>
