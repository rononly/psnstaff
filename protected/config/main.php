<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
date_default_timezone_set('Europe/London');
return array(

    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'The PSN Staff Portal',
    // preloading 'log' component
    'preload' => array('log'),
    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.helpers.*',
        'application.vendors.*',
        'application.vendors.vendor.*',
        'application.vendors.vendor.rev42.tfpdf.src.*',
        'application.vendors.dompdf.*',
        'application.components.*',
        'application.components.class.*',
        'application.modules.srbac.controllers.SBaseController',
        'application.modules.administration.models.*',
        'application.modules.reports.models.Logons',
        'application.helpers.objects.*',
        'application.custom_classes.*',
        'ext.YiiMailer.YiiMailer',
        'ext.helpers.*',
    ),
    'modules' => array(
        'Communication' => array(),
        'random' => array(),
        'resources' => array(),
        'mileage' => array(),
        'administration' => array(),
        'admin' => array(),
        'news' => array(),
        'frontpage' => array(),
        'testing' => array(),
        'timesheet' => array(),
        'help' => array(),
        'reports' => array(),
        'users' => array(),
        'client' => array(),
        'maintenance' => array(),
        'srbac' => array(
            'userclass' => 'User', //communication: User
            'userid' => 'id', //communication: userid
            'username' => 'username', //communication:username
            'delimeter' => '@', //communication:-
            'debug' => false, //communication :false
            'pageSize' => 30, // communication : 15
            'superUser' => 'Authority', //communication: Authorizer
            'css' => 'srbac.css', //communication: srbac.css
            'layout' =>
            'application.views.layouts.main', //communication: application.views.layouts.main,
//must be an existing alias
            'notAuthorizedView' => 'srbac.views.authitem.unauthorized', // communication:
//srbac.views.authitem.unauthorized, must be an existing alias
            'alwaysAllowed' => array( //communication: array()
                'SiteLogin', 'SiteLogout', 'SiteIndex', 'SiteAdmin',
                'SiteError', 'SiteContact'),
            'userActions' => array('Show', 'View', 'List'), //communication: array()
            'listBoxNumberOfLines' => 40, //communication : 10
            'imagesPath' => 'srbac.images', // communication: srbac.images
            'imagesPack' => 'noia', //communication: noia
            'iconText' => true, // communication : false
            'header' => 'srbac.views.authitem.header', //communication : srbac.views.authitem.header,
//must be an existing alias
            'footer' => 'srbac.views.authitem.footer', //communication: srbac.views.authitem.footer,
//must be an existing alias
            'showHeader' => true, // communication: false
            'showFooter' => true, // communication: false
            'alwaysAllowedPath' => 'srbac.components', // communication: srbac.components
// must be an existing alias
        ),
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'giipass',
            // If removed, Gii defaults to localhost only. Edit carefully to taste.
            'ipFilters' => array('86.30.8.29', '82.1.174.250','86.177.214.187','82.1.174.250','::1'),
        ),
    ),
    // application components
    'components' => array(

        'user' => array(
// enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        // uncomment the following to enable URLs in path-format
        /*
          'urlManager'=>array(
          'urlFormat'=>'path',
          'rules'=>array(
          '<controller:\w+>/<id:\d+>'=>'<controller>/view',
          '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
          '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
          ),
          ),
         */
        'authManager' => array(
// Path to SDbAuthManager in srbac module if you want to use case insensitive
//access checking (or CDbAuthManager for case sensitive access checking)
            'class' => 'application.modules.srbac.components.SDbAuthManager',
            // The database component used
            'connectionID' => 'db',
            // The itemTable name (communication:authitem)
            'itemTable' => 'items',
            // The assignmentTable name (communication:authassignment)
            'assignmentTable' => 'assignments',
            // The itemChildTable name (communication:authitemchild)
            'itemChildTable' => 'itemchildren',
        ),

        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=',
            'emulatePrepare' => true,
            'username' => '',
            'password' => '',
            'charset' => 'utf8',
        ),

        'db_' => array(
            'connectionString' => 'mysql:host=localhost;dbname=',
            'emulatePrepare' => true,
            'username' => '',
            'password' => '',
            'charset' => 'utf8',
        ),

        'errorHandler' => array(
// use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                  array(
                  'class'=>'CWebLogRoute',
                  ),
                 */
            ),
        ),
        'ePdf' => array(
            'class' => 'ext.yii-pdf.EYiiPdf',
            'params' => array(
                'HTML2PDF' => array(
                    'librarySourcePath' => 'application.vendors.html2pdf.*',
                    'classFile' => 'html2pdf.class.php', // For adding to Yii::$classMap
                    'defaultParams' => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
                        'orientation' => 'P', // landscape or portrait orientation
                        'format' => 'A4', // format A4, A5, ...
                        'language' => 'en', // language: fr, en, it ...
                        'unicode' => true, // TRUE means clustering the input text IS unicode (default = true)
                        'encoding' => 'UTF-8', // charset encoding; Default is UTF-8
                        'marges' => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
                    ),
                ),
            ),
        ),
    ),
    // application-level parameters that can be accessed
// using Yii::app()->params['paramName']
    'params' => array(
// this is used in contact page
        'timesheets' => array(
            'submit-to' => '',
            'auto-submit' => 'Off',
        ),
        'adminEmail' => '',
        'smtp' => array(
            'relay_host' => '',
            'relay_port' => 587,
            'relay_authenticate' => true,
            'relay_username' => '',
            'relay_password' => '',
            'relay_from' => '',
            'relay_from_name' => '',
            'relay_read_receipt' => '',
            'relay_debug' => false,
            'no_reply' => '',
        )
    ),
);