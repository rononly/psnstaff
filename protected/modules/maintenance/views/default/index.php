<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>
<a href="index.php?r=maintenance/default/findunownedcalls">Find Unowned Calls</a>
</br>
<a href="index.php?r=maintenance/default/reassigncalls">Reassign Calls</a>
</br>
<a href="index.php?r=maintenance/default/showoldclienttableclientswithoutnewid">Show Unmatched Client from Old Table</a>
</br>
<a href="index.php?r=maintenance/default/updateoldclienttablewithnewuserids">Match New Client Ids to Old Client Table</a>
</br>
<a href="index.php?r=maintenance/default/clientidmatchtimesheetentries">Client Id Match Timesheet Entries</a>
</br>
<a href="index.php?r=maintenance/default/timesheetentriesnoclientid">Timesheet Entries with No Client Id</a>
</br>
<a href="index.php?r=maintenance/default/stripspacesfromnewclienttablepostcodes">Normalize Client Postcodes</a>
</br>
<a href="index.php?r=maintenance/default/missinginitialsandpostcode">Find Time Sheet Entries With No Initials AND Postcode</a>
</br>
<a href="index.php?r=maintenance/default/fixclientdetails">Find Time Sheet Entries With No Initials AND Postcode</a>