<?php

class administration_carer_helpers
{
    public function getNames()
    {
        $names = Yii::app()->db->createCommand()
            ->select('id, username')
            ->from('tbl_user')
            ->order('username ASC')
            ->queryAll();

        $prettyfiedNames = array();
        foreach ($names as &$name) {
            $ns = explode(' ', $name['username']);
            foreach ($ns as &$n) {
                $n = ucfirst($n);
            }
            unset($n);
            $name['username'] = implode(' ', $ns);
        }
        unset($name);
        return $names;
    }
    public function getNameById($id)
    {
        $name = Yii::app()->db->createCommand()
            ->select('username')
            ->from('tbl_user')
            ->where('id=:id', array(':id'=>$id))
            ->queryRow();
            return $name;
    }
    public function makePrettyName($name)
    {
        $names = explode(' ', $name);
        foreach($names as &$n)
        {
            $n = ucfirst($n);
        }
        unset($n);
        $outname = implode(' ', $names);
        return $outname;

    }
    public function ReOrderIdListBySurname($id_list)
    {
        /*
         * For report purposes we need to produce lists in alphabetical order of surname
         */

        $carers = array();
        $carer = array();
        foreach($id_list as $id)
        {
            $name = Yii::app()->db->createCommand()
                ->select('username')
                ->from('tbl_user')
                ->where('id=:id', array('id'=>$id))
                ->queryRow();
            $carer['name'] = $name['username'];
            $carer['id'] = $id;
            array_push($carers, $carer);
            unset($carer);
            $carer = array();
        }
        foreach($carers as &$thisCarer)
        {
            $name = $thisCarer['name'];
            $name_bits = explode(' ', $name);
            $renamed = '';
            for($i=count($name_bits); $i > 0; $i--)
            {
                $renamed .= $name_bits[$i] . ' ';
            }
            $thisCarer['name'] = $renamed;
            unset($thisCarer);
        }
        sort($carers);
        $ids = array();
        foreach($carers as $carer)
        {
            array_push($ids, $carer['id']);
        }
        return $ids;
    }
    public function getHoursForMonth()
    {
        $ids = Yii::app()->db->createCommand()
            ->select('id')
            ->from('tbl_user')
            ->order('username ASC')
            ->queryAll();

        //Now we have a list of all user Id's
        //We need to establish date range.
        $date_helper = new administration_date_helpers();
        $dates = $date_helper->createDateRangeArray();
        //Now we have the dates to check.
        //we need to iterate the id's fetching calls within the date range
        //calculate the total hours entered and add to a new array the id and hours.
        $data = array(); //Array for holding id and hours pair.
        foreach($ids as $id)
        {
            $calls = array();
            $hours = 0;
            foreach($dates as $date)
            {
                $todaysCalls = Yii::app()->db->createCommand()
                    ->select('minutes')
                    ->from('timesheets')
                    ->where(array('and', 'id=:id','date=:date'),array( ':id' => $id, ':date' => $date))
                    ->queryAll();

                foreach($todaysCalls as $tHours)
                {
                    $hours += $tHours;
                }
            }
            array_push($data,array('hours' => $hours));
        }
        return $data;
    }
}

?>