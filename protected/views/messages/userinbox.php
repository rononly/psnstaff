<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<?php
/* @var $this MessagesController */
/* @var $model Messages */

$this->breadcrumbs=array('Messages'=>array('viewmymessages'));

$this->menu=array(
	array('label'=>'Inbox', 'url'=>array('viewmymessages')),
	array('label'=>'Compose', 'url'=>array('create')),
);
?>
<?php $inboxHelper = new renderMessagesInbox_helper(); ?>

<h1>Inbox for <?php echo Yii::app()->user->name; ?></h1>

<?php $inboxHelper->renderMessageInbox(Yii::app()->user->id); ?>