<?php
/* @var $this CallTypesController */
/* @var $model CallTypes */

$this->breadcrumbs=array(
	'Call Types'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CallTypes', 'url'=>array('index')),
	array('label'=>'Create CallTypes', 'url'=>array('create')),
	array('label'=>'View CallTypes', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CallTypes', 'url'=>array('admin')),
);
?>

<h1>Update Call Types <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>