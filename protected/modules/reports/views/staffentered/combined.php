<?php
/* @var $this StaffenteredController */

$this->breadcrumbs=array(
	'Staff-Entered'=>array('/reports/staffentered'),
	'Combined Report',
);
?>
<?php $this->renderPartial('_combined_form', array('model' => $model)); ?>
