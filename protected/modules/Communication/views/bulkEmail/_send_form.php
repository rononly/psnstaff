<?php
/* @var $this BulkEmailController */
/* @var $model BulkEmail */
/* @var $form CActiveForm */
?>
<style type="text/css" media="screen">
    div.loading {
        background-color: #eee;
        background-image: url('images/loading2.gif');
        background-position:  center center;
        background-repeat: no-repeat;
        opacity: 1;
    }
    div.loading * {
        opacity: .8;
    }
</style>
<div class="form" id="form-page">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'send-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));

    $getUserNameList = new getUserNamesList_helper();
    $users = $getUserNameList->getUserNamesList(true);
    ?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table border=1>
        <tr>
            <td width=201 rowspan=5 valign=top style=''>
                <div class="row">
                    <?php echo $form->labelEx($model, 'recipients'); ?>
                    <?php echo $form->listBox($model, 'recipients', CHtml::listData($users, 'email', 'username'), array('multiple' => 'multiple', 'size' => 18, 'style' => 'width:200px'));?>
                    <?php echo $form->error($model, 'recipients'); ?>
                </div>
            </td>
            <td width=301 valign=top style=''>
                <div class="row">
                    <?php echo $form->labelEx($model, 'name'); ?>
                    <?php echo $form->dropDownList($model, 'name', CHtml::listData($data, 'id', 'name'),
                        array(
                            'style' => 'width: 392px',
                            'ajax' => array(
                                'type' => 'POST',
                                'beforeSend' => 'function(){
                                    $("#form-page").addClass("loading");}',
                                'complete' => 'function(){
                                    $("#form-page").removeClass("loading");}',
                                'url' => CController::createUrl('BulkEmail/ShowDescription'),
                                'success' => 'js:function(data){
                                    $("#' . CHtml::activeId($model, 'description') . '").val(data);
                    }',
                            )));
                    ?>
                    <?php echo $form->error($model, 'name'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <td width=301 valign=top style=''>
                <div class="row" id="description">
                    <?php echo $form->labelEx($model, 'description'); ?>
                    <?php echo $form->textArea($model, 'description', array('cols' => 46, 'rows' => 5)); ?>
                </div>
            </td>
        </tr>
        <tr>
            <td width=301 valign=top style=''>
                <div class="row" style="">
                    <?php echo $form->labelEx($model, 'subject'); ?>
                    <?php echo $form->textField($model, 'subject', array('size' => 60, 'rows' => 2)); ?>
                    <?php echo $form->error($model, 'subject'); ?>
                </div>
            </td>
        </tr>
        <tr>
            <td width=301 valign=top style=''>

            </td>
        </tr>
        <tr>
            <td width=301 valign=top halign=right style=''>
                <div class="row buttons">
                    <?php echo CHtml::submitButton('Send Bulk Email'); ?>
                </div>
            </td>
        </tr>
    </table>
    <?php $this->endWidget(); ?>
</div><!-- form -->