<?php
/* @var $this ClientController */

$this->breadcrumbs=array(
	'Manage Clients',
);
?>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>
<table class="centre" style="width: auto;max-width: 300px">
    <tbody>
    <tr class="total" style="text-align: left;">
        <td>
            Clients
        </td>
        <td>

        </td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">List Clients</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'list',
                'caption'=>'List',
                'options'=>array('icons'=>'js:{primary:"ui-icon-contact"}'),
                'url'=>$this->createUrl('/client/default/list'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Add Client</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'add',
                'caption'=>'Add',
                'options'=>array('icons'=>'js:{primary:"ui-icon-plusthick"}'),
                'url'=>$this->createUrl('/client/default/add'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Activate Client</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'activate',
                'caption'=>'Activate',
                'options'=>array('icons'=>'js:{primary:"ui-icon-check"}'),
                'url'=>$this->createUrl('/administration/client/activate'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Edit Client</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'edit',
                'caption'=>'Edit',
                'options'=>array('icons'=>'js:{primary:"ui-icon-pencil"}'),
                'url'=>$this->createUrl('/client/default/update'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Deactivate Client</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'deactivate',
                'caption'=>'Deactivate',
                'options'=>array('icons'=>'js:{primary:"ui-icon-minusthick"}'),
                'url'=>$this->createUrl('/administration/client/deactivate'),
            ));
            ?></td>
    </tr>
    <tr>
        <td style="text-align: right;">Remove Client</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'remove',
                'caption'=>'Remove',
                'options'=>array('icons'=>'js:{primary:"ui-icon-closethick"}'),
                'url'=>$this->createUrl('/client/default/capitalizeletters'),
            ));
            ?></td>
    </tr>
    </tbody>
</table>
