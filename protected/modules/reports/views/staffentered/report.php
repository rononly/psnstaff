<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 02/01/14
 * Time: 01:01
 */
$this->breadcrumbs=array(
    'Staff-Entered'=>array('/reports/staffentered'),
    'Display '.Yii::app()->session['_user_generated_report'].' Report',
);
$report_helper = new report_helper();
$title = $report_helper->generateTitle();
$generated_report = $report_helper->generateReport();
Yii::app()->session['_fully_generated_report'] = $generated_report;
$footer = $report_helper->generateFooter();
?>
<table class="centre" style="width: auto"><tr><td>
<?php echo $this->renderPartial('_action_menu', array('generated_report'=>$generated_report)); ?>
<?php echo $title;?>
<?php echo $generated_report;?>
<?php echo $footer;?>
<?php echo $this->renderPartial('_action_menu', array('generated_report'=>$generated_report)); ?>
    </td></tr></table>