<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 24/03/14
 * Time: 22:17
 */

class PopulateReportModel {
    function populateModel($model, $sender, $post)
    {
        if (isset($post[$sender]['staff'])) {
            $model->staff = $post[$sender]['staff'];
        }
        $model->all_staff = $post[$sender]['all_staff'];
        $model->start_date = $post[$sender]['start_date'];
        $model->end_date = $post[$sender]['end_date'];
        $model->submit_data = $post[$sender]['submit_data'];
        if($model->start_date == null || $model->end_date == null)
        {
            $date_helper = new date_helper();
            //Find and set start_date and end_date
            $model->start_date = date('Y-m-d', strtotime('-' . $date_helper->daysSince21st() . ' days', strtotime('today')));
            $model->end_date = date('Y-m-d', strtotime('+' . $date_helper->daysTill20th() . ' days', strtotime('today')));
        }
        //Populate Staff List
        if ($model->staff == NULL) {
            if ($model->all_staff === '0') {
                //Must be using job_role!
                //fetch all staff id's into single dimensional array to put into $model->staff
                foreach ($model->job_role as $role) {
                    $staff_well = JobAssignments::model()->findAllByAttributes(array('job_role_id' => $role), array('select' => 'user_id'));

                    foreach ($staff_well as &$staff) {
                        $staff = $staff['user_id'];
                    }
                    $staff_array[] = $staff_well;
                }
                if (isset($staff_array)) {
                    foreach ($staff_array as $single_array) {
                        foreach ($single_array as $staff_member) {
                            $model->staff[] = $staff_member;
                        }
                    }
                }
            } else {
                //fetch all staff id's into single dimensional array to put into $model->staff

                $model->staff = User::model()->findAll(array("select" => "id", "order" => "id ASC"));
                foreach ($model->staff as &$staff) {
                    $staff = $staff->id;
                }
            }
        } else {
            //$model->staff is single dimensional array( [0],[1],[2].... )
        }

        return $model;
    }
} 