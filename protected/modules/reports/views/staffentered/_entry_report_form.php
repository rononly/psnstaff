<?php
/**
 * Created by PhpStorm.
 * User: Ron Appleton
 * Date: 30/09/14
 * Time: 22:00
 */
$date_helper = new formatDateFor_helper();
?>
<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'logon-report-_entry_report_form-form',
        'enableAjaxValidation' => true,


    ));
    $getUserNameList = new getUserNamesList_helper();
    $users = $getUserNameList->getUserNamesList(true);?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table>
        <tr>
            <td><?php echo $form->labelEx($model, 'staff', array('class' => 'dates')); ?>
                <?php echo $form->dropDownList($model, 'staff', CHtml::listData($users, 'id', 'username')); ?></td>
            <td><?php
                echo $form->labelEx($model, 'start_date', array('class' => 'dates'));
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'start_date',
                    'flat' => false, //remove to hide the datepicker
                    'options' => array(
                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'dateFormat' => 'yy-mm-dd',
                    ),
                    'htmlOptions' => array(
                        'class' => 'dates',
                    ),
                ));
                ?></td>
            <td><?php
                echo $form->labelEx($model, 'end_date', array('class' => 'dates'));
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'end_date',
                    'flat' => false, //remove to hide the datepicker
                    'options' => array(
                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'dateFormat' => 'yy-mm-dd',
                    ),
                    'htmlOptions' => array(
                        'class' => 'dates',
                    ),
                ));
                ?></td>
            <td style="padding-top: 16px;"><?php echo CHtml::submitButton('Submit'); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo $form->error($model, 'start_date'); ?></td>
            <td><?php echo $form->error($model, 'end_date'); ?></td>
            <td></td>
        </tr>
    </table>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php
if(count($entries) == 0 || is_null($entries))
{
    echo '<h3>No entries added by that user within those dates.</h3>';
}
else{
    echo '<h3>The following ' . count($entries) . ' calls were added by that user within those dates:</h3>';
    ?>
    <table>
        <tr>
            <td>Client</td>
            <td>Call Date</td>
            <td>Started</td>
            <td>Ended</td>
            <td style="text-align: center">Added</td>
            <td style="text-align: center">Updated</td>
        </tr>
        <?php
        foreach($entries as $entry)
        {
            ?>
            <tr>
            <td><?php
                $client = Client::model()->findByPk($entry['client_id']);
                echo  $client->initials . ' ' . $client->surname; ?></td>
                <td><?php echo $date_helper->Viewing($entry['date']); ?></td>
                <td><?php echo $entry['start_time'] ?></td>
                <td><?php echo $entry['end_time'] ?></td>
                <td style="text-align: center">
                    <?php
                    $timestamp_bits = explode(' ',$entry['added']);
                    echo $date_helper->Viewing($timestamp_bits[0]).' '.$timestamp_bits[1]; ?></td>
                <?php
                    if($entry['added'] != $entry['updated'])
                    {
                        ?>
                        <td style="background-color: lightgreen; text-align: center; font-weight: bolder;">
                            <?php
                            $timestamp_bits = explode(' ',$entry['updated']);
                            echo $date_helper->Viewing($timestamp_bits[0]).' '.$timestamp_bits[1]; ?></td>
                        </td>
                        <?php
                    }
                    else {
                        echo '<td style="text-align: center">NO</td>';
                    }?>
            </tr>
            <?php
        }
        ?>
    </table>
<?php
}
?>