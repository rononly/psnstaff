<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of getWeekCommencingDate_helper
 *
 * @author Ron
 */
class getWeekCommencingDate_helper
{
    public function getWeekCommencingDate($chosenDate)
    {
        $chosenDate = new DateTime($chosenDate);
        $chosenDate = $chosenDate->format('Y-m-d');
        $chosenDate = strtotime($chosenDate);
        $dayOfWeekName = date("l", $chosenDate);
        $daysToSubtract = 0;
        switch ($dayOfWeekName) {
            case "Monday":
                break;
            case "Tuesday":
                $daysToSubtract = 1;
                break;
            case "Wednesday":
                $daysToSubtract = 2;
                break;
            case "Thursday":
                $daysToSubtract = 3;
                break;
            case "Friday":
                $daysToSubtract = 4;
                break;
            case "Saturday":
                $daysToSubtract = 5;
                break;
            case "Sunday":
                $daysToSubtract = 6;
                break;
        }
        //Now subtract the amount of days from the date and return it.
        $removeDays = sprintf('-%d days', $daysToSubtract);
        ;
        $timeStringMinusTheRemoveDays = strtotime($removeDays, $chosenDate);
        $convertedDate = date('Y-m-d', $timeStringMinusTheRemoveDays);
        return $convertedDate;
    }
}

?>
