<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 29/08/13
 * Time: 23:18
 * To change this template use File | Settings | File Templates.
 * $calls = all calls for this date and user
 * $mileage = all mileage for these calls, referenced in the mileage array via call_ref ($calls['id']
 */
$this->breadcrumbs = array(
    'Mileage'=>array('/mileage/default/index'),
    'Mileage by Call',
);
$addImage = CHtml::image($this->module->assetsURL . '/images/Add.png', 'Add extra mileage', array('width' => 24, 'height' => 24));
$editImage = CHtml::image($this->module->assetsURL . '/images/edit.png', 'Edit mileage', array('width' => 24, 'height' => 24));
$zeroImage = CHtml::image($this->module->assetsURL . '/images/zero.png', 'Zero mileage', array('width' => 24, 'height' => 24));



$formatter = new mileage_formatDateFor_helper();
$mileageHelper = new mileage_fetcher_helper();
$dh = new date_helper();
$dates = $dh->createDateRangeArray();
function lookupClient($id)
{
    $client = Client::model()->findByPk($id);
    return $client;
}
?>
<h3><?php echo $formatter->Viewing($date) . '    (' . date("l jS F Y", strtotime($date)) . ')' ?></h3>

<table class="centre" border="1" style="width: 550px;">
    <tbody>
    <tr class="header">
        <td>Date</td>
        <td>Identifier</td>
        <td>C2C</td>
        <td>SS</td>
        <td>SS</td>
        <td>Zero</td>
    </tr>
    <?php
    $date = '0';
    $total_c2c = 0.0;
    $total_ss = 0.0;
    foreach ($calls as $call) {
        if ($call) {
            $date = $call['date'];
        $thisMileage = $mileageHelper->fetchMiles($call['id']);
        $thisSS = $mileageHelper->fetchSSMiles($call['id']);
        }
        $addExtraLink = CHtml::link($addImage, array('extra/CreateExtra', 'call_ref' => $call['id']));
        $addSSLink = CHtml::link($addImage, array('socialsupport/CreateSocialSupport', 'call_ref' => $call['id']));
        $editSSLink = CHtml::link($editImage, array('socialsupport/UpdateSocialSupport', 'id' => $thisSS['id']));
        $zeroLink = CHtml::link($zeroImage, array('default/ZeroMileage', 'call_ref' => $call['id']));
        //Generate Mileage

        if (!isset($thisMileage['c2c_miles'])) {
            $thisMileage['c2c_miles'] = 0.0;
        }
        if (!isset($thisSS['mileage'])) {
            $thisSS['mileage'] = 0.0;
        }
        ?>
        <tr class="border_bottom">
            <td><?php echo CHtml::link($formatter->Viewing($call['date']), array('/timesheet/default/update',
                'id' => $call['id'], 'date' => $call['date']));?></td>
            <?php $client = lookupClient($call['client_id']); ?>
            <td><?php echo $client['initials'] . ' ' . $client['surname'] . ' ' . $client['postcode']; ?></td>
            <td style="font-weight: bold;">
                <?php echo $thisMileage['c2c_miles'];
                $total_c2c += $thisMileage['c2c_miles']; ?></td>
            <td style="font-weight: bold;">
                <?php echo $thisSS['mileage'];
                $total_ss += $thisSS['mileage']; ?></td>
            <td><?php
                if ($thisSS['mileage'] == 0)
                {
                    echo $addSSLink;
                }
                else
                {
                    echo $editSSLink;
                }?></td>
            <td><?php
                $zeroLink = CHtml::link($zeroImage, '#',
                    array("submit" => array('default/Zero', 'call_ref' => $call['id'], 'date' => $date), 'confirm' => 'Are you sure? This will delete the mileage for Extra and Social Support, have you thought of editing instead?'));
                echo $zeroLink; ?></td>
        </tr>
    <?php } ?>
    <tr class="total">
        <td></td>
        <td>TOTALS</td>
        <td><?php echo $total_c2c; ?></td>
        <td><?php echo $total_ss; ?></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    </tbody>
</table>