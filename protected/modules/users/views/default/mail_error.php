<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 11/03/14
 * Time: 22:02
 */
?>
<h3>An error occured whilst sending email.</h3>
<p>The error returned from the Php_mailer extension was this:</p>
<p><?php echo $mail->getError();?></p>
<h3>Mail Component Contents</h3>
<p>The following is the mail component contents:</p>
<p><?php
    foreach($mail as $key=>$value)
    {
        echo '</br>';
        echo $key.': ' . $value;
        echo '</br>';
    }

    ?></p>