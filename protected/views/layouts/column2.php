<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<?php
$rh = new RBAC_helper;
if ($rh->is('Authority') || $rh->is('Administrator')) {
    $this->menu3 = array(
        array('label' => 'Manage Admin', 'url' => array('/admin/default/index')),
        array('label' => 'Manage Admin - Add News', 'url' => array('/admin/default/addnews')),
        array('label' => 'Manage RBAC', 'url' => array('srbac/authitem/frontpage')),
        array('label' => 'Create with Gii', 'url' => array('/gii')),
        array('label' => 'Manage News', 'url' => array('/News/admin')),
        array('label' => 'Manage Messages', 'url' => array('/Messages/admin')),
        array('label' => 'Manage Job Roles', 'url' => array('/JobRoles/admin')),
        array('label' => 'Manage Call Types', 'url' => array('/CallTypes/admin')),
        array('label' => 'Manage Timesheets', 'url' => array('/Timesheets/admin')),
        array('label' => 'Communication', 'url' => array('/Communication/Communication')),
        array('label' => 'Mileage Admin', 'url' => array('/mileage/admin/index')),
        array('label' => 'Administration', 'url' => array('/administration/default/index')),
        array('label' => 'Reports (Staff-Entered)', 'url' => array('/reports/staffentered/index')),
        array('label' => 'Clean Job Assignments', 'url' => array('/reports/staffentered/cleanjobassignments')),
        array('label' => 'Maintenance', 'url' => array('/maintenance/default/index')),
        array('label' => 'News', 'url' => array('/news/default/index')),
    );
}
if ($rh->is('Manager') || $rh->is('Authority') ||
    $rh->is('Administrator') || $rh->is('Office Admin')
) {
    $this->reports_menu = array(
        array('label' => 'Carer Reports', 'url' => array('/reports/staffentered/index')),
        array('label' => 'Un-Submit Dates', 'url' => array('/reports/staffentered/unsubmit')),
        array('label' => 'Client Reports', 'url' => array('/reports/clients/index')),
        array('label' => 'Manage Users', 'url' => array('/users/default/index')),
        array('label' => 'Manage Clients', 'url' => array('/client/default/index')),
        array('label' => 'Portal Email', 'url' => 'http://email.psnstaff.info', 'linkOptions' => array('target' => '_blank')),
    );
}

$findUserNewsCount = new findUserNewsCount_helper();
$findUserMessagesCount = new findUserMessagesCount_helper();
$this->communication_menu = array(
    array('label' => 'News (' . $findUserNewsCount->findUserNewsCount() . ')', 'url' => array('/user/admin')),
    array('label' => 'Messages (' . $findUserMessagesCount->findUserMessagesCount() . ')', 'url' => array('messages/viewmymessages')),
);
$this->allStaff_menu = array(
    array('label' => 'Time Sheets', 'url' => array('/timesheet/default/selecttimesheetday')),
    array('label' => 'Mileage', 'url' => array('/mileage/default/index')),
);

$this->resources_menu = array(
    array('label' => 'Paperwork', 'url' => array('/resources/default/paperwork')),
);

?>
    <div class="span-19">
        <div id="content">
            <?php echo $content; ?>
        </div>
        <!-- content -->
    </div>

    <div class="span-5 last">
        <div id="sidebar">
            <?php
            /*if (!Yii::app()->user->isGuest) {
                $this->beginWidget('zii.widgets.CPortlet', array(
                    'title' => 'Communication',
                ));
                $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->communication_menu,
                    'htmlOptions' => array('class' => 'operations'),
                ));
                $this->endWidget();
            }*/
            if (!Yii::app()->user->isGuest) {
                $this->beginWidget('zii.widgets.CPortlet', array(
                    'title' => 'Actions',
                ));
                $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->allStaff_menu,
                    'htmlOptions' => array('class' => 'operations'),
                ));
                $this->endWidget();
                $this->beginWidget('zii.widgets.CPortlet', array(
                    'title' => 'Resources',
                ));
                $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->resources_menu,
                    'htmlOptions' => array('class' => 'operations'),
                ));
                $this->endWidget();
            }

            if ($rh->is('Manager') || $rh->is('Authority') ||
                $rh->is('Administrator') || $rh->is('Office Admin')
            ) {
                $this->beginWidget('zii.widgets.CPortlet', array(
                    'title' => 'Administration',
                ));
                $this->widget('zii.widgets.jui.CMenu', array(
                    'items' => $this->reports_menu,
                    'htmlOptions' => array('class' => 'operations'),
                ));
                $this->endWidget();
            }

            if ($rh->is('Authority') || $rh->is('Administrator')) {
                $this->beginWidget('zii.widgets.CPortlet', array(
                    'title' => 'Super Admin',
                ));
                $this->widget('zii.widgets.jui.CMenu', array(
                    'items' => $this->menu3,
                    'htmlOptions' => array('class' => 'operations'),
                ));
                $this->endWidget();
            }
            ?>
        </div>
        <!-- sidebar -->
    </div>
<?php $this->endContent(); ?>