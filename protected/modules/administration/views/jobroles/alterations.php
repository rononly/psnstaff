<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 01/09/13
 * Time: 00:34
 * To change this template use File | Settings | File Templates.
 */
?>
<h2 xmlns="http://www.w3.org/1999/html">Job Role Alterations</h2>

<?php
$dataProvider=new CActiveDataProvider('JobRoles');

$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'columns' => array('job_role', array('class' => 'CButtonColumn')),

));
?>