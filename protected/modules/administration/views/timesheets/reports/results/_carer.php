<?php
//$carer is passed to this partial.
$carer_helpers = new administration_carer_helpers();
$call_type_helpers = new administration_call_type_helpers();
$name = $carer_helpers->getNameById($carer['id']);
$name = $carer_helpers->makePrettyName($name['username']);
$totalHours = 0;
?>
<h2><?php echo $name; ?></h2>
<table style="width: 500px;">
    <tbody>
    <tr>
        <td>Date</td>
        <td>Client ID</td>
        <td>Call Type</td>
        <td>Call Length</td>
        <td>Total</td>
    </tr>
    <?php
    //die(var_dump($carer['calls']));
    foreach($carer['calls'] as $day)
    {
        foreach($day as $call)
        {
        echo '<tr>';
        echo '<td>' . $call['date'] . '</td>';
        echo '<td>' . $call['client_initials'] . ' ' . $call['client_postcode'] . '</td>';
        echo '<td>' . $call_type_helpers->lookUp($call['call_type_id']) . '</td>';
        echo '<td>' . ($call['minutes']/60) . '</td>';
        $totalHours += $call['minutes'];
        echo '<td>' . ($totalHours/60) . '</td>';
        echo '</tr>';
        }
    }
    ?>
    </tbody>
</table>
<h3>Total hours for month for <?php echo $name; ?> :: <?php echo ($totalHours/60); ?> </h3>