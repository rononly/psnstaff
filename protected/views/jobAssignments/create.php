<?php
/* @var $this JobAssignmentsController */
/* @var $model JobAssignments */

$this->breadcrumbs=array(
	'Job Assignments'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List JobAssignments', 'url'=>array('index')),
	array('label'=>'Manage JobAssignments', 'url'=>array('admin')),
);
?>

<h1>Create Job Assignments</h1>

<h2>Assign a job to:  <strong><?php echo Yii::app()->session['relevant_user']; ?></strong></h2>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>