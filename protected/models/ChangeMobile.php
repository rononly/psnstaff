<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ChangePassword
 *
 * @author Ron
 */
class ChangeMobile extends CFormModel
{

    public $mobile;

    public function rules()
    {
        return array(
            array('mobile', 'length', 'is' => 11, 'max' => 11),
            array('mobile', 'numerical', 'integerOnly' => true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'mobile' => 'New Mobile Number',
        );
    }
}
?>
