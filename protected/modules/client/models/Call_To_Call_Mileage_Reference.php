<?php

/**
 * This is the model class for table "call_to_call_mileage_reference".
 *
 * The followings are the available columns in table 'call_to_call_mileage_reference':
 * @property integer $from_client_id
 * @property integer $to_client_id
 * @property double $mileage
 */
class Call_To_Call_Mileage_Reference extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Call_To_Call_Mileage_Reference the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'call_to_call_mileage_reference';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('from_client_id, to_client_id, mileage', 'required'),
			array('from_client_id, to_client_id', 'numerical', 'integerOnly'=>true),
			array('mileage', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('from_client_id, to_client_id, mileage', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'from_client_id' => 'From Client',
			'to_client_id' => 'To Client',
			'mileage' => 'Mileage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('from_client_id',$this->from_client_id);
		$criteria->compare('to_client_id',$this->to_client_id);
		$criteria->compare('mileage',$this->mileage);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}