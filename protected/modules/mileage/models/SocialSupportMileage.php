<?php

/**
 * This is the model class for table "social_support_mileage".
 *
 * The followings are the available columns in table 'social_support_mileage':
 * @property integer $id
 * @property integer $call_ref
 * @property double $mileage
 * @property string $date
 */
class SocialSupportMileage extends CActiveRecord
{
    public $added;
    public $updated;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'social_support_mileage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('mileage, description', 'required'),
            array('mileage', 'numerical', 'integerOnly'=>false, 'min'=>0.1),
            array('mileage', 'CTypeValidator', 'type' => 'float'),
            array('mileage', 'length', 'max'=>5),
            array('description', 'length', 'max'=>1024),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, call_ref, mileage, description, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id' => 'ID',
            'call_ref' => 'Call Ref',
            'mileage' => 'Mileage',
            'description' => 'Description',
            'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('call_ref',$this->call_ref);
        $criteria->compare('mileage',$this->mileage);
        $criteria->compare('description',$this->description,true);
        $criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SocialSupportMileage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
