<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 28/02/14
 * Time: 00:33
 */

class SendPasswordReset {
    public function SendEmail($user,$password)
    {
        $mail_contents = BulkEmail::model()->findByAttributes(array('name'=>'Password Reset'));

        $names = explode(' ', $user['username']);
        $first_name = ucfirst($names[0]);
        $mail_contents['html'] = str_replace('^{user}^', $first_name, $mail_contents['html']);
        $mail_contents['html'] = str_replace('^{user_no_format}^', $user['username'], $mail_contents['html']);
        $mail_contents['html'] = str_replace('^{password}^', $password, $mail_contents['html']);

        $mail = new YiiMailer();
        $mail->IsSMTP();

        $mail->Host = Yii::app()->params['smtp']['relay_host'];
        $mail->Port = Yii::app()->params['smtp']['relay_port'];

        $mail->SMTPAuth = Yii::app()->params['smtp']['relay_authenticate'];
        $mail->Username = Yii::app()->params['smtp']['relay_username'];
        $mail->Password = Yii::app()->params['smtp']['relay_password'];

        $mail->SMTPDebug = Yii::app()->params['smtp']['relay_debug'];

        $mail->ConfirmReadingTo = Yii::app()->params['smtp']['relay_read_receipt'];

        $mail->From = Yii::app()->params['smtp']['relay_from'];
        $mail->FromName = Yii::app()->params['smtp']['relay_from_name'];
        $mail->Subject = 'PSN Staff Portal Password Reset';
        $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
        $mail->IsHTML(true);
        $mail->MsgHTML($mail_contents['html']);
        $mail->AddAddress($user['email']);
        if (!$mail->Send()) {
            return $mail;
            //THROW new Exception('Unable to Send Welcome Mail to '. $user['email']. '. Result->'.$mail->getError());
        } else {
            //record email sent
            Yii::app()->db->createCommand()
                ->insert('bulkemail_log', array('staff_id' => $user['id'], 'be_id' => $mail_contents->name,
                    'timestamp' => date('Y-m-d h:i:s', time())));
            return true;
        }
    }
} 