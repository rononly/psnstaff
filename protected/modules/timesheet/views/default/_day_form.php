<?php
/* @var $this TimesheetsController */
/* @var $model Timesheets */
/* @var $form CActiveForm */
/* @var $idents Client Ids for dropdown */
//Helpers
$date_helper = new date_helper();
$getCallTypes = new getCallTypes_helper();
$th = new timesheet_helper();
?>
    <div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'timesheets-form',
            'enableAjaxValidation' => false,
        ));
        ?>


        <p class="note">The times entered should be your rota times, not actual.</p>
        <?php echo $form->errorSummary($model); ?>
        <table class="centre" border="1" style="width: 600px;">
            <tbody>
            <tr>
                <?php echo $form->hiddenField($model, 'date', array('value' => $date)); ?>
                <?php echo $form->hiddenField($model, 'user_id', array('value' => Yii::app()->user->id)); ?>

                <td>
                    <?php echo $form->labelEx($model, 'client_id'); ?>
                    <?php echo $form->dropDownList($model, 'client_id',CHtml::listData($idents,'id','ident'),array('style'=>'width:140px')); ?>
                </td>

                <td>
                    <?php echo $form->labelEx($model, 'call_type_id'); ?>
                    <?php echo $form->dropDownList($model, 'call_type_id',
                        CHtml::listData($getCallTypes->getCallTypes(), 'id', 'call_type'));
                    ?>
                    <?php echo $form->error($model, 'call_type_id'); ?></td>
                <td>
                    <?php echo $form->labelEx($model, 'shour'); ?>
                    <?php
                    echo $form->dropDownList($model, 'shour', array('00' => '00', '01' => '01',
                        '02' => '02', '03' => '03',
                        '04' => '04', '05' => '05',
                        '06' => '06', '07' => '07',
                        '08' => '08', '09' => '09',
                        '10' => '10', '11' => '11',
                        '12' => '12', '13' => '13',
                        '14' => '14', '15' => '15',
                        '16' => '16', '17' => '17',
                        '18' => '18', '19' => '19',
                        '20' => '20', '21' => '21',
                        '22' => '22', '23' => '23')); ?>
                    <?php
                    echo $form->dropDownList($model, 'smin', array(
                        '00' => '00', '01' => '01',
                        '02' => '02', '03' => '03',
                        '04' => '04', '05' => '05',
                        '06' => '06', '07' => '07',
                        '08' => '08', '09' => '09',
                        '10' => '10', '11' => '11',
                        '12' => '12', '13' => '13',
                        '14' => '14', '15' => '15',
                        '16' => '16', '17' => '17',
                        '18' => '18', '19' => '19',
                        '20' => '20', '21' => '21',
                        '22' => '22', '23' => '23',
                        '24' => '24', '25' => '25',
                        '26' => '26', '27' => '27',
                        '28' => '28', '29' => '29',
                        '30' => '30', '31' => '31',
                        '32' => '32', '33' => '33',
                        '34' => '34', '35' => '35',
                        '36' => '36', '37' => '37',
                        '38' => '38', '39' => '39',
                        '40' => '40', '41' => '41',
                        '42' => '42', '43' => '43',
                        '44' => '44', '45' => '45',
                        '46' => '46', '47' => '47',
                        '48' => '48', '49' => '49',
                        '50' => '50', '51' => '51',
                        '52' => '52', '53' => '53',
                        '54' => '54', '55' => '55',
                        '56' => '56', '57' => '57',
                        '58' => '58', '59' => '59',)); ?>
                </td>
                <td>
                    <?php echo $form->labelEx($model, 'ehour'); ?>
                    <?php
                    echo $form->dropDownList($model, 'ehour', array('00' => '00', '01' => '01',
                        '02' => '02', '03' => '03',
                        '04' => '04', '05' => '05',
                        '06' => '06', '07' => '07',
                        '08' => '08', '09' => '09',
                        '10' => '10', '11' => '11',
                        '12' => '12', '13' => '13',
                        '14' => '14', '15' => '15',
                        '16' => '16', '17' => '17',
                        '18' => '18', '19' => '19',
                        '20' => '20', '21' => '21',
                        '22' => '22', '23' => '23')); ?>
                    <?php
                    echo $form->dropDownList($model, 'emin', array('00' => '00', '01' => '01', '02' => '02', '03' => '03', '04' => '04', '05' => '05', '06' => '06', '07' => '07', '08' => '08', '09' => '09', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31', '32' => '32', '33' => '33', '34' => '34', '35' => '35', '36' => '36', '37' => '37', '38' => '38', '39' => '39', '40' => '40', '41' => '41', '42' => '42', '43' => '43', '44' => '44', '45' => '45', '46' => '46', '47' => '47', '48' => '48', '49' => '49', '50' => '50', '51' => '51', '52' => '52', '53' => '53', '54' => '54', '55' => '55', '56' => '56', '57' => '57', '58' => '58', '59' => '59',)); ?></td>
                <td style="vertical-align: bottom">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Add' : 'Save'); ?></td>
            </tr>
            </tbody>
        </table>

        <?php $this->endWidget(); ?>

    </div><!-- form -->

<?php
$select = new timesheet_helpers();
$data = $select->getDisplaySelectByCall($date, Yii::app()->user->id);
$formatter = new formatDateFor_helper();
$callTypeLU = new getCallTypes_helper();
$editImage = CHtml::image('images/edit.png', 'Edit This Call', array('width' => 24, 'height' => 24));
$deleteImage = CHtml::image('images/delete.png', 'Delete Call', array('width' => 24, 'height' => 24));
function minDiff($start,$end)
{
    $start = strtotime($start);
    $end = strtotime($end);
    $diff = $end - $start;
    $diff = $diff /60; //Converts difference from seconds to minutes.
    return $diff;
}
function lookupClient($id)
{
    $client = Client::model()->findByPk($id);
    return $client;
}
if (!$data === null) {
    ?>
    <div id="Sheet_Display">
<?php
}
if ($data == null) {
    if (strpos(Yii::app()->request->urlReferrer, 'timesheet/update') !== false) {
        $this->redirect(array('/timesheet/selecttimesheetday'));
    }
} else {
    echo '<table class="centre" border="1" style="width: 600px;"><tbody>';
    echo '<tr class="header"><td>Date</td><td>Client<td>Call Time</td><td>Call Type</td><td>
                        Hours</td><td>Total</td><td>Action</td></tr>';
    $totalHours = 0;
    foreach ($data as $call) {
        echo '<tr class="border_bottom">';
        echo '<td>' . CHtml::link($formatter->Viewing($call['date']), array('/timesheet/default/update',
                'id' => $call['id'], 'date' => $call['date'])) . '</td>';
        $client = lookupClient($call['client_id']);
        echo '<td>' . $client['initials'] . ' ' . $client['surname'] . ' ' . $client['postcode'] . '</td>';
        echo '<td>' . $th->cleanTime($call['start_time']) . ' - ' . $th->cleanTime($call['end_time']) . '</td>';
        echo '<td>' . $callTypeLU->lookUp($call['call_type_id']) . '</td>';
        $call_length = round(minDiff($call['start_time'],$call['end_time'])/60,2);
        if($call_length <= 0.0)
        {
            $call_length = 24 + $call_length;
        }
        echo '<td>' . $call_length . '</td>';
        $totalHours += $call_length;
        echo '<td>' . round($totalHours,2) . '</td>';
        $editLink = CHtml::link($editImage, array('/timesheet/default/update',
            'id' => $call['id'], 'date' => $date));
        $deleteLink = CHtml::link($deleteImage, '#',
            array("submit" => array('delete', 'id' => $call['id'], 'date' => $date), 'confirm' => 'Are you sure?'));
        echo '<td>' . $editLink . $deleteLink . '</td>';
        echo '</tr>';
    }
    echo '</table>';
}
if (!$data === null) {
    ?>
    </div>
<?php
}
?>