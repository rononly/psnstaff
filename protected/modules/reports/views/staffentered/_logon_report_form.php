<?php
/* @var $this LogonReportController */
/* @var $model LogonReport */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'logon-report-_logon_report_form-form',
        'enableAjaxValidation' => true,


    ));
    $getUserNameList = new getUserNamesList_helper();
    $users = $getUserNameList->getUserNamesList(true);?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table>
        <tr>
            <td><?php echo $form->labelEx($model, 'staff', array('class' => 'dates')); ?>
                <?php echo $form->dropDownList($model, 'staff', CHtml::listData($users, 'id', 'username')); ?></td>
            <td><?php
            echo $form->labelEx($model, 'start_date', array('class' => 'dates'));
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'start_date',
                'flat' => false, //remove to hide the datepicker
                'options' => array(
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'dateFormat' => 'yy-mm-dd',
                ),
                'htmlOptions' => array(
                    'class' => 'dates',
                ),
            ));
            ?></td>
            <td><?php
            echo $form->labelEx($model, 'end_date', array('class' => 'dates'));
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'end_date',
                'flat' => false, //remove to hide the datepicker
                'options' => array(
                    'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                    'dateFormat' => 'yy-mm-dd',
                ),
                'htmlOptions' => array(
                    'class' => 'dates',
                ),
            ));
            ?></td>
            <td style="padding-top: 16px;"><?php echo CHtml::submitButton('Submit'); ?></td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo $form->error($model, 'start_date'); ?></td>
            <td><?php echo $form->error($model, 'end_date'); ?></td>
            <td></td>
        </tr>
    </table>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<?php
if (!empty($data))
{
    if(count($data) > 1)
    {
        if(count($data) == 2)
        {
            echo '<h3>The user logged on a couple of times within that period as follows:</h3>';
        }
        else {
            echo '<h3>The user logged on several times within that period as follows:</h3>';
        }

    }
    else {
        echo '<h3>The only time the user logged on within that period was:</h3>';
    }
    ?>
    <?php
    foreach ($data as $logons) {
        echo '<h4>' . date('l jS F Y H:i:s', strtotime($logons['datetime'])) . '</h4>';
    }
}
else {
if (!empty($lastlogin)) {
    ?>
    <h3>There was no record of a login within the dates you selected.</h3>
    <h3>The last time the user logged in however was:</h3>
    <?php
    if(!isset($lastlogin['datetime']))
    {
        echo '<h4>' . date('l jS F Y H:i:s', strtotime($lastlogin['last_login'])) . '</h4>';
    }
    else {
        echo '<h4>' . date('l jS F Y H:i:s', strtotime($lastlogin['datetime'])) . '</h4>';
    }
} else {
    ?>
    <h3>There is no record of a login by the person you selected!</h3>
<?php
}
}
?>