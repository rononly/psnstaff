<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 19/01/14
 * Time: 01:39
 */
$this->breadcrumbs=array(
    'Manage Clients'=>array('index'),
    'List Clients',
);
?>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>
<div style="text-align: center">
<?php
$this->widget('zii.widgets.jui.CJuiButton', array(
    'buttonType'=>'link',
    'name'=>'active',
    'caption'=>'Active',
    'options'=>array('icons'=>'js:{primary:"ui-icon-contact"}'),
    'url'=>$this->createUrl('/client/default/list&view=active'),
));
$this->widget('zii.widgets.jui.CJuiButton', array(
    'buttonType'=>'link',
    'name'=>'inactive',
    'caption'=>'Inactive',
    'options'=>array('icons'=>'js:{primary:"ui-icon-contact"}'),
    'url'=>$this->createUrl('/client/default/list&view=inactive'),
));
$this->widget('zii.widgets.jui.CJuiButton', array(
    'buttonType'=>'link',
    'name'=>'all',
    'caption'=>'All',
    'options'=>array('icons'=>'js:{primary:"ui-icon-contact"}'),
    'url'=>$this->createUrl('/client/default/list&view=all'),
));
?>
</div>
<p></p>
<?php
foreach($clients as $client)
{
    ?>
    <table class="centre" style="width: 600px;font-size: 16px;">
        <tbody>
        <tr>
            <td style="min-width: 200px;"><strong><?php echo $client->initials. ' '. $client->surname; ?></strong></td>
            <td style="min-width: 300px;text-align: right;"></td>
            <?php
            //Make formatted link.
            $active_link = CHtml::link('Active',array('/client/default/toggleactive',
                'id'=>$client->id,
                'active'=>$client->active));
            ?>
            <td style="width: 50px;text-align: right"><?php echo $active_link; ?></td>
            <?php
            if($client->active == 1)
            {
                ?><td style="color: black;background-color: limegreen;min-width: 50px;max-width: 50;text-align: left;"><strong>YES</strong></td><?php
            }
            else
            {
                ?><td style="color: black;background-color: red;min-width: 50px;max-width: 50px;text-align: left;"><strong>NO</strong></td><?php
            }
            ?>

        </tr>
        <tr class="border_bottom" style="font-size: 12px;">
            <td style="width: 200px;text-align: right;">Property Number</td>
            <td style="width: 300px;text-align: left;"><?php echo $client->property_number; ?></td>
            <td style="min-width: 50px;max-width: 50px"></td>
            <td style="min-width: 50px;max-width: 50px"></td>
        </tr>
        <tr class="border_bottom" style="font-size: 12px;">
            <td style="width: 200px;text-align: right;">Postcode</td>
            <td style="width: 300px;text-align: left;"><?php echo $client->postcode;?></td>
            <td style="width: 50px;"></td>
            <td style="width: 50px;"></td>
        </tr>
        <tr>
            <td><?php echo CHtml::link('Recalc Mileage',Yii::app()->createUrl('/client/default/singlerecalc',array('id'=>$client->id))); ?></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>
<?php
}