<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 29/08/13
 * Time: 10:58
 * To change this template use File | Settings | File Templates.
 */

$this->breadcrumbs = array(
    'Time Sheets' => array('selecttimesheetday'),
    'Choose Submission Dates' => array('submitcalls'),
    'ooooops');
$this->menu =$menu;
?>
<h1 style="color: red;">oooops.....</h1>

<p>Hey, it would appear you have tried to submit calls, when you have no calls entered this month that you
    can submit.</p>
<p>Or.. you are trying to submit today's calls. For consistency and to prevent errors, the system will not
    let you submit today until tomorrow in case your calls change, and you are trying to submit them before
    completing your calls.</p>
<p>If you believe this is wrong, please contact Ron.</p>