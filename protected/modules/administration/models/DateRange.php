<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 22/08/13
 * Time: 12:38
 * To change this template use File | Settings | File Templates.
 */

class DateRange extends CFormModel
{

    public $start;
    public $end;

    public function rules()
    {
        return array (
            array('start', 'checkStartDate'),
            array('end', 'checkEndDate'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    public function attributeLabels()
    {
        return array(
            'start' => 'Start Date',
            'end' => 'End Date',
        );
    }

    public function checkStartDate($attribute, $params)
    {
        if ($this->$attribute > $this->end)
            $this->addError($attribute, 'The start date must come before or be the same as the end date!');
    }
    public function checkEndDate($attribute, $params)
    {
        if ($this->$attribute < $this->start)
            $this->addError($attribute, 'The end date must come after or be the same as the end date!');
    }
}