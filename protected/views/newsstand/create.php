<?php
/* @var $this NewsStandController */
/* @var $model NewsStand */

$this->breadcrumbs=array(
	'News Stands'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List NewsStand', 'url'=>array('index')),
	array('label'=>'Manage NewsStand', 'url'=>array('admin')),
);
?>

<h1>Create News Stand</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>