<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

    public function actionTimesheets()
    {
        $this->render('timesheets');
    }

    public function actionMileage()
    {
        $this->render('mileage');
    }
}