<?php
/**
 * Created by PhpStorm.
 * User: Ron Appleton
 * Date: 05/10/14
 * Time: 00:12
 */

class mileage_calculator {

    public $debug=false;
    public function calculateMileage($days)
    {
        if (isset($days)) {
            $calc_list = null;
            $calc_list_index = 0;
            foreach ($days as $day) {
                $last_call = null;
                for ($call = 0; $call < count($day); $call++) {
                    if ($i = 0) {
                        $last_call = $day[$call];
                        continue;
                    } else {
                        $calc_list[$calc_list_index]['from'] = $last_call;
                        $calc_list[$calc_list_index]['to'] = $day[$call];
                        $last_call = $day[$call];
                        $calc_list_index++;
                    }
                }
            }

            //Debug Point 1
            if ($this->debug) {
                foreach ($calc_list as $pair) {
                    if ($pair['from']['client_id'] == '') {
                        continue;
                    }
                    $html = '<h3>Debug Point 1 - Checking about to be processed has valid from and to ID.</h3>';
                    $html .= '<table style="width: 200px;"><tr><td style="width: 50px">From Client ID</td><td style="width: 50px">To Client ID</td></tr>';
                    $html .= '<tr><td>' . $pair['from']['client_id'] . '</td><td>' . $pair['to']['client_id'] . '</td></tr>';
                    $html .= '</table>';
                    echo $html;
                }
            }
            //Now we should have an array containing each of the calls needed for the calculation.
            //So we can begin to cycle through the list retrieving the mileage and storing a mileage record.
            foreach ($calc_list as $call_pair) {
                if ($call_pair['from']['client_id'] == '') {
                    continue;
                }
                $criteria = new CDbCriteria();
                $criteria->condition = 'from_client_id=:from_client_id AND to_client_id=:to_client_id';
                $criteria->params = array(':from_client_id' => $call_pair['from']['client_id'],
                    ':to_client_id' => $call_pair['to']['client_id']);
                $mileage_model = Call_To_Call_Mileage_Reference::model()->find($criteria);
                //$mileage should now contain the distance between the two calls
                if (is_null($mileage_model)) // || empty($mileage_model))
                {
                    $mileage = 0.0;
                } else {
                    //This is where we disallow mileage based on break period
                    $time1 = strtotime($call_pair['from']['end_time']);
                    $time2 = strtotime($call_pair['to']['start_time']);
                    $diff = $time2 - $time1;
                    if($diff > 7119)
                    {
                        $mileage = 0.0;
                    }
                    else {
                        $mileage = $mileage_model['mileage'];
                    }
                }
                $criteria = new CDbCriteria();
                $criteria->condition = 'user_id=:user_id AND call_ref=:call_ref';
                $criteria->params = array(':user_id' => $call_pair['to']['user_id'], ':call_ref' => $call_pair['to']['id']);
                $mileage_ref = Mileage::model()->find($criteria);
                //Debug Point 2
                if ($this->debug && !is_null($mileage_ref)) {
                    $html = '<h3>Debug Point 2 - Mileage Reference Found for from and to ID\'s.</h3>';
                    $html .= '<table style="width: 400px;"><tr><td style="width: 100px">From Client ID</td><td style="width: 100px">To Client ID</td><td>Mileage</td></tr>';
                    $html .= '<tr><td>' . $call_pair['from']['client_id'] . '</td><td>' . $call_pair['to']['client_id'] . '</td><td>' . $mileage_model->mileage . '</td></tr>';
                    $html .= '</table>';
                    echo $html;
                    $html = '<table style="width: 400px"><tr><td>User_id</td><td>Call_ref</td><td>C2C_miles</td><td>Date</td></tr>';
                    $html .= '<tr><td>' . $mileage_ref->user_id . '</td>';
                    $html .= '<td>' . $mileage_ref->call_ref . '</td>';
                    $html .= '<td>' . $mileage_ref->c2c_miles . '</td>';
                    $html .= '<td>' . $mileage_ref->date . '</td></tr>';
                    $html .= '</table>';
                    echo $html;
                }
                //Now if a calc for these calls already exists we will have it in $mileage_ref

                if (is_null($mileage_ref)) {
                    //Insert
                    $model = new Mileage();
                    $model->user_id = $call_pair['to']['user_id'];
                    $model->call_ref = $call_pair['to']['id'];
                    $model->c2c_miles = (float)$mileage;
                    $model->date = $call_pair['to']['date'];
                    //Debug Point 3
                    if($this->debug)
                    {
                        $html = '<h3>Debug Point 3 - No milage reference found so generating new model.</h3>';
                        $html .= '<table style="width: 400px"><tr><td>User_id</td><td>Call_ref</td><td>C2C_miles</td><td>Date</td></tr>';
                        $html .= '<tr><td>' . $model->user_id . '</td>';
                        $html .= '<td>' . $model->call_ref . '</td>';
                        $html .= '<td>' . $model->c2c_miles . '</td>';
                        $html .= '<td>' . $model->date . '</td></tr>';
                        $html .= '</table>';
                        echo $html;
                    }

                    if (!$model->save(false) && $this->debug) {
                        $html = '<h3>Failed Save - Call Details</h3>';
                        $html .= '<table style="width: 400px"><tr><td>User_id</td><td>Call_ref</td><td>C2C_miles</td><td>Date</td></tr>';
                        $html .= '<tr><td>' . $model->user_id . '</td>';
                        $html .= '<td>' . $model->call_ref . '</td>';
                        $html .= '<td>' . $model->c2c_miles . '</td>';
                        $html .= '<td>' . $model->date . '</td></tr>';
                        $html .= '</table>';
                        echo $html;
                    } else {
                        if ($this->debug) {
                            $html = '<h3>Saved - Call Details</h3>';
                            $html .= '<table style="width: 400px"><tr><td>User_id</td><td>Call_ref</td><td>C2C_miles</td><td>Date</td></tr>';
                            $html .= '<tr><td>' . $model->user_id . '</td>';
                            $html .= '<td>' . $model->call_ref . '</td>';
                            $html .= '<td>' . $model->c2c_miles . '</td>';
                            $html .= '<td>' . $model->date . '</td></tr>';
                            $html .= '</table>';
                            echo $html;
                        }
                    }

                } else {
                    //Update
                    $mileage_ref->c2c_miles = (float)$mileage;
                    $mileage_ref->date = $call_pair['to']['date'];
                    $mileage_ref->update();
                }

            }
        }
    }
} 