<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 06/03/14
 * Time: 00:26
 *
 * @param clients = list of unmatches clients
 */
?>
<p>Un Assigned Old Clients</p>
<table class="centre">
    <tr class="header">
        <td>Old Client ID</td>
        <td>New Client ID</td>
        <td>Match</td>
    </tr>
<?php
foreach($clients_old as $client)
{
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-form',
    'enableAjaxValidation' => false,
));
    echo $form->hiddenField($model, 'from_ident', array('value'=>$client['ident']));
    ?>
        <tr class="border_bottom">
            <td><?php echo $client['ident'].' ('.$client['postcode'].')';?></td>
            <td><?php echo $form->dropDownList($model,'to_id',CHtml::listData($idents,'id','ident'));?></td>
            <td><?php echo CHtml::submitButton('Match');?></td>
        </tr>
  <?php
    $this->endWidget();
}
?>
</table>
<p>Unmatched New Clients</p>
<?php
foreach($non_matched as $non)
{
    echo $non['id']. ' ' . $non['initials']. ' ' . $non['surname'] . ' '. $non['postcode'].'</br>';
}