<?php
/* @var $this JobAssignmentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Job Assignments',
);

$this->menu=array(
	array('label'=>'Create JobAssignments', 'url'=>array('create')),
	array('label'=>'Manage JobAssignments', 'url'=>array('admin')),
);
?>

<h1>Job Assignments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
