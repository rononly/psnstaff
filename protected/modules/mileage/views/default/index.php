<?php
/* @var $this DefaultController */
/* @var $dates = Valid Dates for adding mileage. */
/* @var $days = Call information. */
$this->breadcrumbs = array(
    ucfirst($this->module->id),
);
$editImage = CHtml::image($this->module->assetsURL . '/images/edit.png', 'Edit mileage', array('width' => 24, 'height' => 24));
$arrowLeft = CHtml::image('images/live_journal_sml.png', 'Back one month');
$arrowRight = CHtml::image('images/live_journal_sml.png', 'Forward one month');
$formatter = new mileage_formatDateFor_helper();
$mileageHelper = new mileage_fetcher_helper();

?>
<table class="centre" border="1" cellpadding="1" cellspacing="1" style="width: 450px;">
    <tbody>
    <tr class="topbar">
        <td>
            <form action="index.php?r=mileage/default/backonemonth" method="post">
                <?php echo CHtml::imageButton('images/arrow-left.png'); ?>
            </form>
        </td>
        <td></td>
        <td>

        </td>
        <td></td>
        <td>
            <form action="index.php?r=mileage/default/forwardonemonth" method="post">
                <?php echo CHtml::imageButton('images/arrow-right.png'); ?>
            </form>
        </td>
    </tr>
    <tr class="header">
        <td>Date</td>
        <td>C2C</td>
        <td>SS</td>
        <td>Edit</td>
    </tr>
    <?php
    $total_c2c_miles = 0.0;
    $total_ss_miles = 0.0;
    $total_extra_miles = 0.0;
    foreach ($days as $day) {
        $thisExtra = $mileageHelper->fetchExtraMilesDate($day['date']);
        $thisSS = $mileageHelper->fetchSSMilesDate($day['date']);
        ?>
        <tr class="border_bottom">
        <td><?php echo CHtml::link($formatter->Viewing($day['date']), array('mileage/calls', 'date' => $day['date'])) ?></td>
        <?php $total_c2c_miles += $day['c2c']; ?>
        <td><?php echo $day['c2c']; ?></td>
        <?php
        if ($thisSS == null)
        {?>
        <td style="font-weight: bold;">0</td>
        <?php } else {
        ?>
        <td style="font-weight: bold;"><?php echo $thisSS;
            $total_ss_miles += $thisSS;?>
        </td><?php
        }?></td>
        <?php
        $editLink = CHtml::link($editImage, array('mileage/calls',
            'date' => $day['date']));?>
        <td><?php echo $editLink; ?></td>
        </tr><?php
    }
    ?>
    <tr class="border_bottom">
        <td style="font-size: 16px; font-weight: bold;">TOTALS</td>
        <td style="font-size: 16px; font-weight: bold;"><?php echo $total_c2c_miles; ?></td>
        <td style="font-size: 16px; font-weight: bold;"><?php echo $total_ss_miles; ?></td>
        <td style="font-size: 16px; font-weight: bold;">(<?php echo $total_c2c_miles + $total_ss_miles;?>)</td>
    </tr>
    </tbody>
</table>