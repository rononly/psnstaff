<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of renderMessagesInbox_helper
 *
 * @author Ron
 */
class renderMessagesInbox_helper
{

    public function renderMessageInbox($user_id)
    {
        //Get Messages
        $messages = Yii::app()->db->createCommand()
            ->select('*')
            ->from('messages')
            ->where('recipient_id=:id', array(':id' => $user_id))
            ->queryAll();
        //Messages got
        //Three columns
        //Date, Sender, Subject
        $header = '<table border="1" cellpadding="1" cellspacing="1" style="width: 650px;">
		<thead><tr><th scope="col">Date Recieved</th><th scope="col">Sender</th>
		<th scope="col">Subject</th></tr></thead><tbody>';
        echo $header;
        $getUsername = new getUserName_helper();
        foreach ($messages as $message) {
            $col1 = '<tr><td>' . $message['creation_date'] . '</td>';
            $col2 = '<td>' . $getUsername->lookUp($message['author_id']) . '</td>';
            $col3 = '<td>' . $message['subject'] . '</td></tr>';
            echo $col1, $col2, $col3;
        }
        echo $footer = '</tbody></table>';
    }

}