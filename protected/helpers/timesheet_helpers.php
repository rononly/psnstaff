<?php
class timesheet_helpers
{
    public function getDisplaySelectByCall($day, $user_id)
    {
        $days = Yii::app()->db->createCommand()
            ->select('*')
            ->from('timesheets')
            ->where(array('and', 'date=:date', 'user_id =:user_id'),
                array(':date' => $day, ':user_id' => $user_id))
            ->order('start_time ASC')
            ->queryAll();
        return $days;
    }

    public function getDisplaySelectByWeekCommencing($week_commencing, $user_id)
    {
        $calls = Yii::app()->db->createCommand()
            ->select('*')
            ->from('timesheets')
            ->where(array('and', 'week_commencing =:week_commencing', 'user_id =:user_id'),
                array(':week_commencing' => $week_commencing, ':user_id' => $user_id))
            ->order('date DESC, id ASC')
            ->queryAll();
        return $calls;
    }

    public function getDisplayCallsByWeekCommencing($week_commencing, $user_id)
    {
        $calls = Yii::app()->db->createCommand()
            ->select('*')
            ->from('timesheets')
            ->where(array('and', 'week_commencing =:week_commencing', 'user_id =:user_id'),
                array(':week_commencing' => $week_commencing, ':user_id' => $user_id))
            ->order('date ASC')
            ->queryAll();
        return $calls;
    }

    public function getDisplayCallsByDay($date, $user_id)
    {

    }
    public function getDisplaySelectByDay($week_commencing, $user_id)
    {
        $display_days = array();
        $day_disp_obj = new day_display_object();
        $mins = 0;

        $days = Yii::app()->db->createCommand()
            ->select('date')
            ->from('timesheets')
            ->where(array('and', 'week_commencing=:week_commencing', 'user_id =:user_id'),
                array(':week_commencing' => $week_commencing, ':user_id' => $user_id))
            ->queryAll();

        $dates = array();
        foreach ($days as $day) {
            $dates[] = $day['date'];
        }
        $dates = array_values(array_unique($dates));

        foreach ($dates as $day) {
            $calls = Timesheets::model()->findAllByAttributes(array('date'=>$day, 'user_id'=>$user_id));

            foreach ($calls as $call) {
                $mins += $call['minutes'];
            }
            $day_disp_obj->date = $day;
            $day_disp_obj->calls = count($calls);
            $day_disp_obj->hours = $mins / 60;
            $mins = 0;
            $display_days[] = $day_disp_obj;
            $day_disp_obj = new day_display_object();
        }
        return $display_days;
    }
    public function getDisplaySelectByDayRange($user_id)
    {
        $display_days = array();

        $mins = 0;
        $dh = new date_helper();
        $dates = $dh->createDateRangeArray();

        foreach ($dates as $day) {
            $day_disp_obj = new day_display_object();
            $calls = Timesheets::model()->findAllByAttributes(array('date'=>$day, 'user_id'=>$user_id));

            if ($calls == null)
            {
                $day_disp_obj->date = $day;
                $day_disp_obj->calls = 0;
                $day_disp_obj->hours = 0;
                $mins = 0;
                $display_days[] = $day_disp_obj;
                continue;
            }
            foreach ($calls as $call) {
                $start = strtotime($call['start_time']);
                $end = strtotime($call['end_time']);
                $diff = ($end - $start) / 60; //Difference in minutes
                if($diff <= 0.0)
                {
                    $diff = 1440 + $diff;
                }
                $mins += $diff;
            }
            $day_disp_obj->date = $day;
            $day_disp_obj->calls = count($calls);
            $day_disp_obj->hours = $mins / 60;
            $mins = 0;
            $display_days[] = $day_disp_obj;
        }
        return $display_days;
    }
    /*
    public function getDisplaySelectByWeek($user_id)
    {
        $display_weeks = array();
        $week_disp_obj = new week_display_object();
        $mins = 0;

        $weeks = Yii::app()->db->createCommand()
            ->select('week_commencing')
            ->from('timesheets')
            ->where('user_id =:user_id', array(':user_id' => $user_id))
            ->order('week_commencing ASC')
            ->queryAll();


        $week_commencing = array();
        foreach ($weeks as $week) {
            $week_commencing[] = $week['week_commencing'];
        }
        $week_commencing = array_values(array_unique($week_commencing));

        foreach ($week_commencing as $week) {
            $days = Yii::app()->db->createCommand()
                ->select('date')
                ->from('timesheets')
                ->where(array('and', 'week_commencing =:week_commencing', 'user_id =:user_id'), array(':week_commencing' => $week, ':user_id' => $user_id))
                ->order('date ASC')
                ->queryAll();

            $dates = array();
            foreach ($days as $day) {
                $dates[] = $day['date'];
            }
            $dates = array_values(array_unique($dates));
            $callCount = 0;
            foreach ($dates as $day) {
                $calls = Yii::app()->db->createCommand()
                    ->select('minutes')
                    ->from('timesheets')
                    ->where(array('and', 'date=:date', 'user_id=:user_id'),
                        array(':date' => $day, ':user_id' => $user_id))
                    ->queryAll();

                foreach ($calls as $call) {
                    $mins += $call['minutes'];
                    $callCount++;
                }
            }
            $week_disp_obj->week_commencing = $week;
            $week_disp_obj->days = count($dates);
            $week_disp_obj->calls = $callCount;
            $week_disp_obj->hours = $mins / 60;
            $mins = 0;
            $display_weeks[] = $week_disp_obj;
            $week_disp_obj = new week_display_object();
        }
        return $display_weeks;
    }
*/

    public function getSubmittedDaysForUserInDateRange($user_id)
    {
        $dh = new date_helper();
        $dateRange = $dh->createDateRangeArray();
        $calls = array();
        $dates = array();
        foreach($dateRange as $date)
        {
            $matches = Yii::app()->db->createCommand()
                ->select('date')
                ->from('timesheets')
                ->where(array('and', 'user_id=:user_id', 'date=:date', 'submitted=:submitted'),
                    array(':user_id' => $user_id, ':date' => $date, ':submitted' => 1))
                ->queryAll();
            foreach($matches as $match)
            {
                array_push($calls, $match);
            }
        }
        //now clean out duplicates.
        foreach($calls as $call)
        {
                array_push($dates,$call['date']);
        }
        $calls = array_unique($dates);
        $calls = array_values($calls);
        return $calls;
    }


    public function isSubmitted($date)
    {
        $subbed = SubmittedDays::model()->findByAttributes(array('date'=>$date));
        if (count($subbed)>0)
            return true;
        else
            return false;
    }
    public function isUserCallDaySubmitted($date)
    {
        $call = Timesheets::model()->find('user_id=:user_id and date=:date',array(':user_id'=>Yii::app()->user->id, ':date'=>$date));
        if(count($call) > 0)
        {
            if($call->submitted == 1)
            {
                return true;
            }
            else{ return false;}
        }
    }
    public function isUserHoliday($user_id,$date)
    {
        $holiday = Holiday::model()->find('user_id = :user_id AND date = :date', array(':user_id'=>$user_id,':date'=>$date));
        if(empty($holiday) || is_null($holiday))
        {
            return 0;
        }
        return $holiday->id;
    }
    public function submitUserDay($user_id,$date)
    {
        $day = new SubmittedUserDays;
        $day->user_id = $user_id;
        $day->date = $date;
        $day->save();
    }
    public function isUserDaySubmitted($user_id,$date)
    {
        $day = SubmittedUserDays::model()->findByAttributes(array('user_id'=>$user_id, 'date'=>$date));
        if($day != NULL)
        {
            return true;
        }
        else { return false; }
    }
    public function submitDay($date)
    {
        $model = new SubmittedDays;
        $model->date = $date;
        $model->save($date);
    }


    public function cleanTime($time)
    {
        $time_bits = explode(':', $time);
        return $time_bits[0].':'.$time_bits[1];
    }
}
