<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 22/08/13
 * Time: 12:33
 * To change this template use File | Settings | File Templates.
 */

$format_date = new formatDateFor_helper();
$date_helper = new date_helper();
$date_range = $date_helper->createDateRangeArray();
foreach($date_range as &$date)
{
    $date = $format_date->Viewing($date);
}
unset($date);
?>
<div class="form" style="margin-left: auto; margin-right: auto;">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'daterange-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>
    <table border="1" style="width: 600px; margin-left: auto; margin-right: auto;">
        <tbody>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'start'); ?>
                <?php echo $form->dropDownList($model, 'start', $date_range);
                ?>
                <?php echo $form->error($model, 'start'); ?></td>
        </tr>
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'end'); ?>
                <?php echo $form->dropDownList($model, 'end', $date_range);
                ?>
                <?php echo $form->error($model, 'end'); ?></td>
        </tr>
        <tr>
            <td>
                <?php echo CHtml::submitButton(' Show Calls '); ?></td>
        </tr>
        </tbody>
    </table>

    <?php $this->endWidget(); ?>
    </div>