<?php

/**
 * This is the model class for table "cid_table".
 *
 * The followings are the available columns in table 'cid_table':
 * @property integer $id
 * @property string $ident
 * @property string $property_no
 * @property string $postcode
 * @property integer $active
 * @property string $added
 * @property string $updated
 */
class CidTable extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cid_table';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ident, postcode, active, added, updated', 'required'),
			array('id, active', 'numerical', 'integerOnly'=>true),
			array('id, ident', 'length', 'max'=>11),
			array('property_no', 'length', 'max'=>5),
			array('postcode', 'length', 'max'=>7),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ident, property_no, postcode, active, added, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id' => 'Id',
			'ident' => 'Ident',
			'property_no' => 'Property No',
			'postcode' => 'Postcode',
			'active' => 'Active',
			'added' => 'Added',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

        $criteria->compare('id',$this->id,true);
		$criteria->compare('ident',$this->ident,true);
		$criteria->compare('property_no',$this->property_no,true);
		$criteria->compare('postcode',$this->postcode,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('added',$this->added,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CidTable the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
