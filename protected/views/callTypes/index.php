<?php
/* @var $this CallTypesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Call Types',
);

$this->menu=array(
	array('label'=>'Create CallTypes', 'url'=>array('create')),
	array('label'=>'Manage CallTypes', 'url'=>array('admin')),
);
?>

<h1>Call Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
