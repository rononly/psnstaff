<?php
/* @var $this DefaultController */
/* @var $model AdminNews */

$this->breadcrumbs=array(
    'Manage Admin'=>array('index'),
    'Add News',
);

?>

<?php echo $this->renderPartial('_news_form', array('model'=>$model)); ?>