<?php
/**
 * Created by PhpStorm.
 * User: Ron Appleton
 * Date: 30/09/14
 * Time: 22:00
 */
$date_helper = new formatDateFor_helper();
?>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>
<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'unsubmit-form',
        'enableAjaxValidation' => true,


    ));
    $getUserNameList = new getUserNamesList_helper();
    $users = $getUserNameList->getUserNamesList(true);?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <table>
        <tr>
            <td><?php echo $form->labelEx($model, 'staff', array('class' => 'dates')); ?>
                <?php echo $form->listBox($model, 'staff', CHtml::listData($users, 'id', 'username'),
                    array('multiple' => 'multiple', 'size' => 21, 'style' => 'width:200px')); ?>
                <?php echo $form->error($model, 'staff'); ?>
            <td><?php
                echo $form->labelEx($model, 'start_date', array('class' => 'dates'));
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'start_date',
                    'flat' => false, //remove to hide the datepicker
                    'options' => array(
                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'dateFormat' => 'yy-mm-dd',
                    ),
                    'htmlOptions' => array(
                        'class' => 'dates',
                    ),
                ));
                ?>
                <?php echo $form->error($model, 'start_date'); ?></td>
            <td><?php
                echo $form->labelEx($model, 'end_date', array('class' => 'dates'));
                $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'model' => $model,
                    'attribute' => 'end_date',
                    'flat' => false, //remove to hide the datepicker
                    'options' => array(
                        'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                        'dateFormat' => 'yy-mm-dd',
                    ),
                    'htmlOptions' => array(
                        'class' => 'dates',
                    ),
                ));
                ?><?php echo $form->error($model, 'end_date'); ?></td>
            <td style="padding-top: 16px;"><?php echo CHtml::submitButton('Submit'); ?></td>
        </tr>
    </table>

    <?php $this->endWidget(); ?>

</div><!-- form -->