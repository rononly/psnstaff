<?php

class TimesheetsController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionReportByDateRange()
    {
        $model = new DateRange();

        if (isset($_POST['DateRange'])) {
            $model->attributes = $_POST['DateRange'];
            if ($model->validate()) {
                /*
                 * Now we need to pull a list of user id's that have entries within the date range
                 * and make the array unique to only hold one reference to each user,
                 * then build an array of arrays of each users calls for the range,
                 * and then finally build a view for the data and email to admin email address.
                 */
                $timesheet_helpers = new administration_timesheet_helpers();
                $date_helper = new administration_date_helpers();
                $date_range = $date_helper->createDateRangeArray();
                //die(var_dump($date_range));
                //$date_range hold date for referencing start and end values.
                $start_date = $date_range[$model->start];
                //die($start_date);
                $end_date = $date_range[$model->end];
                //die($end_date);
                //Now we have the actual start and end dates, get the dates.
                $dates = $date_helper->createDateRangeArrayByDates($start_date, $end_date);
                //die(var_dump($dates));
                //Now $date holds all the days we are interested in,
                //So we list ALL the calls that have these dates.
                $calls = array();
                foreach ($dates as $date) {
                    $dates_calls = array();
                    $dates_calls = $timesheet_helpers->getAllCallsForDate($date);
                    foreach ($dates_calls as $call) {
                        array_push($calls, $call);
                    }
                }
                $ids = array();
                foreach ($calls as $call) {
                    array_push($ids, $call['user_id']);
                }
                $ids = array_unique($ids);
                $ids = array_values($ids);
                $carer_helpers = new administration_carer_helpers();
                $ids = $carer_helpers->ReOrderIdListBySurname($ids);
                /*now we have a sorted array of unique ids ($ids)of all staff that have used,
                 *the system within the date range with which to generate our reports.
                 */

                /*
                 * Now we need to create an array containing arrays of each persons calls
                 * within the date range!
                 */

                $carers = array();
                $carer = array();
                //Repeating ourselves but for clarity.
                /*
                 * We extend above by acquiring the calls for each carer within the date range and attach
                 * the calls array to the id within the carers array giving us an array of arrays for the
                 * display..
                 */
                $calls_to_add = array();
                foreach ($ids as $id) {
                    foreach ($dates as $date) {
                        array_push($calls_to_add, $timesheet_helpers->getDisplaySelectByCall($date, $id));
                    }
                    $carer['id'] = $id;
                    $carer['calls'] = $calls_to_add;
                    array_push($carers, $carer);
                    unset($carer);
                    $carer = array();
                    unset($calls_to_add);
                    $calls_to_add = array();
                }
                $model = new EmailReport();
                $model->start = $start_date;
                $model->end = $end_date;
                $this->render('reports/results/ByDateRange', array('carers' => $carers, 'model' => $model));
            }

        }
        if (!isset($_POST['DateRange'])) {
            $this->render('reports/ByDateRange', array('model' => $model));
        }
    }

    public function actionEmailReport()
    {
        $model = new EmailReport();
        if (isset($_POST['EmailReport'])) {
            $model->submitcalls = $_POST['EmailReport']['submitcalls'];
            $model->start = $_POST['EmailReport']['start'];
            $model->end = $_POST['EmailReport']['end'];
            if ($model->validate()) {
//Setup and send email,
//Submitting if necessary.
                $timesheet_helpers = new administration_timesheet_helpers();
                $date_helper = new administration_date_helpers();
                $dates = $date_helper->createDateRangeArrayByDates($model->start, $model->end);
                $calls = array();
                foreach ($dates as $date) {
                    $dates_calls = array();
                    $dates_calls = $timesheet_helpers->getAllCallsForDate($date);
                    foreach ($dates_calls as $call) {
                        array_push($calls, $call);
                    }
                }
                $ids = array();
                foreach ($calls as $call) {
                    array_push($ids, $call['user_id']);
                }
                $ids = array_unique($ids);
                $ids = array_values($ids);
                $carer_helpers = new administration_carer_helpers();
                $ids = $carer_helpers->ReOrderIdListBySurname($ids);
                $carers = array();
                $carer = array();
                $calls_to_add = array();
                foreach ($ids as $id) {
                    foreach ($dates as $date) {
                        array_push($calls_to_add, $timesheet_helpers->getDisplaySelectByCall($date, $id));
                    }
                    $carer['id'] = $id;
                    $carer['calls'] = $calls_to_add;
                    array_push($carers, $carer);
                    unset($carer);
                    $carer = array();
                    unset($calls_to_add);
                    $calls_to_add = array();
                }
                $html = '';
                $carer_helpers = new administration_carer_helpers();
                $call_type_helpers = new administration_call_type_helpers();
                foreach ($carers as $carer) {
                    $name = $carer_helpers->getNameById($carer['id']);
                    $name = $carer_helpers->makePrettyName($name['username']);
                    $totalHours = 0;
                    $html .= '<h2>' . $name . '</h2>';
                    $html .= '<table style="width: 500px;">';
                    $html .= '<tbody><tr><td>Date</td><td>Client ID</td><td>Call Type</td><td>Call Length</td><td>Total</td></tr>';

                    foreach ($carer['calls'] as $day) {
                        foreach ($day as $call) {
                            $html .= '<tr>';
                            $html .= '<td>' . $call['date'] . '</td>';
                            $html .= '<td>' . $call['client_initials'] . ' ' . $call['client_postcode'] . '</td>';
                            $html .= '<td>' . $call_type_helpers->lookUp($call['call_type_id']) . '</td>';
                            $html .= '<td>' . ($call['minutes'] / 60) . '</td>';
                            $totalHours += $call['minutes'];
                            $html .= '<td>' . ($totalHours / 60) . '</td>';
                            $html .= '</tr>';
                        }
                    }
                    $html .= '</tbody></table>';
                    $html .= '<h3>Total hours for month for ' . $name . ' :: ' . ($totalHours / 60) . '</h3>';
                }
                $html2pdf = Yii::app()->ePdf->HTML2PDF();
                $html2pdf->writeHTML($html);
                $contentPDF = $html2pdf->Output('', EYiiPdf::OUTPUT_TO_STRING);
                $mail = new YiiMailer();
                $mail->IsSMTP();
                $mail->Host = 'relay-hosting.secureserver.net';
                $mail->Username = '55693823';
                $mail->Password = 'COnf01pol1';

                $mail->From = 'noreply@personalsupportnetwork.org';
                $mail->FromName = 'The PSN Staff Portal';
                $mail->Subject = 'Time sheet report generated by ' . Yii::app()->user->name;
                $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
                $mail->IsHTML(true);
                $mail->MsgHTML('Please find attached the requested report for ' . date('d-m-y', strtotime($model->start)) .
                    ' to ' . date('d-m-y', strtotime($model->end)));
                $mail->AddStringAttachment($contentPDF, 'TimeSheet_Report_' . date('d-m-y', strtotime($model->start)) .
                    '_to_' . date('d-m-y', strtotime($model->end)) . '.pdf');
                $mail->AddAddress('enquiries@personalsupportnetwork.org');
                //$mail->AddAddress('ron.appleton@gmail.com');
                if (!$mail->Send()) {
                    $this->render('reports/EmailUnsuccessful');
                } else {
                    if ($model->submitcalls == 1) {
                        foreach($dates as $date)
                        {
                            $date_model = new SubmittedDays();
                            $date_model ->date = $date;
                $date_model->insert();
            }
            foreach ($carers as $carer) {
                foreach ($carer['calls'] as $day) {
                    foreach ($day as $call) {
                        $timesheet_helpers->submitCall($call['id']);
                    }
                }
            }
        }
        $this->render('reports/EmailSuccess');
        //Submit the calls in database.

    }
            }
        }
    }

    public function actionBackOneMonth()
    {
        if (isset(Yii::app()->user->month_offset)) {
            Yii::app()->user->month_offset--;
            if (Yii::app()->user->month_offset < -6) {
                Yii::app()->user->month_offset = -6;
            }
            $this->redirect(array('timesheets/reportbydaterange'));
        }
    }

    public function actionForwardOneMonth()
    {
        if (isset(Yii::app()->user->month_offset)) {
            Yii::app()->user->month_offset++;
            if (Yii::app()->user->month_offset > 0) {
                Yii::app()->user->month_offset = 0;
            }
            $this->redirect(array('timesheets/reportbydaterange'));
        }
    }
// Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
    // return the filter configuration for this controller, e.g.:
    return array(
    'inlineFilterName',
    array(
    'class'=>'path.to.FilterClass',
    'propertyName'=>'propertyValue',
    ),
    );
    }

    public function actions()
    {
    // return external action classes, e.g.:
    return array(
    'action1'=>'path.to.ActionClass',
    'action2'=>array(
    'class'=>'path.to.AnotherActionClass',
    'propertyName'=>'propertyValue',
    ),
    );
    }
    */
}