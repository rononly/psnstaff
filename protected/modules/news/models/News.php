<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $id
 * @property string $author_name
 * @property string $audience
 * @property integer $news_type
 * @property string $headline
 * @property string $story
 * @property string $date
 */
class News extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('author_name, audience, news_type, headline, story, date', 'required'),
			array('news_type', 'numerical', 'integerOnly'=>true),
			array('author_name', 'length', 'max'=>60),
			array('audience', 'length', 'max'=>30),
			array('headline', 'length', 'max'=>80),
			array('story', 'length', 'max'=>16384),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, author_name, audience, news_type, headline, story, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'author_name' => 'Author Name',
			'audience' => 'Audience',
			'news_type' => 'News Type',
			'headline' => 'Headline',
			'story' => 'Story',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('author_name',$this->author_name,true);
		$criteria->compare('audience',$this->audience,true);
		$criteria->compare('news_type',$this->news_type);
		$criteria->compare('headline',$this->headline,true);
		$criteria->compare('story',$this->story,true);
		$criteria->compare('date',$this->date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}