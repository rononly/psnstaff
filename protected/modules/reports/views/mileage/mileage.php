<?php
/* @var $this StaffenteredController */

$this->breadcrumbs=array(
	'Staff-Entered'=>array('/reports/staffentered'),
	'Mileage Report',
);
?>
<?php $this->renderPartial('_mileage_form', array('model' => $model)); ?>
