<?php

class CommunicationController extends Controller
{
	public function actionIndex()
	{
		$this->actionDashboard();
	}

    public function actionDashboard()
    {
        $this->render('dashboard');
    }
}