<?php
/* @var $this JobRolesController */
/* @var $data JobRoles */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_role')); ?>:</b>
	<?php echo CHtml::encode($data->job_role); ?>
	<br />


</div>