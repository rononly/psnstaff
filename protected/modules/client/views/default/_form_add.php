<?php
/* @var $this ClientController */
/* @var $model Client */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'client-gen_form-form',
        'enableAjaxValidation'=>false,
    )); ?>

    <?php echo $form->errorSummary($model); ?>
    <?php echo $form->hiddenField($model, 'id'); ?>
    <table class="centre" style="width: 300px;">
        <tr class="border_bottom">
            <td style="text-align: right;"><?php echo $form->labelEx($model,'initials'); ?></td>
            <td style="text-align: left;"><?php echo $form->textField($model,'initials',array('maxlength' => 3, 'size' => 5)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: left;"><?php echo $form->error($model,'initials'); ?></td>
        </tr>

        <tr class="border_bottom">
            <td style="text-align: right;"><?php echo $form->labelEx($model,'surname'); ?></td>
            <td style="text-align: left;"><?php echo $form->textField($model,'surname',array('maxlength' => 30, 'size' => 20)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: left;"><?php echo $form->error($model,'surname'); ?></td>
        </tr>

        <tr class="border_bottom">
            <td style="text-align: right;"><?php echo $form->labelEx($model,'property_number'); ?></td>
            <td style="text-align: left;"><?php echo $form->textField($model,'property_number', array('maxlength' => 4, 'size' => 5)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: left;"><?php echo $form->error($model,'property_number'); ?></td>
        </tr>

        <tr class="border_bottom">
            <td style="text-align: right;"><?php echo $form->labelEx($model,'postcode'); ?></td>
            <td style="text-align: left;"><?php echo $form->textField($model,'postcode', array('maxlength' => 7, 'size' => 10)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: left;"><?php echo $form->error($model,'postcode'); ?></td>
        </tr>

        <tr>
            <td></td>
            <td style="text-align: right;">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Add Client' : 'Update Client'); ?>
            </td>
        </tr>
    </table>

    <?php $this->endWidget(); ?>

</div><!-- form -->