<?php
/* @var $this CallTypesController */
/* @var $model CallTypes */

$this->breadcrumbs=array(
	'Call Types'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CallTypes', 'url'=>array('index')),
	array('label'=>'Create CallTypes', 'url'=>array('create')),
	array('label'=>'Update CallTypes', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CallTypes', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CallTypes', 'url'=>array('admin')),
);
?>

<h1>View Call Types #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'call_type',
	),
)); ?>
