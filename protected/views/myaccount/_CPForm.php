<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'change_password',
        'enableAjaxValidation' => false,
    )); ?>
    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->label($model, 'currentPassword'); ?>
        <?php echo $form->passwordField($model, 'currentPassword') ?>
        <?php echo $form->error($model, 'currentPassword'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'password'); ?>
        <?php echo $form->passwordField($model, 'password') ?>
        <?php echo $form->error($model, 'password'); ?>
    </div>

    <div class="row">
        <?php echo $form->label($model, 'verifyPassword'); ?>
        <?php echo $form->passwordField($model, 'verifyPassword'); ?>
        <?php echo $form->error($model, 'verifyPassword'); ?>

    </div>

    <div class="row submit">
        <?php echo CHtml::submitButton('Change Password'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->