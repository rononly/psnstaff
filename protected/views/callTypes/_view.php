<?php
/* @var $this CallTypesController */
/* @var $data CallTypes */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('call_type')); ?>:</b>
	<?php echo CHtml::encode($data->call_type); ?>
	<br />


</div>