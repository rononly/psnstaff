<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 25/08/13
 * Time: 12:03
 * To change this template use File | Settings | File Templates.
 */

class GenerateReport extends CFormModel {

    public $all_staff;
    public $staff;
    public $start_date;
    public $end_date;
    public $report_type;
    public $summary;
    public $summary_only;
    public $submit_data;

    public function rules()
    {
        return array (
            array('start_date', 'checkStartDate'),
            array('end_date', 'checkEndDate'),
        );
    }
    public function checkStartDate($attribute, $params)
    {
        if ($this->$attribute > $this->end_date)
            $this->addError($attribute, 'The start date must come before or be the same as the end date!');
    }
    public function checkEndDate($attribute, $params)
    {
        if ($this->$attribute < $this->start_date)
            $this->addError($attribute, 'The end date must come after or be the same as the start date!');
    }
}