<?php
/* @var $this ExtraController */

$this->breadcrumbs=array(
    'Mileage'=>array('/mileage/default/index'),
    'Mileage By Call'=>array('/mileage/mileage/calls&date='.$timesheet['date']),
    'Add Extra Mileage',
);
?>

    <?php
    $th = new timesheet_helpers();
    $formatter = new formatDateFor_helper();
    $callTypeLU = new getCallTypes_helper();
    echo '<table class="centre" border="1" cellpadding="1" cellspacing="1" style="width: 600px; padding-top: 10px;"><tbody>';
    echo '<tr><td>Date</td><td>Client<td>Call Time</td><td>Call Type</td><td>
                        Hours</td></tr>';
    echo '<tr>';
        echo '<td>' . CHtml::link($formatter->Viewing($timesheet['date']), array('/timesheet/default/update',
            'id' => $timesheet['id'], 'date' => $timesheet['date'])) . '</td>';
        echo '<td>' . $timesheet['client_initials'] . ' ' . $timesheet['client_postcode'] . '</td>';
        echo '<td>' . $th->cleanTime($timesheet['start_time']) . ' - ' . $th->cleanTime($timesheet['end_time']) . '</td>';
        echo '<td>' . $callTypeLU->lookUp($timesheet['call_type_id']) . '</td>';
        echo '<td>' . round($timesheet['minutes'] / 60,2) . '</td>';
        echo '</tr>';
    ?>
</table>
<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
