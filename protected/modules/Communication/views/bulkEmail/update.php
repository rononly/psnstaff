<?php
/* @var $this BulkEmailController */
/* @var $model BulkEmail */

$this->breadcrumbs=array(
	'Bulk Emails'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BulkEmail', 'url'=>array('index')),
	array('label'=>'Create BulkEmail', 'url'=>array('create')),
	array('label'=>'View BulkEmail', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage BulkEmail', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>