<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-form',
    'enableAjaxValidation' => false,
));
?>

<table class="centre" style="max-width: 400px">
    <tbody>
    <tr class="border_bottom">
        <td style="text-align: right;">Select Client</td>
        <td style="text-align: left;">
            <?php echo $form->dropDownList($model,'id',CHtml::listData($clients,'id','property_number')); ?>
        </td>
    </tr>
    <tr>
        <td></td>
        <td><?php echo CHtml::submitButton('Update Client'); ?></td></td>
    </tr>
    </tbody>
</table>
<?php $this->endWidget(); ?>