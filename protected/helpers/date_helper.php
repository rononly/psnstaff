<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of date_helper
 *
 * @author Ron
 */
class date_helper
{

    public function daysSince21st()
    {
        if(date('d') > 21)
        {
            $today = new DateTime('now');
            $s21st = new DateTime(date('Y-m-21'));
            return $today->diff($s21st)->format('%a');
        }
        if(date('d') < 21)
        {
            $today = new DateTime('now');
            $s21st = new DateTime(date('Y-m-21', strtotime('-1 month')));
            return $today->diff($s21st)->format('%a');
        }
        if(date('d') == 21)
        {
            return 0;
        }
    }

    public function daysTill20th()
    {
        if(date('d') > 20)
        {
            $today = new DateTime('now');
            $s21st = new DateTime(date('Y-m-21', strtotime('+1 month')));
            return $today->diff($s21st)->format('%a');
        }
        if(date('d') < 20)
        {
            $today = new DateTime('now');
            $s21st = new DateTime(date('Y-m-21'));
            return $s21st->diff($today)->format('%a');
        }
        if(date('d') == 20)
        {
            return 0;
        }
    }

    public function testPrevious($date)
    {
        $strDateFrom = strtotime('-' . $this->daysSince21st() . 'day', strtotime(date('Y-m-d')));

        if (strtotime($date) < $strDateFrom) {
            return true;
        }
        return false;

    }

    public function createDateRangeArray()
    {
        $month_offset = 0;
        if (isset(Yii::app()->session['month_offset'])) {
            $month_offset = Yii::app()->session['month_offset'];
        }
        $strDateFrom = strtotime('-' . $this->daysSince21st() . 'day', strtotime(date('Y-m-d')));
        $strDateTo = strtotime('+' . $this->daysTill20th() . 'day', strtotime(date('Y-m-d')));
        //die(var_dump(date('Y-m-d',$strDateFrom) . ' ' . date('Y-m-d',$strDateTo)));
        //any offset needs calculating here.
        if ($month_offset < 0) {
            $strDateFrom = strtotime($month_offset . 'month', $strDateFrom);
            $strDateTo = strtotime($month_offset . 'month', $strDateTo);
        }

        $aryRange = array();

        $iDateFrom = $strDateFrom; //mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = $strDateTo; //mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        return $aryRange;
    }

    public function createDateRangeArrayByDates($strDateFrom, $strDateTo)
    {
        $strDateFrom = strtotime($strDateFrom);
        $strDateTo = strtotime($strDateTo);

        // die($strDateFrom . '     ' . $strDateTo);
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.

        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange = array();

        $iDateFrom = $strDateFrom; //mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = $strDateTo; //mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        return $aryRange;
    }


}
?>
