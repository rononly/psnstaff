<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}
    public function actionPaperwork()
    {
        $this->render('paperwork');
    }
    public function actionDownload()
    {
        $file = $_GET['file'];
        switch($file)
        {
            case 'record':
                    EDownloadHelper::download(Yii::app()->basePath.'/..'.$this->module->assetsURL.'/docs/'.'DailyRecordSheet.docx');
                break;
                case 'mar':
                    EDownloadHelper::download(Yii::app()->basePath.'/..'.$this->module->assetsURL.'/docs/'.'MAR.docx');
                break;
                case 'timesheet':
                    EDownloadHelper::download(Yii::app()->basePath.'/..'.$this->module->assetsURL.'/docs/'.'TimeSheet.doc');
                break;
                case 'c2cmileage':
                    EDownloadHelper::download(Yii::app()->basePath.'/..'.$this->module->assetsURL.'/docs/'.'MileageSheetCallToCall.docx');
                break;
                case 'ssmileage':
                    EDownloadHelper::download(Yii::app()->basePath.'/..'.$this->module->assetsURL.'/docs/'.'MileageSheetSocialSupport.docx');
                break;
            case 'financial':
                    EDownloadHelper::download(Yii::app()->basePath.'/..'.$this->module->assetsURL.'/docs/'.'FinancialTransactionSheet.docx');
                break;

        }
    }
}