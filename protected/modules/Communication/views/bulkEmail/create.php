<?php
/* @var $this BulkEmailController */
/* @var $model BulkEmail */

$this->breadcrumbs=array(
	'Bulk Emails'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BulkEmail', 'url'=>array('index')),
	array('label'=>'Manage BulkEmail', 'url'=>array('admin')),
);
?>
Valid and available short tags :: ^{user}^ ^{user_no_format}^
<?php $this->renderPartial('_form', array('model'=>$model)); ?>