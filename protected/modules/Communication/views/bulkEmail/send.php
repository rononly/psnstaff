<?php
/* @var $this BulkEmailController */
/* @var $model BulkEmail */

$this->breadcrumbs=array(
    'Bulk Emails'=>array('index'),
    'Send',
);

$this->menu=array(
    array('label'=>'List BulkEmail', 'url'=>array('index')),
    array('label'=>'Manage BulkEmail', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_send_form', array('model'=>$model, 'data' => $data)); ?>