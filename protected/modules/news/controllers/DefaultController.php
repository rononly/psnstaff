<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
        /*
         * Get Job Role List Together For
         * audience selection.
         *
         */
        $jr_hlp = new getJobRoles_helper;
        $roles = $jr_hlp->getActiveJobRoles();
        $role_list = array();
        $role_list_index = 0;
        foreach($roles as $role)
        {
            $role_list[$role_list_index]['id'] = $role->id;
            $role_list[$role_list_index]['job_role'] = $role->job_role;
            $role_list_index++;
        }


        $model = new News();
        if(isset($_POST['News']))
        {
            $model->setAttributes($_POST['News']);
            if($model->validate())
            {
                $model->save();
            }
            $this->redirect(Yii::app()->user->returnUrl);
        }

        $model->date = date('Y-m-d');
        $unh = new getUserName_helper;
        $model->author_name = $unh->makePrettyName(Yii::app()->user->name);
        $this->render('index', array('model'=>$model, 'roles'=>$role_list));
	}

    public function actionUpdateNews($id)
    {
        /*
         * Get Job Role List Together For
         * audience selection.
         *
         */
        $jr_hlp = new getJobRoles_helper;
        $roles = $jr_hlp->getActiveJobRoles();
        $role_list = array();
        $role_list_index = 0;
        foreach($roles as $role)
        {
            $role_list[$role_list_index]['id'] = $role->id;
            $role_list[$role_list_index]['job_role'] = $role->job_role;
            $role_list_index++;
        }
        $model = $this->loadModel($id);

        if(isset($_POST['News']))
        {
            $model->attributes = $_POST['News'];
            if($model->validate())
            {
                $model->save();
            }
            $this->redirect(Yii::app()->user->returnUrl);
        }
        $this->render('index', array('model'=>$model, 'roles'=>$role_list));
    }

    public function actionMarkSeen($id)
    {
        $model = new NewsSeen();
        $model->user_id = Yii::app()->user->id;
        $model->news_id = $id;
        $model->save();
        $this->redirect(Yii::app()->request->urlReferrer);
    }

    public function loadModel($id)
    {
        $model=News::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    public function actionShowStaffNews()
    {
        $news = $this->filterSeen('staff');
        $this->render('staff_news',array('news'=>$news));
    }

    public function actionShowAdminNews()
    {
        $news = $this->filterSeen('admin');
        $this->render('admin_news',array('news'=>$news));
    }

    public function actionShowAllNews()
    {
        $news = $this->filterSeen('both');
        $this->render('all_news',array('news'=>$news));
    }

    public function filterSeen($type)
    {
        $user_id = Yii::app()->user->id;
        $news = array();
        switch($type)
        {
            case 'both':
            $news = News::model()->findAll();
            break;
            case 'staff':
                $cdbcriteria = new CDbCriteria();
                $cdbcriteria->condition = 'news_type=0';
                $news = News::model()->findAll($cdbcriteria);
                break;
            case 'admin':
                $cdbcriteria = new CDbCriteria();
                $cdbcriteria->condition = 'news_type=1';
                $news = News::model()->findAll($cdbcriteria);
                break;
        }

        $unseenNews = array();
        foreach($news as $article)
        {
            $cdbcriteria = new CDbCriteria();
            $cdbcriteria->condition = 'user_id='.$user_id;
            $cdbcriteria->condition = 'news_id='.$article->id;
            if(!NewsSeen::model()->find($cdbcriteria))
            {
                $unseenNews[] = $article;
            }
        }
        return $unseenNews;
    }
}