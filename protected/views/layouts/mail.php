<html>
<head>
	<meta content="text/html; charset=UTF-8" http-equiv="content-type">
</head>
<table style="color:#666;font:13px Arial;line-height:1.4em;width:100%;">
	<tbody>
    <tr>
        <td style="border-bottom: 1px solid lightslategray;">
            <?php echo CHtml::image('../psnlogosmltr.png', 'PSN Logo'); ?>
        </td>
    </tr>
		<tr>
            <td style="color:#777;font-size:16px;padding-top:5px;">
            	<?php if(isset($data['description'])) echo $data['description'];  ?>
            </td>
		</tr>
		<tr>
            <td>
				<?php echo $content ?>
            </td>
		</tr>
		<tr>
            <td style="padding:15px 20px;text-align:right;padding-top:5px;border-top:solid 1px #dfdfdf">
			</td>
		</tr>
	</tbody>
</table>
</body>
<footer>
    <p>The information in this email and any attachments is confidential and may be legally privileged. It is intended solely for the addressee. If you are not the intended recipient, any disclosure, copying, distribution, or transmission of the contained information and/or attachments or any other action, with exception to deletion, is prohibited and may be unlawful.</p>
</footer>
</html>