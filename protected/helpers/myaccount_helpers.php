<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 16/08/13
 * Time: 02:16
 * To change this template use File | Settings | File Templates.
 */

class myaccount_helpers
{

    public function ControllerMenu()
    {
        $menu = array(
            array('label' => 'Change Email', 'url' => array('myaccount/changeemail')),
            array('label' => 'Change Mobile', 'url' => array('myaccount/changemobile')),
            array('label' => 'Change Landline', 'url' => array('myaccount/changelandline')),
            array('label' => 'Change Password', 'url' => array('myaccount/changepassword')),
        );
        return $menu;
    }

    public function getUser($id)
    {
        $user = Yii::app()->db->createCommand()
            ->select('*')
            ->from('tbl_user')
            ->where('id=:id', array(':id' => $id))
            ->limit(1)
            ->queryRow();
        return $user;
    }

    public function getJobRole($id)
    {
        $getJobRoles = new getJobRoles_helper();
        $jobRole = $getJobRoles->userLookup($id);
        return $jobRole;
    }

    public function getPassword($id)
    {
        $password = Yii::app()->db->createCommand()
            ->select('password')
            ->from('tbl_user')
            ->where('id=:id', array(':id' => $id))
            ->limit(1)
            ->queryRow();
        return $password['password'];
    }
}