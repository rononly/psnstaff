<?php

class ReportsModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'reports.models.*',
			'reports.components.*',
            'reports.helpers.*',
            'reports.helpers.objects.*',
            'administration.helpers.*',
            'mileage.models.*',
            'mileage.helpers.*',
            'client.models.*',
            'timesheet.models.*',
            'mileage.helpers.objects.*',
            'application.helpers.*',
		));
	}
    private $_assetsUrl;

    public function getAssetsUrl()
    {
        if ($this->_assetsUrl === null)
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('reports.assets') );
        return $this->_assetsUrl;
    }
	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
