<?php
/* @var $this NewsStandController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'News Stands',
);

$this->menu = array(
    array('label' => 'Create NewsStand', 'url' => array('create')),
    array('label' => 'Manage NewsStand', 'url' => array('admin')),
);
?>

    <h1>News Stands</h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
)); ?>