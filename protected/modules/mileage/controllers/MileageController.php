<?php

class MileageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

    public function actionCalls($date)
    {
        $calls = Timesheets::model()->findAll('date=:date AND user_id=:user_id ORDER BY start_time',
            array(':date' => $date, ':user_id' => Yii::app()->user->id));

        $this->render('calls', array('calls' => $calls, 'date'=>$date));
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */

    public function actionGetipc($user_id,$date)
    {
        $ipc = Yii::app()->db->createCommand()
            ->select('client_initials, client_postcode')
            ->from('timesheets')
            ->where(array('and', 'user_id=:user_id', 'date=:date'), array(':user_id' => $user_id, ':date' => $date))
            ->order('client_initials ASC');
        $res = $ipc->queryAll();

        foreach($res as $r)
        {
            $results[] = array('value' => $r['client_initials'],
                                'client_postcode' => $r['client_postcode']
            );
        }
        $results = array_unique($results);
        echo CJSON::encode($results);
        Yii::app()->end();
    }
	public function actionCreate()
	{
		$model=new Mileage;

        //Get the $_GET['Parameters']
        $model->user_id = $_GET['user_id'];
        $model->call_ref = $_GET['call_ref'];
        $model->date = $_GET['date'];


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Mileage']))
		{
			$model->attributes=$_POST['Mileage'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Mileage']))
		{
			$model->attributes=$_POST['Mileage'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Mileage');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Mileage('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Mileage']))
			$model->attributes=$_GET['Mileage'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Mileage the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Mileage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Mileage $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='mileage-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
