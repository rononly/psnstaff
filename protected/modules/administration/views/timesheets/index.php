<?php
/* @var $this TimesheetsController */

$this->breadcrumbs=array(
    'Reports' => array('/administration/default/index'),
	'Time Sheet Reports',
);
$carersImage = CHtml::image($this->module->assetsURL . '/images/id_card_preferences.png', "By Carer");
$clientsImage = CHtml::image($this->module->assetsURL . '/images/user1_preferences.png', 'By Client');
$rangeImage = CHtml::image($this->module->assetsURL . '/images/table_selection_row.png', 'By Date Range');
?>
<style type="text/css" media="screen">
    td.boxify {
        height: 70px;
        min-width: 96px;
        max-width: 96px;
        border: 1px solid #d0d2d0;
        text-align: center;
        -moz-border-radius: 5px;
        border-radius: 5px;
        border-spacing: 10px;
        -moz-box-shadow: 2px 2px 5px #888;
        -webkit-box-shadow: 2px 2px 5px #888;
        box-shadow: 2px 2px 5px #888;
    }
    td.label {
        width: 100px;
        font-family: Roboto;
        text-align: center;
        border-bottom: 1px solid #139ff7;
    }
    table {
        margin-left: auto;
        margin-right: auto;
        -moz-border-radius: 15px;
        border-radius: 15px;
        width: 420px;
    }
    tr.section_header {
        text-align: center;
        font-weight: bolder;
        font-size: larger;
        text-decoration-line: underline;
    }
</style>

<table><tbody>
    <tr class="section_header"><td>Reports</td><td></td><td></td><td></td></tr>
    <tr>
        <td class="boxify"><?php echo CHtml::link($rangeImage,array('timesheets/reportbydaterange')); ?></td>
        <td class="boxify"><?php echo CHtml::link($clientsImage,array('reports/byclients'),array('onclick'=>'return false;','style'=>'opacity:0.4;
filter:alpha(opacity=40);')); ?></td>
        <td class="boxify"><?php echo CHtml::link($carersImage,array('reports/bycarers'),array('onclick'=>'return false;','style'=>'opacity:0.4;
filter:alpha(opacity=40);')); ?></td>

        <td class="boxify"></td>
    </tr>

    <tr>
        <td class="label">By Date Range</td>
        <td class="label">By Clients</td>
        <td class="label">By Carers</td>
        <td class="label"></td></tr>
    <tr></tr>

    </tbody>
    </table>