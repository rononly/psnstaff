<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ChangePassword
 *
 * @author Ron
 */
class ChangeEmail extends CFormModel
{

    public $email;

    public function rules()
    {
        //TO SORT
        return array(
            array('email', 'email', 'checkMX' => true, 'allowEmpty' => false),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'email' => 'New Email Address',
        );
    }


}
?>
