<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of getUserList_helper
 *
 * @author Ron
 */
class getUserList_helper
{

    public function getUserList($audience)
    {
        //Find users in the db that match the audience via there title.
        $users = Yii::app()->db->createCommand()
            ->select('id')
            ->from('tbl_user')
            ->where('title =:title', array(':title' => $audience))
            ->queryAll();
        //$users should now be populated with the list of corresponding
        //users to send to.
        return $users;
    }

    public function getUsersList()
    {
        $users = Yii::app()->db->createCommand()
            ->select('username', 'email')
            ->from('tbl_user')
            ->order('username ASC')
            ->queryAll();
        return $users;
    }

}

?>
