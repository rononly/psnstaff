<?php
/* @var $this MileageController */
/* @var $model Mileage */
/* @var $form CActiveForm */
/*
 * $model already contains date, user_id, call_ref, client_initials, client_postcode
 */
$mfh = new mileage_fetcher_helper();
$data_array = $mfh->fetchClientIds();
?>
<style media="screen" type="text/css">
    tr.border_bottom td {
        border-bottom: 2pt solid #e5eCf9;
        text-align: center;
    }

    tr.highlight td {
        background-color: lightgreen;
        font-size: smaller;
    }
    tr.header td {
        font-size: larger;
        font-weight: bold;
        border-bottom: 2pt solid #e5eCf9;
        text-align: center;
    }
    table.centre {
        margin-left: auto;
        margin-right: auto;
        -moz-border-radius: 15px;
        border-radius: 15px;
        background-color: #c9e0ed;
        padding-left: 0px;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-right: 4px;
        -moz-box-shadow: 10px 10px 5px #888;
        -webkit-box-shadow: 10px 10px 5px #888;
        box-shadow: 10px 10px 5px #888;
    }
</style>
<?php echo $mfh->getDistance(79,'TS80SE', 67,'TS80TT'); ?>
    <div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'mileage-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

        <p class="note">Fields with <span class="required">*</span> are required.</p>
        <?php echo $form->errorSummary($model); ?>

        <?php echo $form->hiddenField($model,'call_ref'); ?>
        <?php echo $form->hiddenField($model,'user_id'); ?>
        <?php echo $form->hiddenField($model,'date'); ?>

        <table class="centre" border="1" style="width: 550px;"><tbody>
            <tr class="header">
                <td>From</td><td></td><td>To</td><td>Call 2 Call</td>
            </tr>
            <tr class="border_bottom">
                <td>
                    <?php
                    echo $form->dropDownList($model,'client_id',$data_array);
                    ?>
                </td>
                <td>->->-></td>
                <td><?php echo $form->textField($model,'client_initials_1',
                        array('size'=>3,'maxlength'=>3, 'readonly' => true)); ?>
                 &nbsp<?php echo $form->textField($model,'client_postcode_1',
                        array('size'=>8,'maxlength'=>8, 'readonly' => true)); ?></td>
                <td><?php echo $form->textField($model,'c2c_miles',array('size'=>3,'maxlength'=>3)); ?></td>
            <tr>
                <td><?php echo $form->error($model,'client_initials_2'); ?>
                &nbsp<?php echo $form->error($model,'client_postcode_2'); ?></td>
            <td></td>
            <td></td>
            <td><?php echo $form->error($model,'c2c_miles'); ?></td>
                </tr>
            <tr class="header">
                <td>From</td><td></td><td>To</td><td>Social Support</td>
            </tr>
            <tr class="border_bottom">
                <td><?php echo $form->textField($model,'client_initials_1',
                        array('size'=>3,'maxlength'=>3, 'readonly' => true)); ?>
                    &nbsp<?php echo $form->textField($model,'client_postcode_1',
                        array('size'=>8,'maxlength'=>8, 'readonly' => true)); ?></td>
                <td>->->-></td>
                <td>Social Support</td>
                <td><?php echo $form->textField($model,'ss_miles',array('size'=>3,'maxlength'=>3)); ?></td>
            </tr>
            <tr>
            <td></td><td></td><td></td><td><?php echo $form->error($model,'ss_miles'); ?></td>
            </tr>
            </tbody></table>
        <table class="centre" border="1" style="width: 550px;"><tbody>
            <tr class="header">
                <td><?php echo $form->labelEx($model,'ss_description'); ?></td><td></td>
            </tr>
            <tr class="border_bottom">
                <td><?php echo $form->textArea($model,'ss_description',array('size'=>60,'maxlength'=>1024,
                    'cols' => 59, 'rows' => 5, 'spellcheck' => "true",
                    'placeholder' => 'Tell us where you went, what you did.')); ?></td>
                </tr>
            <tr>
                <td><?php echo $form->error($model,'ss_description'); ?></td>
            </tr>
            <tr class="border_bottom">
                <td><?php echo CHtml::submitButton($model->isNewRecord ? 'Add Miles' : 'Save'); ?></td>
            </tr>

            </tbody></table>

            <?php $this->endWidget(); ?>

    </div><!-- form -->
