<?php
/* @var $this BulkEmailController */
/* @var $model BulkEmail */
/* @var $form CActiveForm */ #
$debug = true;
?>
<style type="text/css" media="screen">
    div.loading {
        background-color: #eee;
        background-image: url('/images/500.GIF');
        background-position: center center;
        background-repeat: no-repeat;
        opacity: 1;
    }

    div.loading * {
        opacity: .8;
    }
</style>
<script type="text/javascript">
    window.onload = function () {
        allStaffCheckboxEnable("TimeSheet");
        checkAllStaffCheckBox("TimeSheet");
        staffListBoxDisable("TimeSheet");
    }
    $(document).ajaxStart(function () {
        $("#form-page").addClass("loading");
    });

    $(document).ajaxStop(function () {
        $("#form-page").removeClass("loading");
        window.location.href = "index.php?r=reports/staffentered/DisplayReport";

    });
</script>
<div class="form" id="form-page">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'generate-report-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));

    $getUserNameList = new getUserNamesList_helper();
    $users = $getUserNameList->getUserNamesList(true);
    ?>

    <?php echo $form->errorSummary($model); ?>
    <table id="outer_table" class="centre">
        <tr>
            <td id="left" style="width: 210px">
                <?php echo $form->labelEx($model, 'staff', array('class' => 'staff')); ?>
                <?php echo $form->listBox($model, 'staff', CHtml::listData($users, 'id', 'username'),
                    array('multiple' => 'multiple', 'size' => 21, 'style' => 'width:200px')); ?>
                <?php echo $form->error($model, 'staff'); ?>
            </td>
            <td id="right" style="width: 390px">
                <table id="right-outer-table">
                    <tr>
                        <td>
                            <table>
                                <tr class="border_bottom2">
                                    <td style="width: 38px">
                                        <?php echo $form->labelEx($model, 'all_staff', array('class' => 'allStaff')); ?>
                                    </td>
                                    <td>
                                        <?php echo $form->checkBox($model, 'all_staff', array('onClick' => 'toggleListBox("TimeSheet")')); ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                                    <td>
                                    </td>
                                </tr>
                                <tr class="border_bottom2">
                                    <td style="width: 100px"><?php
                                        echo $form->labelEx($model, 'start_date', array('class' => 'dates'));
                                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                            'model' => $model,
                                            'attribute' => 'start_date',
                                            'flat' => false, //remove to hide the datepicker
                                            'options' => array(
                                                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                                                'dateFormat' => 'yy-mm-dd',
                                            ),
                                            'htmlOptions' => array(
                                                'class' => 'dates',
                                            ),
                                        ));
                                        ?></td>
                                    <td style="width: 100px"><?php
                                        echo $form->labelEx($model, 'end_date', array('class' => 'dates'));
                                        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                            'model' => $model,
                                            'attribute' => 'end_date',
                                            'flat' => false, //remove to hide the datepicker
                                            'options' => array(
                                                'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                                                'dateFormat' => 'yy-mm-dd',
                                            ),
                                            'htmlOptions' => array(
                                                'class' => 'dates',
                                            ),
                                        ));
                                        ?></td>
                                </tr>
                                <tr class="border_bottom">
                                    <td style="width: 40px;"><?php echo $form->labelEx($model, 'submit_data', array('class' => 'allStaff')); ?><?php echo $form->checkBox($model, 'submit_data', array('value' => 1, 'uncheckValue' => 0)); ?></td>
                                    <?php if ($debug) {
                                        ?>
                                        <td style="text-align: left"><?php echo CHtml::submitButton('Generate Time Sheet Report (Debug)'); ?></td>
                                    <?php } else { ?>
                                        <td style="text-align: left"><?php echo CHtml::ajaxSubmitButton('Generate Time Sheet Report', '#'); ?></td>
                                    <?php } ?>
                                </tr>
                    <tr>

                    </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>

                        <td>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <?php $this->endWidget(); ?>
</div><!-- form -->