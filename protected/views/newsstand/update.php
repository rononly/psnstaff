<?php
/* @var $this NewsStandController */
/* @var $model NewsStand */

$this->breadcrumbs=array(
	'News Stands'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List NewsStand', 'url'=>array('index')),
	array('label'=>'Create NewsStand', 'url'=>array('create')),
	array('label'=>'View NewsStand', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage NewsStand', 'url'=>array('admin')),
);
?>

<h1>Update News Stand <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>