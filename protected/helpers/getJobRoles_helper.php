<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of getJobRoles_helper
 *
 * @author Ron
 */
class getJobRoles_helper
{

    public function getJobRoles()
    {
        $roles = JobRoles::model()->findAll(array('order' => 'job_role ASC'));
        return $roles;
    }
    public function getJobRolesAssignment()
    {
        $roles = JobRoles::model()->findAll(array('order' => 'job_role ASC'));
        $allowed = array();
        foreach($roles as $role)
        {
            $allowed[] = array('id'=>$role->id, 'job_role'=>$role->job_role);
        }
        $rbac_helper = new RBAC_helper();
        if($rbac_helper->is('Administrator'))
            return $allowed;
        if($rbac_helper->is('Manager'))
        {
            $allowed = $this->removeElementWithValue($allowed,'job_role','Administrator');
            return $allowed;
        }
        if($rbac_helper->is('Coordinator'))
        {
                $allowed = $this->removeElementWithValue($allowed,'job_role','Administrator');
                $allowed = $this->removeElementWithValue($allowed,'job_role', 'Manager');
                $allowed = $this->removeElementWithValue($allowed,'job_role','Coordinator');
            return $allowed;
        }
        if($rbac_helper->is('Office Admin'))
        {
            $allowed = $this->removeElementWithValue($allowed,'job_role','Administrator');
            $allowed = $this->removeElementWithValue($allowed,'job_role', 'Manager');
            $allowed = $this->removeElementWithValue($allowed,'job_role','Coordinator');
            $allowed = $this->removeElementWithValue($allowed,'job_role','Office Admin');
            return $allowed;
        }
        return $allowed;
    }
    public function getActiveJobRoles()
    {
        $assignments = JobAssignments::model()->findAll();
        $roles = array();
        foreach($assignments as $ass)
        {
            $roleModel = JobRoles::model()->findByPk($ass->job_role_id);
            $roles[$roleModel->id] = $roleModel->job_role;
        }
        ksort($roles);
        $filtered = array();
        foreach($roles as $key => $value)
        {
            $rle = new role();
            $rle->id = $key;
            $rle->job_role = $value;
            $filtered[] = $rle;
        }
        return $filtered;
    }

    public function lookUp($id)
    {
        $jobRole = Yii::app()->db->createCommand()
            ->select('job_role')
            ->from('job_roles')
            ->where('id=:id', array(':id' => $id))
            ->queryRow();
        return $jobRole['job_role'];
    }
    public function lookUpId($id)
    {
        $jobRole = Yii::app()->db->createCommand()
            ->select('job_role')
            ->from('job_roles')
            ->where('id=:id', array(':id' => $id))
            ->queryRow();
        return $jobRole['id'];
    }
    public function lookUpUserJobId($id)
    {
        $jobRole = JobAssignments::model()->findByAttributes(array('user_id'=>$id));
        return $jobRole->job_role_id;
    }

    public function userLookup($user_id)
    {
        $jobRoles = new getJobRoles_helper();
        //Returns Users Job Role via getting job_role_id and then looking up role
        $jobRole = Yii::app()->db->createCommand()
            ->select('job_role_id')
            ->from('job_assignments')
            ->where('user_id=:id', array(':id' => $user_id))
            ->queryRow();
        return $jobRoles->lookUp($jobRole['job_role_id']);
    }
    private function removeElementWithValue($array, $key, $value){
        foreach($array as $subKey => $subArray){
            if($subArray[$key] == $value){
                unset($array[$subKey]);
            }
        }
        return $array;
    }
}

?>
