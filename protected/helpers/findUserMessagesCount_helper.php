<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of findUserMessagesCount_helper
 *
 * @author Ron
 */
class findUserMessagesCount_helper
{
    public function findUserMessagesCount()
    {
        $id = Yii::app()->user->id;
        $messages = Yii::app()->db->createCommand()
            ->select('*')
            ->from('messages')
            ->where('recipient_id=:recipient_id', array(':recipient_id' => $id))
            ->queryAll();
        return count($messages);
    }
}

?>
