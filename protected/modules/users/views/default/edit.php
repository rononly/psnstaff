<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 18/01/14
 * Time: 21:15
 * @users = all users stored.
 */
$this->breadcrumbs=array(
    'Manage Users'=>array('index'),
    'Edit User',
);
?>
<?php $compare = new SelectUser; ?>
    <?php if($model == $compare) {?>
    <?php echo $this->renderPartial('_selectUser', array('model'=>$model,'users'=>$users)); ?>
<?php } else { //Is The Edit Form ?>
    <?php echo $this->renderPartial('_form', array('model'=>$model,'update'=>true)); ?>
<?php }?>