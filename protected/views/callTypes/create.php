<?php
/* @var $this CallTypesController */
/* @var $model CallTypes */

$this->breadcrumbs=array(
	'Call Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CallTypes', 'url'=>array('index')),
	array('label'=>'Manage CallTypes', 'url'=>array('admin')),
);
?>

<h1>Create Call Types</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>