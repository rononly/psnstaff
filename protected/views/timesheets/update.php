<?php
/* @var $this TimesheetsController */
/* @var $model Timesheets */

$this->breadcrumbs = array(
    'Time Sheets' => array('selecttimesheetday'),
    'View By Day' => array('selecttimesheetday'),
    'Update',
);
$this->menu = $menu;
$formatter = new formatDateFor_helper();
?>

<h1><?php echo $formatter->Viewing($date) . '    (' . date("l jS F Y", strtotime($date)) . ')' ?></h1>
<h3>Update Time Sheet</h3>

<?php echo $this->renderPartial('_day_form', array('model' => $model, 'date' => $date)); ?>