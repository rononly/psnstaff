<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
/* @var $roles */
$jr_help = new getJobRoles_helper;
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-_news_form-form',
	'enableAjaxValidation'=>false,
)); ?>
    <?php echo $form->hiddenField($model,'id'); ?>
    <?php echo $form->hiddenField($model,'author_name'); ?>
    <?php echo $form->hiddenField($model,'date'); ?>

<?php if($form->errorSummary($model))
{?>
<table class="centre" style="width: 600px">
<tr>
    <td><?php echo $form->errorSummary($model); ?></td>
</tr>
    </table>
<?php } ?>
    <table class="centre" style="width: 400px">
    <tr class="border_bottom">
        <td><?php echo $form->labelEx($model,'audience'); ?></td>
        <td style="text-align: left;"><?php echo $form->labelEx($model,'news_type'); ?></td>
    </tr>
    <tr class="border_bottom">
        <td><?php echo $form->listBox($model,'audience', CHtml::listData($roles,'id','job_role'),array('multiple'=>'multiple','size'=>count($roles))); ?></td>
        <td style="text-align: left;"><?php echo $form->radioButtonList($model,'news_type',array('0'=>'Staff', '1'=>'Admin')); ?></td>
    </tr>
	<tr>
        <td><?php echo $form->error($model,'audience'); ?></td>
        <td><?php echo $form->error($model,'news_type'); ?></td>
	</tr>
    </table>
<table class="centre" style="width: 600px">
    <tr class="border_bottom">
        <td><?php echo $form->labelEx($model,'headline'); ?></td>
    </tr>
    <tr class="border_bottom">
        <td><?php echo $form->textField($model,'headline',array('size'=>90,'maxlength'=>80)); ?></td>
    </tr>
    <tr>
        <td><?php echo $form->error($model,'headline'); ?></td>
    </tr>
    </table>
<table class="centre" style="width: 600px">
    <tr class="border_bottom">
        <td><?php echo $form->labelEx($model,'story'); ?></td>
    </tr>
    <tr class="border_bottom">
        <td><?php $this->widget('ext.editMe.widgets.ExtEditMe', array(
                'model'=>$model,
                'attribute'=>'story',
                'width'=> 580,
                'height'=> 400,
            ));?></td>
    </tr>
    <tr>
        <td><?php echo $form->error($model,'story'); ?></td>
    </tr>
    <tr class="border_bottom">
        <td><?php echo CHtml::submitButton('Publish Article'); ?></td>
    </tr>

<?php $this->endWidget(); ?>
    </table>