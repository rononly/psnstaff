<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'change_landline',
        'enableAjaxValidation' => false,
    )); ?>
    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->label($model, 'landline'); ?>
        <?php echo $form->textField($model, 'landline') ?>
        <?php echo $form->error($model, 'landline'); ?>
    </div>

    <div class="row submit">
        <?php echo CHtml::submitButton('Change Landline'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->