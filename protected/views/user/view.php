<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'View',
);

$this->menu = array(
    array('label' => 'List User', 'url' => array('index')),
    array('label' => 'Create User', 'url' => array('create')),
    array('label' => 'Update User', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete User', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage User', 'url' => array('admin')),
);
?>

    <h1>View User #<?php echo $model->id; ?></h1>

<?php
if ($model->mobile && $model->landline) {
    $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            'id',
            'username',
            'email',
            'mobile',
            'landline',
        ),
    ));
} else if ($model->mobile && !$model->landline) {
    $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            'id',
            'username',
            'email',
            'mobile',
        ),
    ));
} else if (!$model->mobile && $model->landline) {
    $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            'id',
            'username',
            'email',
            'landline',
        ),
    ));
} else if (!$model->mobile && !$model->landline) {
    $this->widget('zii.widgets.CDetailView', array(
        'data' => $model,
        'attributes' => array(
            'id',
            'username',
            'email',
        ),
    ));
}
?>