<?php
/* @var $this ClientController */

$this->breadcrumbs=array(
	'Client'=>array('/administration/client'),
	'Add',
);
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>