<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 25/08/13
 * Time: 12:03
 * To change this template use File | Settings | File Templates.
 */

class SendBulkEmail extends CFormModel {

    public $email_id;
    public $name;
    public $description;
    public $recipients;
    public $subject;

    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('subject', 'required'),
            array('recipients', 'required', 'message' => 'You must select at least one recipient.'),
            array('subject', 'length', 'max' => 80),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, username, password, email, mobile, landline', 'safe', 'on' => 'search'),
        );
    }
}