<?php /* @var $this Controller */
$help = new getUserName_helper();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <meta name="robots" content="noindex">

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css"
          media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css"
          media="print"/>
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css"
          media="screen, projection"/>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css"/>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/tables.css"/>
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

    <div id="header">
        <div id="logoimg" width="120px" height="100px"></div>
        <div id="logo"><?php //echo CHtml::encode(Yii::app()->name); ?></div>
    </div>
    <!-- header -->

    <div id="mainmenu">
        <?php
        $rbh = new RBAC_helper;
        if($rbh->is('Authority') || $rbh->is('Administrator') || $rbh->is('Coordinator')
        || $rbh->is('Manager') || $rbh->is('Office Admin'))
        {
            $home = array('/news/default/showallnews');
        }
        else{
            if($rbh->is('Team Leader') || $rbh->is('Support Worker') || $rbh->is('Activities Organizer'))
            {
                $home = array('/news/default/showstaffnews');
            }
            else{
                $home = array('/site/index');
            }
        }
        $this->widget('zii.widgets.CMenu', array(
            'items' => array(
                array('label' => 'Home', 'url' => $home),
                array('label' => 'Login', 'url' => array('/site/login'), 'visible' => Yii::app()->user->isGuest),
                array('label' => 'Logout (' . $help->makePrettyName(Yii::app()->user->name) . ')', 'url' => array('/site/logout'),
                    'visible' => !Yii::app()->user->isGuest),
                array('label' => 'My Account', 'url' => array('/myaccount/index'), 'visible' => !Yii::app()->user->isGuest),
                array('label' => 'Help and Support', 'url' => array('/help/default/index')),
            ),
        ));
        ?>
    </div>
    <!-- mainmenu -->
    <?php if (isset($this->breadcrumbs)): ?>
        <?php
                $this->widget('zii.widgets.CBreadcrumbs', array(
                    'links' => $this->breadcrumbs,
                ));
                ?><!-- breadcrumbs -->
    <?php endif ?>

    <?php echo $content; ?>

    <div class="clear"></div>

    <div id="footer">
        Copyright &copy; <?php echo date('Y'); ?> The Personal Support Network.<br/>
        All Rights Reserved.<br/>
        <?php //echo Yii::powered();  ?>
        <div style="text-align: left;">
        </div>
    </div>
    <!-- footer -->

</div>
<!-- page -->


</body>
</html>