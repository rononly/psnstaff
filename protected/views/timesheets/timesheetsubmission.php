<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 22/08/13
 * Time: 15:04
 * To change this template use File | Settings | File Templates.
 */

$this->breadcrumbs = array(
    'Time Sheets' => array('selecttimesheetday'),
    'Choose Submission Dates' => array('submitcalls'),
    'Submit Time Sheet');
/*
 * Add Operations Menu
 */
$this->menu = $menu;
?>
    <style media="screen" type="text/css">
        tr.border_bottom td {
            border-bottom: 2pt solid #e5eCf9;
        }
        tr.submitted td {
            background-color: lightgreen;
            font-size: smaller;
        }
        tr.submitting td {
            background-color: lightgoldenrodyellow;
            font-weight: bold;
        }
    </style>
<?php
// Sheet to display all calls and totals for a full week
$helpers = new timesheet_helpers();
$submissionDays = $helpers->getSubmittedDaysForUserInDateRange(Yii::app()->user->id);
$formatter = new formatDateFor_helper();
if ($data == null) {
    echo '<h1>No Time Sheets</h1>';
    echo '<p>You have not yet created any time sheets that you can view or submit,</br>
                please use the operations menu to "Create a Time Sheet".</p>';
} else {

    //Create another array to store the IDs of the calls we will be submitting.
    $call_ids = array();
    $output = '<h1>' . Yii::app()->user->name . '</h1>';
    $output .= '<table border="0" cellpadding="1" cellspacing="1" style="width: 600px;"><tbody>';
    $output .= '<tr><td>Date</td><td>Indentifier<td>Hours</td><td>Total Hours</td><td>Status</td></tr>';
    $totalHours = 0;
    foreach ($data as $call) {
        $submitted = in_array($call['date'],$submissionDays);
        if(!$submitted)
        {
            array_push($call_ids, $call['id']);
        }
        if($submitted)
        {
            $output .= '<tr class="submitted">';
        }
        else {
            $output .= '<tr class="submitting">';
        }
        $output .= '<td>' . $call['date'] . '</td>';
        $output .= '<td>' . $call['client_initials']. ' ' . $call['client_postcode'] . '</td>';
        $output .= '<td>' . ($call['minutes']/60)  . '</td>';
        $totalHours += $call['minutes'];
        $output .= '<td>' . ($totalHours/60)  . '</td>';
        if($submitted)
        {
            $output .= '<td>SUBMITTED</td>';
        }
        else{
            $output .= '<td>WILL SUBMIT</td>';
        }

        $output .= '</tr>';
    }
    $output .= '</table>';
    $output .= '<h3>' . 'Total hours submitted on this sheet = ' . $totalHours / 60 . '</h3>';
    echo $output;
    $_SESSION['submit_mail'] = array(
        'to' => 'ron.appleton@gmail.com',
        'week' => $_GET['week'],
        'message' => $output,
    );
}?>

    <h4>By clicking below, you agree and understand that you can no longer edit the days being submitted.</h4>
    <?php
    if(empty($call_ids)){
        $this->redirect(array('nocalls'));
    }
$ids = implode(',', $call_ids);
    $submitImage = CHtml::image('images/submit.png', 'Edit week commencing');
?><p style="text-align: center;"> <?php
    echo CHtml::link($submitImage, '#',
        array("submit" => array('submit', 'ids' => $ids),'confirm' => 'Are you sure you want to submit these dates ?'));
?></p>