<?php
/**
 * Created by PhpStorm.
 * User: PSN2
 * Date: 14/01/14
 * Time: 12:02
 */
$this->breadcrumbs = array(
    'Resources',
    'Paperwork',);
?>
<table class="centre" style="width: auto;">
    <thead>
    <comment style="text-align: center;font-weight: bold;"><p>Please Note: The paperwork is here to download should you need it at short notice,
                are unable to come to the office and have a printer available to use.</p>
            <p>Please feel free to come into the office anytime to acquire fresh paperwork.</p>
    <p>Also if you cannot open the files, you can download the Microsoft Word&reg; Viewer from here
        <a href="http://www.microsoft.com/en-gb/download/confirmation.aspx?id=4" target="_blank">Word Viewer</a></a></p></comment>
    </thead>
    <tbody>
    <tr class="border_bottom">
        <td style="text-align: right;">Daily Record Sheet</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'record',
                'caption'=>'Download',
                'options'=>array('icons'=>'js:{primary:"ui-icon-circle-arrow-s"}'),
                'url'=>$this->createUrl('/resources/default/download', array('file'=>'record')),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Medical Administration Record</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'mar',
                'caption'=>'Download',
                'options'=>array('icons'=>'js:{primary:"ui-icon-circle-arrow-s"}'),
                'url'=>$this->createUrl('/resources/default/download', array('file'=>'mar')),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Time Sheet</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'timesheet',
                'caption'=>'Download',
                'options'=>array('icons'=>'js:{primary:"ui-icon-circle-arrow-s"}'),
                'url'=>$this->createUrl('/resources/default/download', array('file'=>'timesheet')),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Call to Call Mileage Sheet</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'c2cmileage',
                'caption'=>'Download',
                'options'=>array('icons'=>'js:{primary:"ui-icon-circle-arrow-s"}'),
                'url'=>$this->createUrl('/resources/default/download', array('file'=>'c2cmileage')),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Social Support Mileage Sheet</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'ssmileage',
                'caption'=>'Download',
                'options'=>array('icons'=>'js:{primary:"ui-icon-circle-arrow-s"}'),
                'url'=>$this->createUrl('/resources/default/download', array('file'=>'ssmileage')),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Financial Transaction Sheet</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'financial',
                'caption'=>'Download',
                'options'=>array('icons'=>'js:{primary:"ui-icon-circle-arrow-s"}'),
                'url'=>$this->createUrl('/resources/default/download', array('file'=>'financial')),
            ));
            ?></td>
    </tr>
    </tbody>
</table>