<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 20/03/14
 * Time: 12:05
 */
?>
<table class="centre">
    <tr class="header">
        <td>Timesheet ID</td>
        <td>Client Initials and Postcode</td>
        <td>Pick Match</td>
        <td>Confirm</td>
    </tr>
<?php
foreach($timesheets as $timesheet)
{
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
    ));
    echo $form->hiddenField($model, 'id', array('value'=>$timesheet->id));
    ?>
    <tr class="border_bottom">
        <td><?php echo $timesheet->id?></td>
        <td><?php echo $timesheet->client_initials.' '.$timesheet->client_postcode?></td>
        <td><?php echo $form->dropDownList($model,'to_id',CHtml::listData($idents,'id','ident'));?></td>
        <td><?php echo CHtml::submitButton('Confirm');?></td>
    </tr>

      <?php
    $this->endWidget();
}
?>
</table>