<?php
/* @var $this MyaccountController */
/* @var $model ChangePassword */

$this->breadcrumbs = array(
    'My Account' => array('/myaccount'),
    'Change Email',
);
$mh = new myaccount_helpers();
$this->menu = $mh->ControllerMenu();
?>

    <h1>Change Email</h1>

<?php echo $this->renderPartial('_CEForm', array('model' => $model)); ?>