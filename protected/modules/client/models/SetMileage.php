<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 20/03/14
 * Time: 01:59
 */
class SetMileage extends CFormModel {

    public $from_id;
    public $to_id;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels()
    {
        return array(
            'from_id' => 'From',
            'to_id' => 'To',
        );
    }
}