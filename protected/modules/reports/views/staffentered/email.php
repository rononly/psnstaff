<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 04/01/14
 * Time: 01:54
 */
?>
<style>
    .bigger td {
        font-size: 16px;
    }
</style>
<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'email_report-form',
        'enableAjaxValidation' => false,
    ));
    ?>
    <table style="width: 300px;" class="centre">
        <tbody>
        <tr class="bigger border_bottom">
            <td>
                <strong>Email Report</strong>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr class="border_bottom">
            <td style="text-align: right">
                <?php echo $form->labelEx($model, 'email_to'); ?>
            </td>
            <td style="text-align: left">
                <?php echo $form->textField($model, 'email_to'); ?></td>
            <td></td>
        </tr>
        <tr class="border_bottom">
            <td></td>
            <td style="text-align: left;">
                <?php echo $form->error($model, 'email_to'); ?>
            </td>
            <td></td>
        </tr>
        <tr class="border_bottom">
            <td style="text-align: right">
                <?php echo $form->labelEx($model, 'submit_calls'); ?>
            </td>
            <td style="text-align: left">
                <?php echo $form->checkBox($model, 'submit_calls', array('value' => 'Y', 'uncheckValue' => 'N')); ?>
            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: left"><input type="submit" value="Email Report"></td>
            <td></td>
        </tr>
        </tbody>
    </table>
    <?php $this->endWidget(); ?>
</div>