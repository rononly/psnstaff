<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 18/01/14
 * Time: 21:41
 */
class SelectClient extends CFormModel {
    public $id;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
        );
    }
}