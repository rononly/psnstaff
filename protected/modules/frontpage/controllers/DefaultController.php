<?php

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'date DESC , id DESC';
        $news = FrontPageNews::model()->findAll($criteria);
        $this->render('index',array('news'=>$news));
    }

    public function actionAddNews()
    {
        $model = new FrontPageNews();
        if(isset($_POST['FrontPageNews']))
        {
            $model->setAttributes($_POST['FrontPageNews']);
            if($model->validate())
            {
                $model->save();
            }
            $this->redirect(array('/frontpage/default/index'));
        }

        $date = date('Y-m-d');
        $model->date = $date;
        $this->render('addnews', array('model'=>$model));
    }
    public function actionUpdateNews($id)
    {
        $model = $this->loadModel($id);

        if(isset($_POST['FrontPageNews']))
        {
            $model->attributes = $_POST['FrontPageNews'];
            if($model->validate())
            {
                $model->save();
            }
            $this->redirect(array('/frontpage/default/index'));
        }
        $this->render('addnews', array('model'=>$model));
    }

    public function actionMarkSeen($id)
    {

    }

    public function loadModel($id)
    {
        $model=FrontPageNews::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}