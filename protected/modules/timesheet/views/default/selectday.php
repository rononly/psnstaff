<?php
$this->breadcrumbs = array(
    'Time Sheets' => array('selecttimesheetday'),
    'View By Day',);
/*
 * Add Operations Menu
 */
$th = new timesheet_helpers();
$dh = new date_helper();
$this->menu = $menu;
//$submissionDays = $th->getSubmittedDaysForUserInDateRange(Yii::app()->user->id);
//$submissionDays = $th->getDisplaySelectByDayRange(Yii::app()->user->id);
//die(var_dump($submissionDays));
$editImage = CHtml::image('images/edit.png', 'Edit This Call', array('width'=>24,'height'=>24));
$holidayImage = CHtml::image('images/suitcase_travel.png', 'Add Holiday', array('width'=>24,'height'=>24));
$cancelHolidayImage = CHtml::image('images/suitcase_travel_cancel.png', 'Add Holiday', array('width'=>24,'height'=>24));
$runningTotal = true;

?>
<?php

$days = $dh->createDateRangeArray();
//die(var_dump($days));
$formatter = new formatDateFor_helper();
//die(var_dump($data));
if ($data == null) {
    ?>
    <h1>No Time Sheets</h1>
    <p>You have not yet created any time sheets that you can view or submit,
        please use the operations menu to "Create a Time Sheet".</p>
<?php
} else {
    ?>
    <table class="centre" border="1" style="max-width: 450px;">
        <tbody>
        <tr class="topbar">
            <td>
                <form action="index.php?r=timesheet/default/backonemonth" method="post">
                    <?php echo CHtml::imageButton('images/arrow-left.png'); ?>
                </form>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <form action="index.php?r=timesheet/default/forwardonemonth" method="post">
                    <?php echo CHtml::imageButton('images/arrow-right.png'); ?>
                </form>
            </td>
        </tr>
        <tr class="header">
            <td>Date</td>
            <td>Calls</td>
            <td>Hours</td>
            <?php
            if ($runningTotal) {
                ?>
                <td>Running Total</td>
            <?php
            }
            ?>
            <td>Status</td>
        </tr>
        <?php
        if ($runningTotal) {
            $hours = 0;
        }
        foreach ($data as $day) {
            if(strtotime($day->date) > time()){
                continue;
            }
            $submitted = $th->isUserDaySubmitted(Yii::app()->user->id,$day->date);
            $holiday = $th->isUserHoliday(Yii::app()->user->id,$day->date);
            if ($submitted && $holiday==0) {
                ?>
                <tr class="highlight">
            <?php
            } else {
                if($holiday>0)
                {
                    ?>
                    <tr class="highlight2">
                        <?php
                }
                else {
                ?>
                <tr class="border_bottom">
            <?php
                }
            }

            if ($submitted) {
                ?>
                <td><?php echo $formatter->Viewing($day->date) ?> </td>
            <?php
            } else {
                ?>
                <td><?php echo CHtml::link($formatter->Viewing($day->date), array('/timesheet/default/update',
                        'date' => $day->date)); ?></td>
            <?php
            }
            ?>
            <td><?php echo $day->calls; ?></td>
            <td><?php echo round($day->hours,2); ?></td>
            <?php
            if ($runningTotal) {
                $hours += $day->hours;
                ?>
                <td><?php echo round($hours,2); ?></td>
            <?php
            }
            if ($submitted) {
                ?>
                <td><?php echo Chtml::image('images/submitted.png'); ?></td>
            <?php
            } else {
                $editLink = CHtml::link($editImage, array('/timesheet/default/update',
                    'date' => $day->date));
                if($holiday>0)
                {
                    $holidayLink = CHtml::link($cancelHolidayImage, array('/timesheet/holiday/delete',
                        'id'=> $holiday));
                }
                else{
                    $holidayLink = CHtml::link($holidayImage, array('/timesheet/holiday/create',
                        'date'=> $day->date));
                }

                ?>
                <td><?php echo $editLink; ?><?php echo $holidayLink; ?></td>
            <?php
            }
            ?>
            </tr>
        <?php
        }
        if ($runningTotal) {
            ?>
            <tr class="total">
                <td></td>
                <td></td>
                <td>Total</td>
                <td><?php echo round($hours,2); ?></td>
                <td></td>
            </tr>
        <?php
        }
        ?>
    </table>
<?php
}
?>