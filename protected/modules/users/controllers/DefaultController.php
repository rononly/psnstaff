<?php

class DefaultController extends Controller
{
    /**
     * @var string the layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    public $menu = array(
        array('label' => 'Alter Time Sheet', 'url' => array('update', 'week' => 'false')),
        array('label' => 'Download A Time Sheet', 'url' => array('download')),
        //array('label' => 'View Old Sheets', 'url' => array('archived')),
    );

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionAdd()
    {
        $model = new User;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $uh = new getUserName_helper();
            $model->attributes = $_POST['User'];
            if ($model->save())
                Yii::app()->session['relevant_user'] = $_POST['User']['username'];
            //Assign Job Role
            $RBACJobRolesAssignment = new RBACJobRoleAssignment_helper();
            $getJobRoles = new getJobRoles_helper();
            $getUserName = new getUserName_helper();
            $jobRoleAssign = new jobRoleAssignment_helper();
            $jobRoleAssign->assignJobRole($_POST['User']['job_role'], $getUserName->lookUpID($_POST['User']['username']));
            $RBACJobRolesAssignment->assignRBACJobRole($getJobRoles->lookUp($_POST['User']['job_role']), $getUserName->lookUpID($_POST['User']['username']));
            $user = User::model()->findByAttributes(array('username' => $_POST['User']['username']));
            $smh = new SendWelcomeEmail();
            $smh->SendEmail($user, $_POST['User']['password']);
            Yii::app()->user->setFlash('success', "User ".
                $uh->makePrettyName($user->username). ' has been added successfully and welcome email sent successfully.');
            $this->redirect(array('index'));
        }

        $random = rand(2039485, 9999999);
        $model->password = $random;
        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionEdit()
    {
        $uh = new getUserName_helper();
        $users = User::model()->findAll();
        $model = new SelectUser;

        foreach ($users as &$user) {
            $user->username = $uh->makePrettyName($user->username);
        }
        if (isset($_POST['SelectUser'])) {
            $userModel = User::model()->findByPk($_POST['SelectUser']['user_id']);
            $this->render('edit', array('model' => $userModel));
        }
        if (isset($_POST['User'])) {
            $userModel = User::model()->findByPk($_POST['User']['id']);
            $userModel->setAttributes($_POST['User']);
            if ($userModel->validate()) {
                $this->updateUserModel($userModel);
                $uh = new getUserName_helper();
                Yii::app()->user->setFlash('success', "User ".
                    $uh->makePrettyName($userModel->username). ' has been successfully altered.');
                $this->redirect(array('index'));
            }
        }

        $this->render('edit', array('model' => $model, 'users' => $users));
    }

    public function actionEditJob()
    {
        $uh = new getUserName_helper();
        $users = User::model()->findAll();
        $model = new SelectUser;

        foreach ($users as &$user) {
            $user->username = $uh->makePrettyName($user->username);
        }
        if (isset($_POST['SelectUser'])) {
            $jobAssignment = JobAssignments::model()->findByAttributes(array('user_id' => $_POST['SelectUser']['user_id']));
            $this->render('editJob', array('model' => $jobAssignment));
        }
        if (isset($_POST['JobAssignments'])) {
            $jobAssignment = JobAssignments::model()->findByPk($_POST['JobAssignments']['id']);
            $jobAssignment->setAttributes($_POST['JobAssignments']);
            if ($jobAssignment->validate()) {
                $jobAssignment->update();
                Yii::app()->user->setFlash('success', 'Job role has been successfully altered.');
                $this->redirect(array('index'));
            }
        }

        $this->render('editJob', array('model' => $model, 'users' => $users));
    }
    public function actionResetPass()
    {
        $uh = new getUserName_helper();
        $users = User::model()->findAll();
        $model = new SelectUser;
        foreach ($users as &$user) {
            $user->username = $uh->makePrettyName($user->username);
        }

        if (isset($_POST['SelectUser'])) {
            $userModel = User::model()->findByPk($_POST['SelectUser']['user_id']);
            $random = rand(2039485, 9999999);
            $userModel->password = $random;
            $userModel->save();
            $smh = new SendPasswordReset();
            $sent = $smh->SendEmail($userModel, $random);
            if($sent == true){
            Yii::app()->user->setFlash('success', "Password reset for ".
                $uh->makePrettyName($userModel->username). ' and new password sent via email successfully.');
            }
            else{
                $this->MailError($sent);
            }
            $this->redirect(array('index'));
        }

        $this->render('resetPass', array('model' => $model, 'users' => $users));
    }

    public function actionRemove()
    {

    }

    public function actionList()
    {
        if(isset($_GET['view']))
        {
            switch($_GET['view'])
            {
                case 'active':
                    $cdBCriteria = new CDbCriteria();
                    $cdBCriteria->condition = 'active=1';
                    $cdBCriteria->order = 'username ASC';
                    $users = User::model()->findAll($cdBCriteria);
                    break;
                case 'inactive':
                    $cdBCriteria = new CDbCriteria();
                    $cdBCriteria->condition = 'active=0';
                    $cdBCriteria->order = 'username ASC';
                    $users = User::model()->findAll($cdBCriteria);
                    if(count($users)==0)
                    {
                        Yii::app()->user->setFlash('error','You have been returned to All user list as
                        there are no inactive users.');
                        $this->redirect(array('/user/default/list','view'=>'all'));
                    }
                    break;
                default:
                    $users = User::model()->findAll(array('order' => 'username ASC'));
                    break;
            }
        }
        else {
            $users = User::model()->findAll(array('order' => 'username ASC'));
        }

        $this->render('list', array('users' => $users));
    }


    /**
     * Performs the AJAX validation.
     * @param Timesheets $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'timesheet-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Old User Functions
     */

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);
        $model->password = 'Not for Public Viewing!';
        $this->render('view', array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }
        $model->password = null;
        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new User('search');
        $model->unsetAttributes(); // clear any communication values
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return User the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function updateUserModel($model)
    {
        User::model()->updateByPk($model->id, array(
            'email' => $model->email,
            'mobile' => $model->mobile,
            'landline' => $model->landline,
        ));
    }

    public function MailError($mail)
    {
        $this->render('mail_error', array('mail'=>$mail));
    }

    public function actionToggleActive()
    {
        $id = $_GET['id'];
        $active = $_GET['active'];

        if($active)
        {
            User::model()->updateByPk($id,array(
                'active'=>0,
            ));
        }
        else
        {
            User::model()->updateByPk($id,array(
                'active'=>1,
            ));
        }

        $this->redirect(Yii::app()->request->urlReferrer);
    }
}