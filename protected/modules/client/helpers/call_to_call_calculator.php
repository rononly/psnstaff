<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 03/03/14
 * Time: 23:32
 */

class call_to_call_calculator
{
    public function process_mileage_reference_for_creation_table()
    {
        $references_for_creation = Mileage_Reference_For_Creation::model()->findAll();
        $this->doMath($references_for_creation);
    }

    public function process_mileage_reference_for_client($client_id)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'from_client_id=:client_id
                 OR to_client_id=:client_id';
        $criteria->params = array(':client_id' => $client_id);
        $references_for_creation = Mileage_Reference_For_Creation::model()->findAll($criteria);
        $this->doMath($references_for_creation);
    }

    public function process_mileage_reference_for_client_from_and_client_to($from_id,$to_id)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'from_client_id=:from_id
                 AND to_client_id=:to_id';
        $criteria->params = array(':from_id' => $from_id, ':to_id' => $to_id);
        $references_for_creation = Mileage_Reference_For_Creation::model()->findAll($criteria);
        $this->doMath($references_for_creation);
    }

    public function doMath($references_for_creation)
    {
        foreach ($references_for_creation as $reference) {

            $shortestDistance = 0.0;
            $distancesBasedOnShortestTime = $this->obtainDistanceTime($reference);
            $distancesBasedOnShortestRoute = $this->obtainDistance($reference);
            $distancesBasedOnTrafficRoute = $this->obtainDistanceTimeWithTraffic($reference);
            if (!empty($distancesBasedOnShortestTime)) {
                foreach ($distancesBasedOnShortestTime as $distance) {
                    if ($shortestDistance == 0.0) {
                        $shortestDistance = $distance;
                        continue;
                    }
                    if ($distance < $shortestDistance) {
                        $shortestDistance = $distance;
                    }
                }
            }
            if (!empty($distancesBasedOnShortestRoute)) {
                foreach ($distancesBasedOnShortestRoute as $distance) {
                    if ($shortestDistance == 0.0) {
                        $shortestDistance = $distance;
                        continue;
                    }
                    if ($distance < $shortestDistance) {
                        $shortestDistance = $distance;
                    }
                }
            }
            if (!empty($distancesBasedOnTrafficRoute)) {
                foreach ($distancesBasedOnTrafficRoute as $distance) {
                    if ($shortestDistance == 0.0) {
                        $shortestDistance = $distance;
                        continue;
                    }
                    if ($distance < $shortestDistance) {
                        $shortestDistance = $distance;
                    }
                }
            }

            //We can now store result in call_to_call_mileage_reference table
            //and remove address combination from mileage_reference_for_creation table
            if ($shortestDistance > 0.0) {
                $c2cMRef = new Call_To_Call_Mileage_Reference();
                $c2cMRef->from_client_id = $reference->from_client_id;
                $c2cMRef->to_client_id = $reference->to_client_id;
                $c2cMRef->mileage = (float)round($shortestDistance, 1);
                if ($c2cMRef->save()) {
                    //Safe to delete mileage_reference_for_creation
                    $criteria = new CDbCriteria();
                    $criteria->condition = 'from_client_id=:from_client_id
                 AND to_client_id=:to_client_id';
                    $criteria->params = array(':from_client_id' => $reference->from_client_id,
                        ':to_client_id' => $reference->to_client_id);
                    $model = Mileage_Reference_For_Creation::model()->find($criteria);
                    $model->delete();
                }
            }
        }
    }
    public function obtainDistanceTime($addresses)
    {
        $bingKey = 'key=AtPKu-E8hgIWKz5fJH-cotVWFT12kD6SbpKp-HfJphP8VayhXSbrDA19Jzyl9mgn';
        $restRouteUrl = 'http://dev.virtualearth.net/REST/v1/Routes?';
        //waypoints from entry.
        $waypoints = 'wayPoint.1=';
        if ($addresses['from_property_number'] != null) {
            $waypoints .= $addresses['from_property_number'] . ',';
        }
        $waypoints .= $addresses['from_postcode'];
        $waypoints .= '&waypoint.2=';
        if ($addresses['to_property_number'] != null) {
            $waypoints .= $addresses['to_property_number'] . ',';
        }
        $waypoints .= $addresses['to_postcode'] . '&';
        $optimize = 'optimise=time';
        $maxSolutions = 'maxSolutions=3';
        $routePathOutput = 'routePathOutput=none';
        $distanceUnit = 'distanceUnit=mi';

        //Build Request
        $request = $restRouteUrl . $waypoints . $optimize . '&' . $maxSolutions . '&' .
            $routePathOutput . '&' . $distanceUnit . '&' . $bingKey;
        $curl = curl_init($request);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        set_time_limit(240);
        $result = curl_exec($curl);
        $dec = json_decode($result);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if ($httpCode != 200) {
        }
        if ($httpCode == 500) {
            return null;
        }
        $results = array();
        foreach ($dec->resourceSets as $rSet) {
            foreach ($rSet->resources as $rSources) {
                $results[] = $rSources->travelDistance;
            }
        }
        return $results;
    }

    public function obtainDistance($addresses)
    {
        $bingKey = 'key=AtPKu-E8hgIWKz5fJH-cotVWFT12kD6SbpKp-HfJphP8VayhXSbrDA19Jzyl9mgn';
        $restRouteUrl = 'http://dev.virtualearth.net/REST/v1/Routes?';
        //waypoints from entry.
        $waypoints = 'wayPoint.1=';
        if ($addresses['from_property_number'] != null) {
            $waypoints .= $addresses['from_property_number'] . ',';
        }
        $waypoints .= $addresses['from_postcode'];
        $waypoints .= '&waypoint.2=';
        if ($addresses['to_property_number'] != null) {
            $waypoints .= $addresses['to_property_number'] . ',';
        }
        $waypoints .= $addresses['to_postcode'] . '&';
        $optimize = 'optimise=distance';
        $maxSolutions = 'maxSolutions=3';
        $routePathOutput = 'routePathOutput=none';
        $distanceUnit = 'distanceUnit=mi';

        //Build Request
        $request = $restRouteUrl . $waypoints . $optimize . '&' . $maxSolutions . '&' .
            $routePathOutput . '&' . $distanceUnit . '&' . $bingKey;
        $curl = curl_init($request);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        set_time_limit(240);
        $result = curl_exec($curl);
        $dec = json_decode($result);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if ($httpCode != 200) {
        }
        if ($httpCode == 500) {
            return null;
        }
        $results = array();
        foreach ($dec->resourceSets as $rSet) {
            foreach ($rSet->resources as $rSources) {
                $results[] = $rSources->travelDistance;
            }
        }
        return $results;
    }

    public function obtainDistanceTimeWithTraffic($addresses)
    {
        $bingKey = 'key=AtPKu-E8hgIWKz5fJH-cotVWFT12kD6SbpKp-HfJphP8VayhXSbrDA19Jzyl9mgn';
        $restRouteUrl = 'http://dev.virtualearth.net/REST/v1/Routes?';
        //waypoints from entry.
        $waypoints = 'wayPoint.1=';
        if ($addresses['from_property_number'] != null) {
            $waypoints .= $addresses['from_property_number'] . ',';
        }
        $waypoints .= $addresses['from_postcode'];
        $waypoints .= '&waypoint.2=';
        if ($addresses['to_property_number'] != null) {
            $waypoints .= $addresses['to_property_number'] . ',';
        }
        $waypoints .= $addresses['to_postcode'] . '&';
        $optimize = 'optimise=timeWithTraffic';
        $maxSolutions = 'maxSolutions=3';
        $routePathOutput = 'routePathOutput=none';
        $distanceUnit = 'distanceUnit=mi';

        //Build Request
        $request = $restRouteUrl . $waypoints . $optimize . '&' . $maxSolutions . '&' .
            $routePathOutput . '&' . $distanceUnit . '&' . $bingKey;
        $curl = curl_init($request);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        set_time_limit(240);
        $result = curl_exec($curl);
        $dec = json_decode($result);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if ($httpCode != 200) {
        }
        if ($httpCode == 500) {
            return null;
        }
        $results = array();
        foreach ($dec->resourceSets as $rSet) {
            foreach ($rSet->resources as $rSources) {
                $results[] = $rSources->travelDistance;
            }
        }
        return $results;
    }

} 