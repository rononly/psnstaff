<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

    public function actionRBACTests($test)
    {
        switch($test)
        {
            case 'helper':
                $this->render('/rbactests/helper');
                break;
        }
    }
}