<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 18/01/14
 * Time: 21:41
 */
class SelectClientMatch extends CFormModel {

    public $from_ident;
    public $to_id;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels()
    {
        return array(
            'from_ident' => 'From Ident',
            'to_id' => 'To Client',
        );
    }
}