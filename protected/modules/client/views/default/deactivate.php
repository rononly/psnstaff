<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 02/03/14
 * Time: 15:10
 */
$this->breadcrumbs=array(
    'Manage Clients'=>array('index'),
    'Deactivate Client',
);
?>
<?php echo $this->renderPartial('_selectClient', array('model'=>$model,'clients'=>$clients)); ?>