<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
        $criteria = new CDbCriteria();
        $criteria->order = 'date DESC , id DESC';
        $news = AdminNews::model()->findAll($criteria);

        $criteria2 = new CDbCriteria();
        $criteria2->order = 'date DESC , id DESC';
        $fpnews = FrontPageNews::model()->findAll($criteria2);

		$this->render('index',array('news'=>$news, 'fpnews'=>$fpnews));
	}

    public function actionAddNews()
    {
        $model = new AdminNews();
        if(isset($_POST['AdminNews']))
        {
            $model->setAttributes($_POST['AdminNews']);
            if($model->validate())
            {
                $model->save();
            }
            $this->redirect(array('/admin/default/index'));
        }

        $date = date('Y-m-d');
        $model->date = $date;
        $this->render('addnews', array('model'=>$model));
    }
    public function actionUpdateNews($id)
    {
        $model = $this->loadModel($id);

        if(isset($_POST['AdminNews']))
        {
            $model->attributes = $_POST['AdminNews'];
            if($model->validate())
            {
                $model->save();
            }
            $this->redirect(array('/admin/default/index'));
        }
        $this->render('addnews', array('model'=>$model));
    }

    public function loadModel($id)
    {
        $model=AdminNews::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
}