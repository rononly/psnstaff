<?php
/* @var $this DefaultController */
/* @var $model Client */

$this->breadcrumbs=array(
    'Manage Clients'=>array('index'),
    'Add',
);

?>

<?php echo $this->renderPartial('_form_add', array('model'=>$model)); ?>