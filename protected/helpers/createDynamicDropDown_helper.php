<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of createDynamicDropDown_helper
 *
 * @author Ron
 */
class createDynamicDropDown_helper
{

    public function dynamicDropDownJob_Roles()
    {
        $getJobRoles = new getJobRoles_helper();
        $roles = $getJobRoles->getJobRoles();
        $output = '';
        $output .= '<select id="User_job_role">';
        foreach ($roles as $role) {
            $output .= '<option value="' . $role['id'] . '" ';
            if ($role['job_role'] == 'Support Worker') {
                $output .= 'selected="selected">';
            } else
                $output .= '>';
            $output .= $role['job_role'] . '</option>';
        }

        $output .= '</select>';
        return $output;
    }

}

?>
