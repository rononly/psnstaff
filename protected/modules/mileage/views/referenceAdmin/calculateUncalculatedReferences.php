<?php
/* @var $this ReferenceAdminController */

$this->breadcrumbs=array(
    'Reference Admin'=>array('/mileage/referenceAdmin'),
    'Calculate Uncalculated References',
);
?>
    <style type="text/css" media="screen">
        div.loading {
            background-color: #eee;
            background-image: url('images/loading2.gif');
            background-position:  center center;
            background-repeat: no-repeat;
            opacity: 1;
        }
        div.loading * {
            opacity: .8;
        }
    </style>
<?php echo Chtml::ajaxButton('Calculate', array(
    'type' => 'POST',
    'beforeSend' => 'function(){
                                    $("#form-page").addClass("loading");}',
    'complete' => 'function(){
                                    $("#form-page").removeClass("loading");}',
    'url' => CController::createUrl('mileage/ReferenceAdmin/actionCalculateUncalculated'),
    'success' => 'js:function(data){
                                    <?php $this-redirect("statistics");?>
                    }',));
?>