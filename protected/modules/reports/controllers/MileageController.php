<?php

class MileageController extends Controller
{
    private $_time_limit = 240;

	public function actionIndex()
	{
        set_time_limit($this->_time_limit);
        $model = new GenerateReport();
        if (isset($_POST['GenerateReport'])) {
            if (isset($model)) {
                $poRepModel = new PopulateReportModel;
                $model = $poRepModel->populateModel($model, 'GenerateReport', $_POST);
            }

            if ($model->Validate()) {
                $model->report_type = 'mileage';

                $date_helper = new date_helper();
                $dateRange = $date_helper->createDateRangeArrayByDates(
                    $model->start_date, $model->end_date);

                foreach ($model->staff as $carer) {
                    //Generate mileage.
                    //Get a list of the days for which calls have been entered by this user.
                    $daysWithCalls = array();
                    foreach ($dateRange as $date) {
                        $dates = Timesheets::model()->findAll('date=:date AND user_id=:user_id',
                            array(':date' => $date, ':user_id' => $carer));

                        foreach ($dates as $call) {
                            array_push($daysWithCalls, $call['date']);
                        }
                    }
                    $daysWithCalls = array_unique($daysWithCalls);
                    foreach ($daysWithCalls as $day) {
                        $fltarray = array('user_id' => $carer, 'date' => $day);
                        $conditionArr = array('order' => 'start_time ASC');
                        //$calls = Timesheets::model()->findAllByAttributes($fltarray, $conditionArr);
                        $calls = Yii::app()->db->createCommand()
                            ->select('*')
                            ->from('timesheets')
                            ->where(array('and', 'user_id=:user_id', 'date=:date'), array(':user_id' => $carer, ':date' => $day))
                            ->order('start_time ASC')
                            ->queryAll();

                        $days[$day] = $calls;
                        //die(var_export($days));
                    }
                    //Now farmed to helper to reports and mileage both sync and changes only need making to helper.
                    $calc = new mileage_calculator();
                    $calc->calculateMileage($days);

                }

                $data = array();

                foreach ($model->staff as $staff_member) {
                    $data[] = $this->fetchMileageForUserBetweenDates(
                        array(
                            'user_id' => $staff_member,
                            'dateRange' => $dateRange,
                        ));
                }

                Yii::app()->session['report_model'] = $model;
                Yii::app()->session['report_data'] = $data;

                $this->redirect(array('report'));
            }
        }
        $model->report_type = 'mileage';
        $this->render('index', array('model' => $model));
	}

    public function fetchMileageForUserBetweenDates($params)
    {
        $user_id = $params['user_id'];
        $dateRange = $params['dateRange'];

        $mileage_array = array(); // For holding our mileage data

        /**
         * First we fill the mileage array with our c2c mileage, then we can cycle through
         * each entry and fetch the extra mileage and then the support mileage and attach
         * it to the data within our mileage array to give us one correlated array of all
         *  our mileage.
         */
        foreach ($dateRange as $date) {
            $calls = Timesheets::model()->findAllByAttributes(
                array('user_id' => $user_id, 'date' => $date));
            foreach ($calls as $call) {
                $miles = new miles();
                /**
                 * public $id = null;
                 * public $date = null;
                 * public $call_ref = null;
                 * public $c2c = null;
                 * public $c2c_extra = null;
                 * public $social_support = null;
                 */
                $miles->id = $user_id;
                $miles->date = $date;
                $miles->client_id = $call->client_id;
                $miles->call_ref = $call->id;
                $c2c = Mileage::model()->findByAttributes(
                    array('call_ref' => $call->id));
                if ($c2c == null || empty($c2c)) {
                    $miles->c2c = 0.0;
                } else {
                    $miles->c2c = $c2c->c2c_miles;
                }

                $extra = C2cExtraMileage::model()->findByAttributes(
                    array('call_ref' => $call->id));
                if ($extra == null || empty($extra)) {
                    $miles->c2c_extra = 0.0;
                } else {
                    $miles->extra = $extra->mileage;
                }
                $ss = SocialSupportMileage::model()->findByAttributes(
                    array('call_ref' => $call->id));
                if ($ss == null || empty($ss)) {
                    $miles->social_support = 0.0;
                } else {
                    $miles->social_support = $ss->mileage;
                }
                $mileage_array[$user_id][] = $miles;
            }
        }
        return $mileage_array;
    }

    public function actionReport()
    {
        $report = Yii::app()->session['report_model'];
        //die(var_export($report));
        $this->makePDF($report['start_date'],$report['end_date']);
        $this->render('report',array('data'=>Yii::app()->session['report_data']));
    }

    public function makePDF($start, $end)
    {
        set_time_limit($this->_time_limit);
        $username_helper = new getUserName_helper;
        //Begin TCPDF Codes
        $pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf',
                'P', 'mm', 'A4', true, 'ISO-8859-1', false);

        $pdf->setFontSubsetting(false);
        $pdf->SetCreator('Personal Support Network Staff Portal');
        $pdf->SetAuthor($username_helper->makePrettyName(Yii::app()->user->name));
        $pdf->SetTitle(
            'Mileage' .
            date('d-m-y', strtotime($start)) .
            '_to_' . date('d-m-y', strtotime($end)));
        $pdf->SetSubject('Mileage');

        // set default header data
        $pdf->setJPEGQuality(90);
        $pdf->SetHeaderData(
            PDF_HEADER_LOGO,
            36.777083333,
            'Mileage Report ' . date('d-m-y', strtotime($start)) .
            ' to ' . date('d-m-y', strtotime($end)),
            'Generated by ' . $username_helper->makePrettyName(Yii::app()->user->name) .
            ' on ' . date('D jS F Y'),
            array(0, 0, 0),
            array(0, 0, 0)
        );
        $pdf->setFontSubsetting(false);
        $pdf->setFooterData(array(0, 0, 0), array(0, 0, 0));
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 11));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', 11));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(15);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('dejavusans', '', 8, '', true);

        $summaries = array();
        $summaries_index = 0;
        foreach(Yii::app()->session['report_data'] as $carer)
        {
            $carer_helpers = new administration_carer_helpers;
            $name = $carer_helpers->getNameById(key($carer));
            $name = $carer_helpers->makePrettyName($name['username']);

            //$output = $this->htmlOut($carer);
            var_dump($this->htmlOut($carer));
            /*
            if($output['summary'][$name]['total'] == 0) continue;
            */
            //$summaries[$summaries_index]['name'] = $name;
            //$summaries[$summaries_index]['summary'] = $output['summary'];
            //$summaries_index++;
            if(count($carer)==0) continue;
           // $pdf->AddPage();
            //$pdf->writeHTML($output['html'], true, false, true, false, '');
            /*
             * Now get summary details to write to html.
             */
            //$pdf->writeHTML($this->outputCarerSummary($name,$output['summary']), true, false, true, false, '');
        }
        //$pdf->AddPage();
        //$pdf->writeHTML($this->outputCarerSummariesTable($summaries), true, false, true, false, '');

        //$pdf->Output('Mileage_Report_' . date('d-m-y', strtotime($start)) .
           // '_to_' . date('d-m-y', strtotime($end)) . '.pdf', 'I');
    }

    public function outputCarerSummariesTable($summaries)
    {
        $html = '';
        $html .= '<table><tr><td><strong>Total Mileage Summaries</strong></td></tr><tr><td></td></tr></table>';
        $html .= '</br></br>';
        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td>Carer</td>';
        $html .= '<td>C2C Miles</td>';
        $html .= '<td>Extra Miles</td>';
        $html .= '<td>Social Support</td>';
        $html .= '<td>Total Miles</td>';
        $html .= '</tr><tr><td></td><td></td><td></td><td></td><td></td></tr>';

        $totals = array();
        foreach($summaries as $summary)
        {
            $html .= '<tr>';
            $html .= '<td>'.$summary['name'].'</td>';
            $html .= '<td>'.$summary['summary'][$summary['name']]['c2c'].'</td>';
            $totals['c2c'] += $summary['summary'][$summary['name']]['c2c'];
            $html .= '<td>'.$summary['summary'][$summary['name']]['c2c_extra'].'</td>';
            $totals['c2c_extra'] += $summary['summary'][$summary['name']]['c2c_extra'];
            $html .= '<td>'.$summary['summary'][$summary['name']]['social_support'].'</td>';
            $totals['social_support'] += $summary['summary'][$summary['name']]['social_support'];
            $html .= '<td>'.$summary['summary'][$summary['name']]['total'].'</td>';
            $totals['totals'] += $summary['summary'][$summary['name']]['total'];
            $html .= '</tr>';
        }
        $html .= '</tr><tr><td></td><td></td><td></td><td></td><td></td></tr>';

        $html .= '<tr>';
        $html .= '<td>Accumulated Totals</td>';
        $html .= '<td>' . $totals['c2c'] . '</td>';
        $html .= '<td>' . $totals['c2c_extra'] . '</td>';
        $html .= '<td>' . $totals['social_support'] . '</td>';
        $html .= '<td>' . $totals['totals'] . '</td>';
        $html .= '</tr>';

        $html .= '</table>';
        return $html;
    }
    public function htmlOut($carer)
    {

        $carer_helpers = new administration_carer_helpers;
        $date_helper = new formatDateFor_helper;
        $html = '';

        $name = $carer_helpers->getNameById(key($carer));
        $name = $carer_helpers->makePrettyName($name['username']);
        $totalMiles = 0.0;
        $c2cMiles = 0.0;
        $ssMiles = 0.0;

        $html .= '<h2>' . $name . '</h2>';
        $html .= '<table style="width: 500px;">';
        $html .= '<tbody>';
        $html .= '<tr>';
        $html .= '<td>Date</td>';
        $html .= '<td>Client ID</td>';
        $html .= '<td>To Call</td>';
        $html .= '<td>Extra</td>';
        $html .= '<td>Social Support</td>';
        $html .= '<td>Combined</td>';
        $html .= '<td>Total</td>';
        $html .= '</tr>';
        //Blank Row
        $html .= '<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';

        $summary = array();
        foreach ($carer as $mileage_object) {
            foreach ($mileage_object as $singular) {
                $html .= '<tr>';
                $html .= '<td>' . $this->humanDate($singular->date) . '</td>';
                $client = Client::model()->findByPk($singular->client_id);
                $html .= '<td>' . $client->initials . ' ' . $client->surname . '</td>';
                $html .= '<td>' . round($singular->c2c,2) . '</td>';
                $html .= '<td>' . round($singular->extra,2) . '</td>';
                $html .= '<td>' . round($singular->social_support,2) . '</td>';
                $html .= '<td>' . round($singular->c2c + $singular->extra + $singular->social_support, 2) . '</td>';
                $totalMiles += $singular->c2c + $singular->extra + $singular->social_support;
                $c2cMiles += ($singular->c2c + $singular->extra);
                $ssMiles += $singular->social_support;
                $html .= '<td>' . round($totalMiles, 2) . '</td>';
                $html .= '</tr>';
                $summary[$name]['c2c'] += $singular->c2c;
                $summary[$name]['c2c_extra'] += $singular->extra;
                $summary[$name]['social_support'] += $singular->social_support;
                $summary[$name]['total'] = $totalMiles;
            }
        }

        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '<h3>Total miles for period '.
            $date_helper->Viewing(Yii::app()->session['report_model']['start_date']).
            ' to '.$date_helper->Viewing(Yii::app()->session['report_model']['end_date']).
            ' for <strong>' . $name . '</strong> : <strong>' .
            round($totalMiles, 2) . '</strong></h3>';
        $output['html'] = $html;
        $output['summary'] = $summary;
        return $output;
    }

    public function outputCarerSummary($name,$summary)
    {
        /*
         * The input given ($summary)
         * is equal to:
         * ['name']['c2c']
         * ['name']['c2c_extra']
         * ['name']['social_support']
         * ['name']['total']
         */
        $html = '<table>';
        $html .= '<tr><td><strong>'.$name.'</strong></td><td></td></tr>';
        $html .= '<tr><td>Total Call to Call Mileage :</td><td>' . $summary[$name]['c2c'] . '</td></tr>';
        $html .= '<tr><td>Total Call to Call Extra Mileage :</td><td>' . $summary[$name]['c2c_extra'] . '</td></tr>';
        $html .= '<tr><td>Total Social Support Mileage :</td><td>' . $summary[$name]['social_support'] . '</td></tr>';
        $html .= '<tr><td>Total Mileage :</td><td>' . $summary[$name]['total'] . '</td></tr>';
        $html .= '</table>';

        return $html;
    }

    public function humanDate($date)
    {
        $date_bits = explode('-',$date);
        $human = $date_bits[2].'-'.$date_bits[1].'-'.$date_bits[0];
        return $human;
    }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
    public function beforeAction($action)
    {

        if (parent::beforeAction($action)) {

            $baseUrl = Yii::app()->baseUrl;

            $cs = Yii::app()->clientScript;
            $cs->registerScriptFile($baseUrl . '/js/mileage_reports.js');
            return true;
        }
        return false;
    }
}