<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of getWeeksCommencing_helper
 *
 * @author Ron
 */
class getWeeksCommencing_helper
{
    public function getWeeksCommencingListForUser($user_id)
    {
        $display_weeks = array();
        $week_disp_obj = new week_display_object();
        $mins = 0;

        $weeks = Yii::app()->db->createCommand()
            ->select('week_commencing')
            ->from('timesheets')
            ->where('user_id =:user_id', array(':user_id' => $user_id))
            ->order('week_commencing ASC')
            ->queryAll();


        $week_commencing = array();
        foreach ($weeks as $week) {
            $week_commencing[] = $week['week_commencing'];
        }
        $week_commencing = array_values(array_unique($week_commencing));

        foreach ($week_commencing as $week) {
            $days = Yii::app()->db->createCommand()
                ->select('date')
                ->from('timesheets')
                ->where(array('and', 'week_commencing =:week_commencing', 'user_id =:user_id'),
                    array(':week_commencing' => $week, ':user_id' => $user_id))
                ->order('date ASC')
                ->queryAll();

            $dates = array();
            foreach ($days as $day) {
                $dates[] = $day['date'];
            }
            $dates = array_values(array_unique($dates));

            foreach ($dates as $day) {
                $calls = Yii::app()->db->createCommand()
                    ->select('minutes')
                    ->from('timesheets')
                    ->where(array('and', 'date=:date', 'user_id=:user_id'),
                        array(':date' => $day, 'user_id' => $user_id))
                    ->queryAll();

                foreach ($calls as $call) {
                    $mins += $call['minutes'];
                }

            }
            $week_disp_obj->week_commencing = $week;
            $week_disp_obj->days = count($days);
            $week_disp_obj->calls = count($calls);
            $week_disp_obj->hours = $mins / 60;
            $mins = 0;
            $display_weeks[] = $week_disp_obj;
            $week_disp_obj = new week_display_object();
        }
        return $display_weeks;
    }
}