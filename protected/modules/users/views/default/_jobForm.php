<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
$getJobs = new getJobRoles_helper();
$userHelper = new getUserName_helper();
?>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
    ));
    ?>
<div class="form">
    <?php echo $form->hiddenField($model, 'id'); ?>
    <?php echo $form->errorSummary($model); ?>
<table class="centre" style="max-width: 325px;">
    <tr class="border_bottom">
        <td style="text-align: right;">
        <?php echo $form->labelEx($model, 'username'); ?>
        </td>
        <td style="text-align: left;">
        <?php echo CHtml::textField('user_id',$userHelper->lookUp($model->user_id), array('size'=>30,'readonly'=>true)); ?>
        <?php echo $form->error($model, 'username'); ?>
        </td>
    </tr>

    <tr class="border_bottom">
        <td style="text-align: right;">
            <?php echo $form->labelEx($model, 'job_role'); ?>

        </td>
        <td style="text-align: left;">
            <?php echo $form->dropDownList($model, 'job_role_id', CHtml::listData($getJobs->getJobRolesAssignment(), 'id', 'job_role'),array('prompt'=>'Select Role'));
            ?>
        </td>
    </tr>

    <tr>
        <td style="text-align: right;"></td>
        <td style="text-align: right;">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        </td>
    </tr>
</table>
    <?php $this->endWidget(); ?>
</div>