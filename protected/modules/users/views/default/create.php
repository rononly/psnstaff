<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Manage Users'=>array('index'),
	'Add',
);

?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>