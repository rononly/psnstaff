<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 25/08/13
 * Time: 12:03
 * To change this template use File | Settings | File Templates.
 */

class ClientSocialSupportReportModel extends CFormModel {

    public $all_clients;
    public $seperate_pages;
    public $clients;
    public $start_date;
    public $end_date;


    public function rules()
    {
        return array (
            array('start_date, end_date', 'required'),
        );
    }
    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'all_clients'=>'All Clients',
            'clients' => 'Select Clients',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'seperate_pages' => 'Seperate Pages',
        );
    }
    public function yes_no($attribute)
    {
        if($this->$attribute === 'Y')
        {

        }
        if($this->$attribute === 'N')
        {

        }
    }
}