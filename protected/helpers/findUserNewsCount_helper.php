<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of findUserNews_helper
 *
 * @author Ron
 */
class findUserNewsCount_helper
{
    public function findUserNewsCount()
    {
        $id = Yii::app()->user->id;
        $news = Yii::app()->db->createCommand()
            ->select('*')
            ->from('news_stand')
            ->where('user_id=:user_id', array(':user_id' => $id))
            ->queryAll();
        return count($news);
    }
}

?>
