<?php
/* @var $this DefaultController */

$this->breadcrumbs = array(
    $this->module->id,
);
$editImage = CHtml::image($this->module->assetsURL . '/images/edit.png', 'Edit News', array('width' => 24, 'height' => 24));
$addImage = CHtml::image($this->module->assetsURL . '/images/Add.png', 'Add News', array('width' => 24, 'height' => 24));
$deleteImage = CHtml::image($this->module->assetsURL . '/images/delete.png', 'Delete News', array('width' => 24, 'height' => 24));
$addLink = CHtml::link($addImage, array('/frontpage/default/addnews'));
?>
<p></p>
<table class="centre" style="width: 600px;">
    <tr>
        <td></td>
        <td><h3>Front Page News</h3></td>
        <td><?php echo $addLink; ?></td>
    </tr>
    <?php
    if (!empty($news)) {
        foreach ($news as $article) {
            $editLink = CHtml::link($editImage, array('/frontpage/default/updatenews', 'id' => $article['id']));
            $deleteLink = CHtml::link($deleteImage, array('/frontpage/default/markseen', 'id' => $article['id']));
            $html = '<tr>
            <td style="width: 10px; background-color: #c9e0ed;"></td><td style="font-weight: bolder">';
            $html .= $article['date'];
            $html .= '</td><td style="width: 10px; background-color: #c9e0ed"></td></tr>';

            echo $html;

            $html = '<tr style="background-color: white;">
            <td style="width: 10px; background-color: #c9e0ed;"></td><td>';
            $html .= $article['html'];
            $html .= '</td><td style="width: 10px; background-color: #c9e0ed">'. $deleteLink . $editLink .'</td></tr>';

            echo $html;
        }
    } else {
        echo '<tr><td>There is no news to display!</td></tr>';
    }
    ?>
</table>