<?php
/* @var $this StaffenteredController */

$this->breadcrumbs = array(
    'Staff-Entered',
);
?>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>

<table class="centre" style="width: auto;max-width: 300px">
    <tbody>
    <tr class="total" style="text-align: left;">
        <td>
            Other Reports
        </td>
        <td>

        </td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;"></td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'timesheetreport',
                'caption'=>'Time Sheet Report',
                'options'=>array('icons'=>'js:{primary:"ui-icon-pencil"}'),
                'url'=>$this->createUrl('/reports/timeSheet/index'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;"></td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'mileagereport',
                'caption'=>'Mileage Report',
                'options'=>array('icons'=>'js:{primary:"ui-icon-key"}'),
                'url'=>$this->createUrl('/reports/mileage/index'),
            ));
            ?></td>
    </tr>


    </tbody>
</table>


<form id="select_report" method="post" action="<?php echo CController::createURL('/reports/staffentered/index'); ?>">
    <table class="centre" style="min-width: 200px;max-width: 200px;">
        <tr class="border_bottom">
            <td style="text-align: right;"><input type="radio" name="choice" value="1" checked></td>
            <td style="text-align: left;">Time Sheets</td>
        </tr>
        <tr class="border_bottom">
            <td style="text-align: right;"><input type="radio" name="choice" value="2"></td>
            <td style="text-align: left;">Mileage</td>
        </tr>
        <tr class="border_bottom">
            <td style="text-align: right;"><input type="radio" name="choice" value="3"></td>
            <td style="text-align: left;">Combined</td>
        </tr>
        <tr>
            <td></td>
            <td style="text-align: left;"><input type="submit" value="Select"></td>
        </tr>
    </table>
</form>

<table class="centre" style="width: auto;max-width: 300px">
    <tbody>
    <tr class="total" style="text-align: left;">
        <td>
            Other Reports
        </td>
        <td>

        </td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;"></td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'entryreport',
                'caption'=>'Entry Report',
                'options'=>array('icons'=>'js:{primary:"ui-icon-pencil"}'),
                'url'=>$this->createUrl('/reports/staffentered/entryreport'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;"></td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'logonreport',
                'caption'=>'Logon Report',
                'options'=>array('icons'=>'js:{primary:"ui-icon-key"}'),
                'url'=>$this->createUrl('/reports/staffentered/logonreport'),
            ));
            ?></td>
    </tr>


    </tbody>
</table>