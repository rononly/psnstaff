<?php
Yii::import('ext.runactions.components.ERunActions');
class DefaultController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

    public function actionList()
    {
        if(isset($_GET['view']))
        {
            switch($_GET['view'])
            {
                case 'active':
                    $cdBCriteria = new CDbCriteria();
                    $cdBCriteria->condition = 'active=1';
                    $cdBCriteria->order = 'surname ASC';
                    $clients = Client::model()->findAll($cdBCriteria);
                    break;
                case 'inactive':
                    $cdBCriteria = new CDbCriteria();
                    $cdBCriteria->condition = 'active=0';
                    $cdBCriteria->order = 'surname ASC';
                    $clients = Client::model()->findAll($cdBCriteria);
                    if(count($clients)==0)
                    {
                        Yii::app()->user->setFlash('error','You have been returned to All client list as
                        there are no inactive clients.');
                        $this->redirect(array('/client/default/list','view'=>'all'));
                    }
                    break;
                default:
                    $clients = Client::model()->findAll(array('order' => 'surname ASC'));
                    break;
            }
        }
        else {
            $clients = Client::model()->findAll(array('order' => 'surname ASC'));
        }

        $this->render('list', array('clients' => $clients));
    }

    public function actionAdd()
    {
        $model=new Client;

        // uncomment the following code to enable ajax-based validation
        /*
        if(isset($_POST['ajax']) && $_POST['ajax']==='client-gen_form-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        */

        if(isset($_POST['Client']))
        {
            $model->attributes=$_POST['Client'];
            $model->added = date('Y-m-d');
            $model->updated = date('Y-m-d');
            if($model->validate())
            {
                // time to sort imputs prior to save
                $model->initials = strtoupper($model->initials);
                $model->surname = ucfirst(strtolower($model->surname));
                $model->postcode = strtoupper(str_replace(' ', '', $model->postcode));

                if($model->validate())//Still Validates
                {
                    try {
                        if($model->save()){
                        Yii::app()->user->setFlash('success',
                            'Clientel '.$model->initials.' '.$model->surname.' added successfully.');

                        $reference_details = Yii::app()->db->createCommand()
                            ->select('id, property_number, postcode')
                            ->from('client')
                            ->where(array('and',
                                    'initials=:initials',
                                    'surname=:surname',
                                    'postcode=:postcode'),
                                array(':initials' => $model->initials,
                                    ':surname' => $model->surname,
                                    ':postcode' => $model->postcode))
                            ->queryRow();
                        if($reference_details != null)
                        {
                            $this->insertReferencesForCreation($reference_details);
                            $this->actionCalcUncalcedReferences();
                        }
                        else{
                            Yii::app()->user->setFlash('error',
                                'Clientel '.$model->initials.' '.$model->surname.' reference details
                                could not be added.');
                        }
                    }
                    }
                    catch(CDbException $e)
                    {
                        if($e->errorInfo[1] == 1062)
                        {
                            Yii::app()->user->setFlash('error',
                                'Clientel '.$model->initials.' '.$model->surname.' COULD NOT be added
                                as duplicate details were found.');
                        }
                    }
                    $this->redirect(array('/client/default/index'));
                }
            }
        }
        $this->render('add',array('model'=>$model));
    }

    public function actionUpdate()
    {
        //die(var_export($_POST));
        $criteria = new CDbCriteria();
        $criteria->order = 'surname ASC';
        $clients = Client::model()->findAll($criteria);
        foreach($clients as &$client)
        {
            $client->property_number = $client->initials . ' ' . $client->surname . ' ' .
                $client->postcode;
        }
        $model = new SelectClient;

        if (isset($_POST['SelectClient'])) {
            $clientModel = Client::model()->findByPk($_POST['SelectClient']['id']);
            $this->render('update', array('model' => $clientModel));
        }
        if (isset($_POST['Client'])) {
            $clientModel = Client::model()->findByPk($_POST['Client']['id']);
            $clientModel->setAttributes($_POST['Client']);
            $clientModel->initials = strtoupper($clientModel->initials);
            $clientModel->surname = ucfirst(strtolower($clientModel->surname));
            $clientModel->postcode = strtoupper(str_replace(' ', '', $clientModel->postcode));
            $clientModel->updated = date('Y-m-d');
            if ($clientModel->validate()) {
                $clientModel->update();
                //Recalculate Mileage References
                //First we need to remove the existing mileage references from the call_to_call_mileage_reference table
                $criteria = new CDbCriteria();
                $criteria->condition = 'from_client_id=:client_id OR to_client_id=:client_id';
                $criteria->params = array(':client_id'=>$clientModel->id);
                Call_To_Call_Mileage_Reference::model()->deleteAll($criteria);

                //All existing references for this client should now be gone.
                //Now to arrange new referencing
                    $criteria = new CDbCriteria();
                    $criteria->select = array('id', 'property_number', 'postcode');
                    $criteria->condition = 'id=:id';
                    $criteria->params = array(':id'=>$clientModel->id);
                $reference_details = Client::model()->find($criteria);
                if($reference_details != null)
                {
                    $this->insertReferencesForCreation($reference_details);
                    $this->actionCalcUncalcedReferences();
                }
                else{
                    Yii::app()->user->setFlash('error',
                        'Clientel '.$model->initials.' '.$model->surname.' reference details
                                could not be added.');
                }


                //End of Recalculating Mileage References
                Yii::app()->user->setFlash('success', "Client ".
                    $clientModel->initials. ' '. $clientModel->surname.
                    ' '. $clientModel->postcode.' has been successfully altered and mielage references recreated.');
                $this->redirect(array('/client/default/index'));
            }
        }

        $this->render('update', array('model' => $model, 'clients' => $clients));
    }

    public function actionDelete()
    {

    }

    /*
     * Use Below as needed, hook up when neccesary.
     */
    public function actionTransferClients()
    {
        //Old table cid_table
        //New table client

        //Deal is take all records from cid_table and insert them into new table,
        //however.. the ident column from cid_table must go to surname column of new table.

        $old_client_list = CidTable::model()->findAll();

        foreach($old_client_list as $old_client)
        {
            $this_client = new Client();
            $this_client->initials = 'xx';
            $this_client->surname = $old_client->ident;
            $this_client->property_number = $old_client->property_no;
            $this_client->postcode = $old_client->postcode;
            $this_client->active = $old_client->active;
            $this_client->added = $old_client->added;
            $this_client->updated = $old_client->updated;
            $this_client->save();
        }
        Yii::app()->user->setFlash('success', 'All clients from cid_table now transferred.');
        $this->redirect(array('/administration/client/index'));
    }
    public function actionCapitalizeLetters()
    {
        $client_list = Client::model()->findAll();

        foreach($client_list as &$model)
        {
            $model->postcode = str_replace(' ', '', $model->postcode);
            Client::model()->updateByPk($model->id, array(
                'initials' => strtoupper($model->initials),
                'surname' => ucfirst(strtolower($model->surname)),
                'postcode' => strtoupper($model->postcode)
            ));
        }
        Yii::app()->user->setFlash('success', 'All clients data capitalised.');
        $this->redirect(array('/administration/client/index'));
    }

    /*
     * @reference_details id
     * @reference_details property_number
     * @reference_details postcode
     */
    public function insertReferencesForCreation($reference_details)
    {
        $criteria = new CDbCriteria();
        $criteria->select = 'id, property_number, postcode';
        $client_references = Client::model()->findAll($criteria);

        foreach($client_references as $client_reference)
        {
            if($client_reference->id == $reference_details['id']) continue;
            $mRFC = new Mileage_Reference_For_Creation();
            $mRFC->from_client_id = $reference_details['id'];
            $mRFC->from_property_number = $reference_details['property_number'];
            $mRFC->from_postcode = $reference_details['postcode'];
            $mRFC->to_client_id = $client_reference->id;
            $mRFC->to_property_number = $client_reference->property_number;
            $mRFC->to_postcode = $client_reference->postcode;
            try {
                $mRFC->save();
            }
            catch (CDbException $e)
            {
                if($e->errorInfo[1] == 1062)
                {
                    continue;
                }
            }
        }
        foreach($client_references as $client_reference)
        {
            if($client_reference->id == $reference_details['id']) continue;
            $mRFC = new Mileage_Reference_For_Creation();
            $mRFC->from_client_id = $client_reference->id;
            $mRFC->from_property_number = $client_reference->property_number;
            $mRFC->from_postcode = $client_reference->postcode;
            $mRFC->to_client_id = $reference_details['id'];
            $mRFC->to_property_number = $reference_details['property_number'];
            $mRFC->to_postcode = $reference_details['postcode'];
            try {
                $mRFC->save();
            }
            catch (CDbException $e)
            {
                if($e->errorInfo[1] == 1062)
                {
                    continue;
                }
            }
        }
    }
    public function actionQueueAllForRecalculation()
    {
        $criteria = new CDbCriteria();
        $criteria->select = 'id, property_number, postcode';
        $client_references = Client::model()->findAll($criteria);

        foreach($client_references as $client)
        {
            set_time_limit(60);
            $this->insertReferencesForCreation($client);
        }
        Yii::app()->user->setFlash('success', 'All client C2C references queued for recalculation.');
        $calc = new call_to_call_calculator;
        $calc->process_mileage_reference_for_creation_table();
        $this->redirect(array('/client/default/index'));

    }
    public function actionViewUncalculated()
    {
        $model = new SetMileage();

        if(isset($_POST['SetMileage']))
        {
            $model = new Call_To_Call_Mileage_Reference();
            $model->from_client_id = $_POST['SetMileage']['from_id'];
            $model->to_client_id = $_POST['SetMileage']['to_id'];
            $model->mileage = 0;
            if($model->save())
            {
                $create_ref_model = Mileage_Reference_For_Creation::model()->findByAttributes(
                    array('from_client_id'=>$_POST['SetMileage']['from_id'],
                    'to_client_id'=>$_POST['SetMileage']['to_id']));
                $create_ref_model->delete();
            }
        }
        $uncalced = Mileage_Reference_For_Creation::model()->findAll();
        $this->render('uncalced',array('model'=>$model,'uncalced'=>$uncalced));
    }

    public function actionCalcUncalcedReferences()
    {
            $calc = new call_to_call_calculator();
            $calc->process_mileage_reference_for_creation_table();
    }

    public function actionRun()
    {
        Yii::import('application.modules.client.controllers.DefaultController');
        $controller_instance = new DefaultController("Default");
        $controller_instance->actionCalcUncalcedReferences();
        Yii::app()->user->setFlash('success',
        'Clientel Mileage reference are being generated.');
        $this->redirect(array('index'));
    }

    public function actionCalcOffice()
    {
        $client = Client::model()->findByPk(96);
        Yii::app()->user->setFlash('success', 'All client C2C references queued for recalculation.');
        $this->insertReferencesForCreation($client);
        $this->actionCalcUncalcedReferences();
        $this->redirect(array('/client/default/index'));
    }

    public function actionToggleActive()
    {
        $id = $_GET['id'];
        $active = $_GET['active'];

        if($active)
        {
            Client::model()->updateByPk($id,array(
                'active'=>0,
                ));
        }
        else
        {
            Client::model()->updateByPk($id,array(
                'active'=>1,
            ));
        }

        $this->redirect(Yii::app()->request->urlReferrer);
    }

    public function actionSingleRecalc($id)
    {
        /*
         * Delete all mileage references to this individual
         */
        Call_To_Call_Mileage_Reference::model()->deleteAllByAttributes(array('from_client_id'=>$id));
        Call_To_Call_Mileage_Reference::model()->deleteAllByAttributes(array('to_client_id'=>$id));
        /*
         * Now queue individual for mileage calculation.
         */
        $reference_details = Client::model()->findByPk($id);
        if($reference_details != null)
        {
            $this->insertReferencesForCreation($reference_details);
            $this->actionCalcUncalcedReferences();
        }
    }
}