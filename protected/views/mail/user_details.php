<?php

if (isset($_SESSION['selected_user']))
{
$username = $_SESSION['selected_user']['username'];
$names = explode(' ', $username);
$firstname = ucfirst($names[0]);
}
if (isset($firstname))
{
    $indentifier = $firstname;
}
else { $identifier = 'Personal Support Network Employee'; }
$password = 'changeme';
?>
<h1>Hi <?php echo $identifier; ?></h1>
<p>You are receiving this email as an account has been created for you
on the PSN Staff Portal.</p>
<p>Your login credentials are:
<table>
    <tr><td>Username:</td><td><?php echo $username; ?></td></tr>
    <tr><td>Password:</td><td><?php echo $password; ?></td></tr>
    <tr><td>Website:</td><td><a href="http:\\staff.personalsupportnetwork.org">PSN Staff Portal</a></td></tr>
</table></p>

<p>As you can see, your password is not very secure and needs changing
the first time you log onto the site which is located at :
<a href="http:\\staff.personalsupportnetwork.org">staff.personalsupportnetwork.org</a></p>
<p>Click on the 'Login' link to login. Once logged in, the 'MyAccount'
option will appear. From the MyAccount area you can change your personal
details.
Please take time when you first login, to change your password and enter
your phone numbers.</p>
<p>The site has been created to allow staff to fill in and submit time sheets
online.  It will also be adapted for mileage as well, and over time more features
will be added that should assist you in your work, and we will be adding a
learning resources section to assist with finding relevent materials when
studying.</p>

<p>The best thing to do now is logon, change your details and have a play
around to get a feel for the system, try adding, removing, editing and viewing
calls, You will not be able to submit your sheets yet, but by the next submit
date, the system will be ready so you can start using the system to record your
calls, straight away, I will also introduce a help link very quickly
on each page, should you need assistance.</p>
<p>In the mean time, if you would like hands on training with the system, then
please phone in and ask when I (Ron) am in the office next, and drop in to see
me as I will be glad to help.</p>
<p>Ron Appleton
Personal Support Network.</p>