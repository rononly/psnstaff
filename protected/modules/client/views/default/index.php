<?php
/* @var $this ClientController */

$this->breadcrumbs=array(
    'Manage Clients',
);
?>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>
<table class="centre" style="width: auto;max-width: 300px">
    <tbody>
    <tr class="total" style="text-align: left;">
        <td>
            Clients
        </td>
        <td>

        </td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">List Clients</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'list',
                'caption'=>'List',
                'options'=>array('icons'=>'js:{primary:"ui-icon-contact"}'),
                'url'=>$this->createUrl('/client/default/list'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Add Client</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'add',
                'caption'=>'Add',
                'options'=>array('icons'=>'js:{primary:"ui-icon-plusthick"}'),
                'url'=>$this->createUrl('/client/default/add'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Edit Client</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'edit',
                'caption'=>'Edit',
                'options'=>array('icons'=>'js:{primary:"ui-icon-pencil"}'),
                'url'=>$this->createUrl('/client/default/update'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Recalculate C2C References</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'recalculate',
                'caption'=>'Recalculate',
                'options'=>array('icons'=>'js:{primary:"ui-icon-contact"}'),
                'url'=>$this->createUrl('/client/default/queueallforrecalculation'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Calculate Uncalculated Mileage References</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'calcuncalc',
                'caption'=>'Calculate',
                'options'=>array('icons'=>'js:{primary:"ui-icon-contact"}'),
                'url'=>$this->createUrl('/client/default/run'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Uncalculated C2C References</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'uncalculated',
                'caption'=>'Uncalculated',
                'options'=>array('icons'=>'js:{primary:"ui-icon-contact"}'),
                'url'=>$this->createUrl('/client/default/viewuncalculated'),
            ));
            ?></td>
    </tr>
    <tr>
        <td style="text-align: right;">Remove Client</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'remove',
                'caption'=>'Remove',
                'options'=>array('icons'=>'js:{primary:"ui-icon-closethick"}'),
                'url'=>$this->createUrl('/client/default/delete'),
            ));
            ?></td>
    </tr>
    </tbody>
</table>