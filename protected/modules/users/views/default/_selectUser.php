<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'user-form',
    'enableAjaxValidation' => false,
));
?>

    <table class="centre" style="max-width: 400px">
        <tbody>
        <tr class="border_bottom">
            <td style="text-align: right;">Select User</td>
            <td style="text-align: left;">
                <?php echo $form->dropDownList($model,'user_id',CHtml::listData($users,'id','username')); ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><?php echo CHtml::submitButton('Edit User'); ?></td></td>
        </tr>
        </tbody>
    </table>
<?php $this->endWidget(); ?>