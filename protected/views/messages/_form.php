<?php
/* @var $this MessagesController */
/* @var $model Messages */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'messages-form',
        'enableAjaxValidation' => false,
    ));
    $getUserNameList = new getUserNamesList_helper();
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'creation_date'); ?>
        <?php echo $form->textField($model, 'creation_date', array('value' => date('Y-m-d'), 'readonly' => true, 'size' => 7)); ?>
<?php echo $form->error($model, 'creation_date'); ?>
    </div>

    <div class="row">
<?php echo $form->hiddenField($model, 'author_id', array('value' => Yii::app()->user->id)); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'recipient_id'); ?>
        <?php echo $form->dropDownList($model, 'recipient_id', CHtml::listData($getUserNameList->getUserNamesList(), 'id', 'username'));
        ?>
<?php echo $form->error($model, 'recipient_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'subject'); ?>
        <?php echo $form->textField($model, 'subject', array('size' => 60)); ?>
<?php echo $form->error($model, 'subject'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'message'); ?>
        <?php echo $form->textArea($model, 'message', array('style' => 'width: 387px; height: 80px;')); ?>
<?php echo $form->error($model, 'message'); ?>
    </div>

    <div class="row buttons">
<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->