<?php

class StaffenteredController extends Controller
{
    private $_time_limit = 240;
    public function actionIndex()
    {
        if (isset($_POST['choice'])) {
            switch ($_POST['choice']) {
                case 1:
                    $this->redirect(array('Timesheet'));
                    break;
                case 2:
                    $this->redirect(array('/reports/mileage/index'));
                    break;
                case 3:
                    $this->redirect(array('Combined'));
                    break;
                default:
                    $this->redirect(array('Index'));
            }
        }
        $this->render('index');
    }

    public function actionEntryReport()
    {
        $model = new EntryReport();
        $entries = null;
        if(isset($_POST['EntryReport']))
        {
            $model->setAttributes($_POST['EntryReport']);
            $model->staff = $_POST['EntryReport']['staff'];
            if($model->validate())
            {
                $criteria = new CDbCriteria();
                $criteria->condition = 'user_id=:id AND added >= :start_date AND added < :end_date + INTERVAL 1 DAY';
                $criteria->params = array(':id'=>$model->staff, ':start_date'=>$model->start_date, ':end_date'=>$model->end_date);
                $entries = Timesheets::model()->findAll($criteria);


            }
        }
        $this->render('entryreport', array('model'=>$model, 'entries'=>$entries));
    }

    public function actionUnsubmit()
    {
        $c = 0;
        $model = new Unsubmit();
        if(isset($_POST['Unsubmit']))
        {
            $model->setAttributes($_POST['Unsubmit']);
            $model->staff = $_POST['Unsubmit']['staff'];
            if($model->validate())
            {
                $records_to_delete = null;
                foreach($model->staff as $staff)
                {
                    $criteria = new CDbCriteria();
                    $criteria->condition = 'user_id=:id AND date >= :start_date AND date <= :end_date';
                    $criteria->params = array(':id'=>$staff, ':start_date'=>$model->start_date, ':end_date'=>$model->end_date);
                    $records_to_unsubmit = SubmittedUserDays::model()->findAll($criteria);
                    foreach($records_to_unsubmit as $record)
                    {
                        $records_to_delete[] = $record;
                        $c ++;
                    }
                }
                foreach($records_to_delete as $rtd)
                {
                    /**
                     * @todo Add counting function to ensure all records are deleted or report those that are not.
                     */
                    $criteria = new CDbCriteria();
                    $criteria->condition = 'user_id=:id AND date=:date';
                    $criteria->params = array(':id'=>$rtd['user_id'], ':date'=>$rtd['date']);
                    SubmittedUserDays::model()->find($criteria)->delete();
                }
                Yii::app()->user->setFlash('success', 'Records Unsubmitted');
                $model = new Unsubmit();
            }
        }
        $this->render('unsubmit', array('model'=>$model));
    }
    public function actionLogonReport()
    {
        $model = new LogonReport();
        $data = null;
        $lastlogin = null;
        if(isset($_POST['LogonReport']))
        {
            $model->setAttributes($_POST['LogonReport']);
            $model->staff = $_POST['LogonReport']['staff'];
            if($model->validate())
            {
                $criteria = new CDbCriteria();
                $criteria->condition = 'userid=:id AND datetime >= :start_date AND datetime < :end_date + INTERVAL 1 DAY';
                $criteria->params = array(':id'=>$model->staff, ':start_date'=>$model->start_date, ':end_date'=>$model->end_date);
                $data = Logons::model()->findAll($criteria);
                if(empty($data))
                {
                    $criteria = new CDbCriteria();
                    $criteria->condition = 'userid=:id ORDER BY datetime DESC';
                    $criteria->params = array(':id'=>$model->staff);
                    $criteria->limit = 1;
                    $lastlogin = Logons::model()->find($criteria);
                    if(empty($lastlogin))
                    {
                        $criteria = new CDbCriteria();
                        $criteria->condition = 'id=:id';
                        $criteria->params = array(':id'=>$model->staff);
                        $lastlogin = User::model()->find($criteria);
                    }
                }
                //$this->redirect(array('displaylogonreport', 'model'=>$model, 'data'=>$data));
                //$this->actionDisplayLogonReport($model,$data);
            }
        }
        $this->render('logonreport', array('model'=>$model, 'data'=>$data, 'lastlogin'=>$lastlogin));
    }

    public function actionCombined()
    {
        set_time_limit($this->_time_limit);
        $model = new GenerateReport();
        if (isset($_POST['GenerateReport'])) {
            if (isset($model)) {
                $poRepModel = new PopulateReportModel;
                $model = $poRepModel->populateModel($model, 'GenerateReport', $_POST);
            }
            /*
             * 1. staff, list of integers (user_id)
             * 2. all_staff, (integer 1 = all_staff, 0 = not_chosen)
             * 3. job_role, (integer/s possible multiple integers of job_type id(array))
             * 4. report_coverage,
             *          thisMonth
             *          thisPayMonth
             *          thisWeek
             *          chooseDates (brings start_date and end_date into use)
             * 5. start_date, (start_date of date range to generate)
             * 6. end_date, (end_date of date range to generate)
             * 7. report_type, (mileage, timesheet, combined)
             */

            if ($model->Validate()) {
                $model->report_type = 'combined';
                $this->generateCombinedReport($model);
            }
        }
        $model->report_type = 'combined';
        $model->report_coverage = 'thisPayMonth';
        $this->render('combined', array('model' => $model));
    }






    public function generateCombinedReport($model)
    {
        $date_helper = new date_helper();
        $dateRange = $date_helper->createDateRangeArrayByDates(
            $model->start_date, $model->end_date);
        $data = array();
        foreach ($model->staff as $staff_member) {
            $data[] = $this->fetchCallsAndMileageForUserBetweenDates(
                array(
                    'user_id' => $staff_member,
                    'dateRange' => $dateRange,
                ));
        }
        Yii::app()->session['_user_generated_data'] = $data;
        Yii::app()->session['_user_generated_report'] = 'Combined';
        Yii::app()->session['_user_generated_report_model'] = $model;
        $this->redirect(array('DisplayReport'));
    }







    public function actionCleanJobAssignments()
    {
        $cleaner = new job_assignment_helper();
        $cleaner->cleanJobAssignments();
    }

    public function actionDisplayReport()
    {
        $this->render('report');
    }

    public function actionDisplayTimesheetReport()
    {
        $this->render('time_sheet_report');
    }

    public function makePDF($names, $output, $start, $end)
    {
        set_time_limit($this->_time_limit);
        $username_helper = new getUserName_helper;
        switch ($output) {
            case 'Screen':
                $out = 'I';
                break;
            case 'Download':
                $out = 'D';
                break;
            case 'String':
                $out = 'S';
                break;
        }
        //Begin TCPDF Codes
        if ($names['file'] == 'Combined_TimeSheet_and_Mileage') {
            $pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf',
                'L', 'mm', 'A4', true, 'ISO-8859-1', false);
        } else {
            $pdf = Yii::createComponent('application.extensions.tcpdf.ETcPdf',
                'P', 'mm', 'A4', true, 'ISO-8859-1', false);
        }
        $pdf->setFontSubsetting(false);
        $pdf->SetCreator('Personal Support Network Staff Portal');
        $pdf->SetAuthor($username_helper->makePrettyName(Yii::app()->user->name));
        $pdf->SetTitle(
            $names['title'] .
            date('d-m-y', strtotime($start)) .
            '_to_' . date('d-m-y', strtotime($end)));
        $pdf->SetSubject($names['title']);

        // set default header data
        $pdf->setJPEGQuality(90);
        $pdf->SetHeaderData(
            PDF_HEADER_LOGO,
            36.777083333,
            $names['title'] . 'Report ' . date('d-m-y', strtotime($start)) .
            ' to ' . date('d-m-y', strtotime($end)),
            'Generated by ' . $username_helper->makePrettyName(Yii::app()->user->name) .
            ' on ' . date('D jS F Y'),
            array(0, 0, 0),
            array(0, 0, 0)
        );
        $pdf->setFontSubsetting(false);
        $pdf->setFooterData(array(0, 0, 0), array(0, 0, 0));
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 11));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', 11));
        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(15);
        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('dejavusans', '', 10, '', true);
        $pdf->AddPage();
        $pdf->writeHTML(Yii::app()->session['_fully_generated_report'], true, false, true, false, '');
        if ($out == 'S') {
            return $pdf->Output($names['file'] . '_Report_' . date('d-m-y', strtotime($start)) .
                '_to_' . date('d-m-y', strtotime($end)) . '.pdf', $out);
        }
        $pdf->Output($names['file'] . '_Report_' . date('d-m-y', strtotime($start)) .
            '_to_' . date('d-m-y', strtotime($end)) . '.pdf', $out);
    }



    public function fetchCallsAndMileageForUserBetweenDates($params)
    {
        $debug = true;
        /*
                 * This function gives in return for the given parameters of
                 * user_id
                 * dateRange
                 *
                 * an object containing all the mileage data for the given user
                 * within the given date range.
                 */
        $user_id = $params['user_id'];
        $dateRange = $params['dateRange'];

        /**
         * Now we need a variable to hold the calls and mileage,
         * To keep things simple we will attach the calls and
         * mileage to the array as array[user_id][call] and [mileage]
         * so when building the output string we know we can access
         * the call and mileage data from within the same foreach loop
         * that is indexed.
         */
        $combined = array();
        /**
         * So the process now is to cycle through each of the dates in
         * the dateRange then through each call, store the call and then
         * create and store a miles object with it.
         */
        foreach ($dateRange as $date) {
            if ($debug) {
                //echo 'fetchCallsAndMileageForUserBetweenDates for user '.$user_id.'</br></br>'.var_dump($dateRange);
            }
            //Get all calls for this day for this user..
            $calls = Timesheets::model()->findAllByAttributes(
                array('user_id' => $user_id, 'date' => $date));
            //For each of these calls..
            foreach ($calls as $call) {
                $miles = new miles();
                /**
                 * public $id = null;
                 * public $date = null;
                 * public $call_ref = null;
                 * public $c2c = null;
                 * public $c2c_extra = null;
                 * public $social_support = null;
                 */
                $miles->id = $user_id;
                $miles->date = $date;
                $miles->client_id = $call->client_initials . ' ' . $call->client_postcode;
                $miles->call_ref = $call->id;
                $c2c = Mileage::model()->findByAttributes(
                    array('call_ref' => $call->id));
                if ($c2c == null || empty($c2c)) {
                    $miles->c2c = 0.0;
                } else {
                    $miles->c2c = $c2c->c2c_miles;
                }

                $extra = C2cExtraMileage::model()->findByAttributes(
                    array('call_ref' => $call->id));
                if ($extra == null || empty($extra)) {
                    $miles->c2c_extra = 0.0;
                } else {
                    $miles->extra = $extra->mileage;
                }
                $ss = SocialSupportMileage::model()->findByAttributes(
                    array('call_ref' => $call->id));
                if ($ss == null || empty($ss)) {
                    $miles->social_support = 0.0;
                } else {
                    $miles->social_support = $ss->mileage;
                }
                $comb = new Combined();
                $comb->call = $call;
                $comb->mileage = $miles;
                $combined[$user_id][] = $comb;
                unset($comb);
            }
        }
        return $combined;
    }

    public function beforeAction($action)
    {

        if (parent::beforeAction($action)) {

            $baseUrl = Yii::app()->baseUrl;
            Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/reports.js', CClientScript::POS_HEAD);
            return true;
        }
        return false;
    }

    public function actionEmailSuccess()
    {
        $this->render('EmailSuccess');
    }

    public function actionEmailUnsuccessful()
    {
        $this->render('EmailUnsuccessful', array('error' => $_GET['error']));
    }
    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}

class TIME_SHEET_PDF extends tFPDF
{
    function __construct()
    {
        $this->nameHelper = new administration_carer_helpers();
        $this->summaries = array();
        $this->call_type_helpers = new getCallTypes_helper;
        $this->timeSheet_helpers = new timesheet_helpers;
    }

    public function carerTimeSheetTable($carer)
    {
        $this->SetFont('', 'B', '16');
        $username = $this->nameHelper->getNameById(key($carer));
        $this->Cell(40, 10, $this->nameHelper->makePrettyName($username['username']), 15);
        $this->Ln();

        $this->SetFont('', 'B', '10');
        $this->SetFillColor(128, 128, 128);
        $this->SetTextColor(255);
        $this->SetDrawColor(92, 92, 92);
        $this->SetLineWidth(.3);

        $this->Cell(20, 7, "Date", 1, 0, 'C', true);
        $this->Cell(30, 7, "Client ID", 1, 0, 'C', true);
        $this->Cell(30, 7, "Call Type", 1, 0, 'C', true);
        $this->Cell(20, 7, "Call Length", 1, 0, 'C', true);
        $this->Cell(20, 7, "Total", 1, 0, 'C', true);
        $this->Ln();

        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');

        $fill = false;

        $totalMinutes = 0.0;
        $dayCount = 0;
        $callCount = 0;
        foreach ($carer as $inDays) {
            $dayCount++;
            foreach ($inDays as $inCalls) {
                $callCount++;
                $this->Cell(20, 6, $inCalls['date'], 'LR', 0, 'L', $fill);
                $this->Cell(30, 6, $inCalls['client_initials'] . ' ' . $inCalls['client_postcode'], 'LR', 0, 'R', $fill);
                $this->Cell(30, 6, $this->call_type_helpers->lookUp($inCalls['call_type_id']), 'LR', 0, 'L', $fill);
                $this->Cell(20, 6, round($inCalls['minutes'] / 60, 2), 'LR', 0, 'R', $fill);
                $totalMinutes += $inCalls['minutes'];
                $this->Cell(20, 6, round($totalMinutes / 60, 2), 'LR', 0, 'R', $fill);
                $this->Ln();
                $fill = !$fill;
            }
            if (Yii::app()->session['_user_generated_report_model']->submit_data == 1) {
                $this->timeSheet_helpers->submitCall($inDays['id']);
            }
        }
        $this->Cell(120, 0, '', 'T');
    }
}