<?php
/* @var $this UserController */
/* @var $data User */
$getJobRole = new getJobRoles_helper();
?>

<div class="view">
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
        &nbsp;&nbsp;&nbsp;&nbsp;
	<b><?php echo CHtml::encode($data->getAttributeLabel('username')); ?>:</b>
	<?php echo CHtml::encode($data->username); ?>
        &nbsp;&nbsp;&nbsp;&nbsp;
	<b>
        <?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <b>Job Role:</b>
        <?php echo $getJobRole->userLookup($data->id); ?>
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<b><?php 
        if ($data->mobile)
            echo CHtml::encode($data->getAttributeLabel('mobile')); ?><?php if ($data->mobile) echo ':'; ?></b>
	<?php 
        if ($data->mobile)
            echo CHtml::encode($data->mobile); ?>
&nbsp;&nbsp;&nbsp;&nbsp;
	<b><?php 
        if ($data->landline)
            echo CHtml::encode($data->getAttributeLabel('landline')); ?><?php if ($data->landline) echo ':'; ?></b>
	<?php 
        if ($data->landline)
            echo CHtml::encode($data->landline); ?>


</div>