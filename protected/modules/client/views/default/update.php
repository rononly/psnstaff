<?php
/* @var $this DefaultController */
/* @var $model Client */

$this->breadcrumbs=array(
    'Manage Clients'=>array('index'),
    'Update',
);

?>
<?php $compare = new SelectClient; ?>
<?php if($model == $compare) {?>
    <?php echo $this->renderPartial('_selectClient', array('model'=>$model,'clients'=>$clients)); ?>
<?php } else { //Is The Update Form ?>
    <?php echo $this->renderPartial('_form_add', array('model'=>$model)); ?>
<?php }?>