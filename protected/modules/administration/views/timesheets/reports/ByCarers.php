<?php
/* @var $this ReportsController */

$this->breadcrumbs = array(
    'Time Sheet Reports' => array('/administration/timesheets/index'),
    'ByCarers',
);
?>
<style type="text/css" media="screen">
    div.loading {
        background-color: #eee;
        background-image: url('images/loading2.gif');
        background-position:  center center;
        background-repeat: no-repeat;
        opacity: 1;
    }
    div.loading * {
        opacity: .8;
    }
</style>
<div class="form" id="form-page">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'UserList-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <table border=1 cellspacing=0 cellpadding=0>
        <tr>
            <td width=201 rowspan=5 valign=top style=''>
                <div class="row">
                    <?php echo $form->listBox($model, 'userlist', CHtml::listData($users, 'email', 'username'), array('multiple' => 'multiple', 'size' => 18, 'style' => 'width:200px'));?>
                    <?php echo $form->error($model, 'userlist'); ?>
                </div>
            </td>
            <td width=301 valign=top style=''>
                <div class="row">
                </div>
            </td>
        </tr>
        <tr>
            <td width=301 valign=top style=''>
                <div class="row" id="description">
                </div>
            </td>
        </tr>
        <tr>
            <td width=301 valign=top style=''>
                <div class="row" style="">
                </div>
            </td>
        </tr>
        <tr>
            <td width=301 valign=top style=''>

            </td>
        </tr>
        <tr>
            <td width=301 valign=top halign=right style=''>
                <div class="row buttons">
                    <?php echo CHtml::submitButton('Send Bulk Email'); ?>
                </div>
            </td>
        </tr>
    </table>
    <?php $this->endWidget(); ?>
</div><!-- form -->