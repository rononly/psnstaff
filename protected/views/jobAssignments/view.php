<?php
/* @var $this JobAssignmentsController */
/* @var $model JobAssignments */

$this->breadcrumbs=array(
	'Job Assignments'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List JobAssignments', 'url'=>array('index')),
	array('label'=>'Create JobAssignments', 'url'=>array('create')),
	array('label'=>'Update JobAssignments', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete JobAssignments', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage JobAssignments', 'url'=>array('admin')),
);
?>

<h1>View Job Assignments #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'job_role_id',
	),
)); ?>
