<?php
/* @var $this MileageController */
/* @var $model Mileage */

$this->breadcrumbs=array(
	'Mileages'=>array('index'),
	'Add Mileage',
);

$this->menu=array(
	array('label'=>'List Mileage', 'url'=>array('index')),
	array('label'=>'Manage Mileage', 'url'=>array('admin')),
);
?>

<h2>Add Mileage</h2>

<?php $this->renderPartial('my_form', array('model'=>$model)); ?>