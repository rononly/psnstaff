<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 17/09/13
 * Time: 22:08
 * To change this template use File | Settings | File Templates.
 */
$html = '';
foreach($carers as $carer)
{
    $html .= $this->renderPartial('reports/results/_carer', array('carer' => $carer));
}
echo $html;
?>
<div class="form" style="margin-left: auto; margin-right: auto;">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'emailreport-form',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->createUrl('administration/timesheets/emailreport'),
    ));
?>
<?php echo $form->labelEx($model, 'submitcalls') . $form->checkBox($model, 'submitcalls'); ?>
<p class="hint">
Hint: Selecting to Submit Calls, will record in the database that the calls have</br>
been submitted and will therefore, prevent anyone from changing there rota within</br>
the date range of the report.</p>
<?php echo $form->hiddenField($model, 'start'); ?>
    <?php echo $form->hiddenField($model, 'end'); ?>
</br></br>
<?php echo CHtml::submitButton(' Email Report '); ?></td>


<?php $this->endWidget(); ?>
</div>