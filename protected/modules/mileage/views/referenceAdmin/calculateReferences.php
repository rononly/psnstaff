<?php
/* @var $this ReferenceAdminController */

$this->breadcrumbs=array(
	'Reference Admin'=>array('/mileage/referenceAdmin'),
	'Calculate References',
);
?>
<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'CalculateReferences-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Calculate'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->