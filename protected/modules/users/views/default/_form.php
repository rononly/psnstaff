<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
$getJobs = new getJobRoles_helper();
?>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
    ));
    ?>
<div class="form">
    <?php echo $form->hiddenField($model, 'id'); ?>
    <?php echo $form->errorSummary($model); ?>
<table class="centre" style="max-width: 325px;">
    <?php if(isset($update) == false) { ?>
    <tr class="border_bottom">
        <td style="text-align: right;">
        <?php echo $form->labelEx($model, 'username'); ?>
        </td>
        <td style="text-align: left;">
        <?php echo $form->textField($model, 'username', array('size' => 30, 'maxlength' => 128)); ?>
        <?php echo $form->error($model, 'username'); ?>
        </td>
    </tr>
<?php } else { ?>
    <tr class="border_bottom">
        <td style="text-align: right;">
            <?php echo $form->labelEx($model, 'username'); ?>
        </td>
        <td style="text-align: left;">
            <?php echo $form->textField($model, 'username', array('size' => 30, 'readonly'=>true)); ?>
            <?php echo $form->error($model, 'username'); ?>
        </td>
    </tr>
    <?php } ?>
    <tr class="border_bottom">
        <td style="text-align: right;">
            <?php echo $form->labelEx($model, 'email'); ?>
        </td>
        <td style="text-align: left;">
            <?php echo $form->textField($model, 'email', array('size' => 30, 'maxlength' => 128)); ?>
            <?php echo $form->error($model, 'email'); ?>
        </td>
    </tr>
<?php if(isset($update) == false) { ?>
    <tr class="border_bottom">
        <td style="text-align: right;">
        <?php echo $form->labelEx($model, 'password'); ?>
        </td>
        <td style="text-align: left;">
        <?php echo $form->textField($model, 'password', array('size' => 30, 'maxlength' => 60, 'readonly'=>true)); ?>
        <?php echo $form->error($model, 'password'); ?>
        </td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">
            <?php echo $form->labelEx($model, 'job_role'); ?>

        </td>
        <td style="text-align: left;">
            <?php echo $form->dropDownList($model, 'job_role', CHtml::listData($getJobs->getJobRolesAssignment(), 'id', 'job_role'));
            ?>
        </td>
    </tr>
<?php } ?>
    <tr class="border_bottom">
        <td style="text-align: right;">
        <?php echo $form->labelEx($model, 'mobile'); ?>
        </td>
        <td style="text-align: left;">
        <?php echo $form->textField($model, 'mobile', array('size' => 17, 'maxlength' => 11)); ?>
        <?php echo $form->error($model, 'mobile'); ?>
        </td>
    </tr>

    <tr class="border_bottom">
        <td style="text-align: right;">
        <?php echo $form->labelEx($model, 'landline'); ?>
        </td>
        <td style="text-align: left;">
        <?php echo $form->textField($model, 'landline', array('size' => 17, 'maxlength' => 11)); ?>
        <?php echo $form->error($model, 'landline'); ?>
        </td>
    </tr>

    <tr>
        <td style="text-align: right;"></td>
        <td style="text-align: right;">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Add User' : 'Update User'); ?>
        </td>
    </tr>
</table>
    <?php $this->endWidget(); ?>
</div>