<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	'enableAjaxValidation'=>false,
)); 
$getJobRoles = new getJobRoles_helper(); 
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'author_name'); ?>
		<?php echo $form->textField($model,'author_name',array('size'=>60,'maxlength'=>60, 'value'=>Yii::app()->user->name, 'readonly'=>true)); ?>
		<?php echo $form->error($model,'author_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'audience'); ?>
                <?php echo $form->dropDownList($model,'audience', 
                        CHtml::listData($getJobRoles->getJobRoles(), 'id', 'job_role'));?>
		<?php echo $form->error($model,'audience'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'headline'); ?>
		<?php echo $form->textField($model,'headline',array('size'=>60,'maxlength'=>80)); ?>
		<?php echo $form->error($model,'headline'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'story'); ?>
		<?php echo $form->textArea($model,'story',array ( 'style'=>'width: 387px; height: 80px;' )); ?>
		<?php echo $form->error($model,'story'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date', array('value'=>date('Y-m-d'),'readonly'=>true ,'size'=>7)); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'expiry'); ?>
		<?php echo $form->textField($model,'expiry',array('size'=>7)); ?>
                <?php echo '<p class="hint">Hint: Days the news will be active (0 = indefinately)</p>'; ?>
		<?php echo $form->error($model,'expiry'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->