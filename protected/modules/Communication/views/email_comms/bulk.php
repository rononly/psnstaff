<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 20/08/13
 * Time: 21:35
 * To change this template use File | Settings | File Templates.
 */
?>
<h2>Bulk Mailing</h2>

<h3><?php echo CHtml::link('Create a bulk email.', array('/Communication/BulkEmail/create')); ?></h3>
<h3><?php echo CHtml::link('Edit a bulk email.', array('/Communication/BulkEmail/edit')); ?></h3>
<h3><?php echo CHtml::link('Send a bulk email.', array('/Communication/BulkEmail/send')); ?></h3>
<h3><?php echo CHtml::link('View bulk emails.', array('/Communication/BulkEmail/index')); ?></h3>
<h3><?php echo CHtml::link('Bulk Mail Settings.', array('/Communication/BulkEmail/index')); ?></h3>