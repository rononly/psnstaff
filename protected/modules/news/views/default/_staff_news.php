<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 02/04/14
 * Time: 01:10
 *
 * @var $news News
 */
$editImage = CHtml::image($this->module->assetsURL . '/images/edit.png', 'Edit News', array('width' => 24, 'height' => 24));
$addImage = CHtml::image($this->module->assetsURL . '/images/Add.png', 'Add News', array('width' => 24, 'height' => 24));
$deleteImage = CHtml::image($this->module->assetsURL . '/images/delete.png', 'Delete News', array('width' => 24, 'height' => 24));
$addLink = CHtml::link($addImage, array('/news/default/index'));
?>
<p></p>
<table class="centre" style="width: 600px;">
    <tr>
        <td></td>
        <td><h3>Staff News</h3></td>
        <td><?php echo $addLink; ?></td>
    </tr>
    <?php
    if (!empty($news)) {
        $news = array_reverse($news);
        foreach ($news as $article) {
            if(!$article['news_type']==0)
            {
                continue;
            }
            $editLink = CHtml::link($editImage, array('/news/default/updatenews', 'id' => $article['id']));
            $deleteLink = CHtml::link($deleteImage, array('/news/default/markseen', 'id' => $article['id']));
            $html = '<tr>
            <td style="width: 10px; background-color: #c9e0ed;"></td><td style="font-weight: bolder">';
            $html .= '<h6>'.$article['date'].'</h6>';
            $html .= '<h3>'.$article['headline'].'</h3>';
            $html .= '<h6> by '.$article['author_name'].'</h6>';
            $html .= '</td><td style="width: 10px; background-color: #c9e0ed"></td></tr>';

            echo $html;

            $html = '<tr style="background-color: white;">
            <td style="width: 10px; background-color: #c9e0ed;"></td><td>';
            $html .= $article['story'];
            $html .= '</td><td style="width: 10px; background-color: #c9e0ed">'. $deleteLink . $editLink .'</td></tr>';

            echo $html;
        }
    } else {
        echo '<tr><td>There is no news to display!</td></tr>';
    }
    ?>
</table>