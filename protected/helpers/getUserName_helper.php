<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of getUserName_helper
 *
 * @author Ron
 */
class getUserName_helper
{
    public function lookUp($id)
    {
        $username = Yii::app()->db->createCommand()
            ->select('username')
            ->from('tbl_user')
            ->where('id=:id', array(':id' => $id))
            ->limit(1)
            ->queryRow();
        return $username['username'];
    }

    public function lookUpID($username)
    {
        $id = Yii::app()->db->createCommand()
            ->select('id')
            ->from('tbl_user')
            ->where('username=:username', array(':username' => $username))
            ->limit(1)
            ->queryRow();
        return $id['id'];
    }

    public function makePrettyName($name)
    {
        $names = explode(' ', $name);
        foreach($names as &$n)
        {
            $n = ucfirst($n);
        }
        unset($n);
        $outname = implode(' ', $names);
        return $outname;

    }

    public function lookupClientName($id)
    {
        $username = Yii::app()->db->createCommand()
            ->select('initials, surname')
            ->from('client')
            ->where('id=:id', array(':id' => $id))
            ->limit(1)
            ->queryRow();
        return $username['initials'].' '.$username['surname'];
    }
}

?>
