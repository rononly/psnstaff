<?php
/* @var $this MyaccountController */

$this->breadcrumbs = array(
    'Myaccount',
);
$mh = new myaccount_helpers();
$user = $mh->getUser(Yii::app()->user->id);
$this->menu = $mh->ControllerMenu();
?>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>
<h1>My Account</h1>

<table>
    <tr>
        <td width="50px"></td>
        <td style="text-align: right;" width="80px">Name:</td>
        <td><?php echo $user['username']; ?></td>
        <td></td>
    </tr>
    <tr>
        <td width="50px"><?php echo CHtml::button('Change', array('submit' => array('myaccount/changeemail'))); ?></td>
        <td style="text-align: right;" width="80px">Email:</td>
        <td><?php echo $user['email']; ?></td>
        <td></td>
    </tr>
    <tr>
        <td width="50px"></td>
        <td style="text-align: right;" width="80px">Job Role:</td>
        <td><?php echo $mh->getJobRole(Yii::app()->user->id); ?></td>
        <td></td>
    </tr>
    <tr>
        <td width="50px"><?php echo CHtml::button('Change', array('submit' => array('myaccount/changemobile'))); ?></td>
        <td style="text-align: right;" width="80px">Mobile:</td>
        <td><?php echo $user['mobile']; ?></td>
        <td></td>
    </tr>
    <tr>
        <td width="50px"><?php echo CHtml::button('Change', array('submit' => array('myaccount/changelandline'))); ?></td>
        <td style="text-align: right;" width="80px">Landline:</td>
        <td><?php echo $user['landline']; ?></td>
        <td></td>
    </tr>
    <tr>
        <td width="50px"><?php echo CHtml::button('Change', array('submit' => array('myaccount/changepassword'))); ?></td>
        <td style="text-align: right;" width="80px">Password:</td>
        <td>**********</td>
        <td></td>
    </tr>
</table>