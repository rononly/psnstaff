<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Manage Users',
);

?>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>
<table class="centre" style="width: auto;max-width: 300px">
    <tbody>
    <tr class="total" style="text-align: left;">
        <td>
            Users
        </td>
        <td>

        </td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">List Users</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'list',
                'caption'=>'List',
                'options'=>array('icons'=>'js:{primary:"ui-icon-contact"}'),
                'url'=>$this->createUrl('/users/default/list'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Add User</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'add',
                'caption'=>'Add',
                'options'=>array('icons'=>'js:{primary:"ui-icon-plusthick"}'),
                'url'=>$this->createUrl('/users/default/add'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Edit User</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'edit',
                'caption'=>'Edit',
                'options'=>array('icons'=>'js:{primary:"ui-icon-pencil"}'),
                'url'=>$this->createUrl('/users/default/edit'),
            ));
            ?>

    </tr>
    <tr class="border_bottom">
        </td><td style="text-align: right;">Edit Job Role</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'editjob',
                'caption'=>'Edit Job Role',
                'options'=>array('icons'=>'js:{primary:"ui-icon-pencil"}'),
                'url'=>$this->createUrl('/users/default/editjob'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        </td><td style="text-align: right;">Reset Password</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'resetPass',
                'caption'=>'Reset Password',
                'options'=>array('icons'=>'js:{primary:"ui-icon-pencil"}'),
                'url'=>$this->createUrl('/users/default/resetpass'),
            ));
            ?></td>
    </tr>
    <tr class="border_bottom">
        <td style="text-align: right;">Remove User - Not Advised</td>
        <td style="text-align: left;"><?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'buttonType'=>'link',
                'name'=>'remove',
                'caption'=>'Remove',
                'options'=>array('icons'=>'js:{primary:"ui-icon-closethick"}'),
                'url'=>$this->createUrl('/users/default/remove'),
            ));
            ?></td>
    </tr>

    </tbody>
</table>