<?php

class DefaultController extends Controller
{
    /**
     * @var string the communication layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate()
    {
        $clients = Client::model()->findAll('active=1 order by surname');
        $idents = array();
        $idents_index = 0;
        foreach ($clients as $client) {
            $idents[$idents_index]['id'] = $client['id'];
            $idents[$idents_index]['ident'] = $client['surname'] . ' ' . $client['initials'] . ' (' .
                $client['postcode'] . ')';
            $idents_index++;
        }
        if (isset($_GET['id'])) {
            $model = $this->loadModel($_GET['id']);
            $start_time_bits = explode(':', $model->start_time);
            $end_time_bits = explode(':', $model->end_time);
            $model->shour = $start_time_bits[0];
            $model->smin = $start_time_bits[1];
            $model->ehour = $end_time_bits[0];
            $model->emin = $end_time_bits[1];
        } else {
            $model = new Timesheets();
            $model->added = date('Y-m-d H:i:s', time());
        }

        if (isset($_GET['date'])) {
            $date = $_GET['date'];
        } else {
            $this->redirect(array('selecttimesheetday'));
        }

        if (isset($_POST['Timesheets'])) {
            $model->attributes = $_POST['Timesheets'];
            $model->user_id = $_POST['Timesheets']['user_id'];
            $model->date = $_POST['Timesheets']['date'];
            $model->client_id = $_POST['Timesheets']['client_id'];
            $model->start_time = $_POST['Timesheets']['shour'] . ':' . $_POST['Timesheets']['smin'];
            $model->end_time = $_POST['Timesheets']['ehour'] . ':' . $_POST['Timesheets']['emin'];
            //$model->added = date('Y-m-d H:i:s', time());
            $model->updated = date('Y-m-d H:i:s', time());
            //die(var_dump($model));
            if ($model->Validate()) {
                if ($model->save()) {
                        $this->redirect(array('update', 'date' => $_POST['Timesheets']['date']));
                } else
                    die('There was no save!');
            }
        }

        if(isset($date))
        {
            $this->render('update', array(
                'model' => $model, 'date' => $date, 'idents' => $idents,
            ));
        }
        else{
            $this->render('update', array(
                'model' => $model, 'date' => date('Y-m-d'), 'idents' => $idents,
            ));
        }

    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id, $date)
    {
        $this->loadModel($id)->delete();
        $this->redirect(array('update', 'date' => $date));
    }


    public function actionSelectTimeSheetDay()
    {
        $th = new timesheet_helpers();
        $calls = $th->getDisplaySelectByDayRange(Yii::app()->user->id);
        $this->render('selectday', array('data' => $calls));
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Timesheets the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Timesheets::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionBackOneMonth()
    {
        if (isset(Yii::app()->session['month_offset'])) {
            Yii::app()->session['month_offset'] = Yii::app()->session['month_offset'] - 1;
            if (Yii::app()->session['month_offset'] < -6) {
                Yii::app()->session['month_offset'] = -6;
            }
            $this->redirect(array('selecttimesheetday'));
        }
    }

    public function actionForwardOneMonth()
    {
        if (isset(Yii::app()->session['month_offset'])) {
            Yii::app()->session['month_offset'] = Yii::app()->session['month_offset'] + 1;
            if (Yii::app()->session['month_offset'] > 0) {
                Yii::app()->session['month_offset'] = 0;
            }
            $this->redirect(array('selecttimesheetday'));
        }
    }

    public function actionDownload()
    {
        $this->render('download', array('menu' => $this->menu));
    }

    /**
     * Performs the AJAX validation.
     * @param Timesheets $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'timesheet-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}