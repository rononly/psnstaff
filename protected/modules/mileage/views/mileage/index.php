<?php
/* @var $this MileageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Mileages',
);

$this->menu=array(
	array('label'=>'Create Mileage', 'url'=>array('create')),
	array('label'=>'Manage Mileage', 'url'=>array('admin')),
);
?>

<h1>Mileages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
