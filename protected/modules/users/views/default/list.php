<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 19/01/14
 * Time: 01:39
 */
$this->breadcrumbs=array(
    'Manage Users'=>array('index'),
    'List Users',
);
?>
<?php
foreach(Yii::app()->user->getFlashes() as $key => $message) {
    echo '<div class="flash-' . $key . '">' . $message . "</div>\n";
}
?>
    <div style="text-align: center">
        <?php
        $this->widget('zii.widgets.jui.CJuiButton', array(
            'buttonType'=>'link',
            'name'=>'active',
            'caption'=>'Active',
            'options'=>array('icons'=>'js:{primary:"ui-icon-contact"}'),
            'url'=>$this->createUrl('/users/default/list&view=active'),
        ));
        $this->widget('zii.widgets.jui.CJuiButton', array(
            'buttonType'=>'link',
            'name'=>'inactive',
            'caption'=>'Inactive',
            'options'=>array('icons'=>'js:{primary:"ui-icon-contact"}'),
            'url'=>$this->createUrl('/users/default/list&view=inactive'),
        ));
        $this->widget('zii.widgets.jui.CJuiButton', array(
            'buttonType'=>'link',
            'name'=>'all',
            'caption'=>'All',
            'options'=>array('icons'=>'js:{primary:"ui-icon-contact"}'),
            'url'=>$this->createUrl('/users/default/list&view=all'),
        ));
        ?>
    </div>
    <p></p>
<?php
$uh = new getUserName_helper;
$jr = new getJobRoles_helper;
$ja = new job_assignment_helper;
foreach($users as $user)
{
    ?>
<table class="centre" style="width: 600px;font-size: 16px;">
    <tbody>
    <tr>
        <td style="min-width: 200px;"><strong><?php echo $uh->makePrettyName($user->username); ?></strong></td>
        <td style="min-width: 300px;text-align: right;"></td>
        <?php
        //Make formatted link.
        $active_link = CHtml::link('Active',array('/users/default/toggleactive',
            'id'=>$user->id,
            'active'=>$user->active));
        ?>
        <td style="width: 50px;text-align: right"><?php echo $active_link; ?></td>
        <?php
        if($user->active == 1)
        {
            ?><td style="color: black;background-color: limegreen;min-width: 50px;max-width: 50;text-align: left;"><strong>YES</strong></td><?php
        }
        else
        {
            ?><td style="color: black;background-color: red;min-width: 50px;max-width: 50px;text-align: left;"><strong>NO</strong></td><?php
        }
        ?>

    </tr>
    <tr class="border_bottom" style="font-size: 12px;">
        <td style="width: 200px;text-align: right;">Job Title</td>
        <td style="width: 300px;text-align: left;"><?php echo $jr->lookUp($jr->lookUpUserJobId($user->id)); ?></td>
        <td style="min-width: 50px;max-width: 50px"></td>
        <td style="min-width: 50px;max-width: 50px"></td>
    </tr>
    <tr class="border_bottom" style="font-size: 12px;">
        <td style="width: 200px;text-align: right;">Email Address</td>
        <td style="width: 300px;text-align: left;"><?php echo $user->email;?></td>
        <td style="width: 50px;"></td>
        <td style="width: 50px;"></td>
    </tr>
    <?php
    if($user->mobile != '')
    {
        ?>
        <tr class="border_bottom" style="font-size: 12px;">
        <td style="width: 200px;text-align: right;">Mobile</td>
        <td style="width: 300px;text-align: left;"><?php echo $user->mobile;?></td>
        <td style="width: 50px;"></td>
        <td style="width: 50px;"></td>
        </tr>
        <?php
    }?>
    <?php
    if($user->landline != '')
    {
        ?>
        <tr class="border_bottom" style="font-size: 12px;">
            <td style="width: 200px;text-align: right;">Landline</td>
            <td style="width: 300px;text-align: left;"><?php echo $user->landline;?></td>
            <td style="width: 50px;"></td>
            <td style="width: 50px;"></td>
        </tr>
    <?php
    }?>
    </tbody>
</table>
    <?php
}