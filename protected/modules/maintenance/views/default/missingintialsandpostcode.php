<?php
/**
 * Created by PhpStorm.
 * User: Ron Appleton
 * Date: 05/10/14
 * Time: 01:13
 */
?>
<h3>Time Sheets Entries Missing Initials AND Postcodes</h3>
<?php if (empty($timesheets)) { echo '<h3>NO Timesheets</h3>'; } ?>
<table>
    <tr>
        <td>Time Sheet ID</td>
        <td>User Id</td>
        <td>Client Id</td>
        <td>Date Added</td>
    </tr>
    <?php
    foreach($timesheets as $sheet)
    {
        $html = '<tr>';
        $html.= '<td>' . $sheet['id'] . '</td>';
        $html.= '<td>' . $sheet['user_id'] . '</td>';
        $html.= '<td>' . $sheet['client_id'] . '</td>';
        $html.= '<td>' . $sheet['added'] . '</td>';
        $html.= '</tr>';
        echo $html;
    }
    ?>
</table>