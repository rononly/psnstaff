<?php
/* @var $this BulkEmailController */
/* @var $model BulkEmail */

$this->breadcrumbs=array(
	'Bulk Emails'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List BulkEmail', 'url'=>array('index')),
	array('label'=>'Create BulkEmail', 'url'=>array('create')),
	array('label'=>'Update BulkEmail', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BulkEmail', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BulkEmail', 'url'=>array('admin')),
);
?>

<h1>View BulkEmail #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
		'html',
	),
)); ?>
