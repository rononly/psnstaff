<?php
$this->breadcrumbs = array(
    'Time Sheets' => array('selecttimesheetday'),
    'View By Day',);
/*
 * Add Operations Menu
 */
$th = new timesheet_helpers();
$dh = new date_helper();
$this->menu = $menu;
$submissionDays = $th->getSubmittedDaysForUserInDateRange(Yii::app()->user->id);
$editImage = CHtml::image('images/live_journal_sml.png', 'Edit This Call');
$arrowLeft = CHtml::image('images/live_journal_sml.png', 'Back one month');
$arrowRight = CHtml::image('images/live_journal_sml.png', 'Forward one month');
$runningTotal = true;

?>
<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 22/08/13
 * Time: 15:51
 * To change this template use File | Settings | File Templates.
 */
?>
    <style media="screen" type="text/css">
        tr.border_bottom td {
            border-bottom: 2pt solid #e5eCf9;
            text-align: center;
        }

        tr.topbar td {
            text-align: center;
            font-family: Roboto;
            font-size: 16px;
            font-weight: bold;
            width: 80px;
        }

        tr.highlight td {
            background-color: lightgreen;
            border-bottom: 2pt solid #e5eCf9;
            text-align: center;
        }

        tr.header td {
            font-size: larger;
            font-weight: bold;
            border-bottom: 2pt solid #e5eCf9;
            text-align: center;
        }

        table.centre {
            margin-left: auto;
            margin-right: auto;
            -moz-border-radius: 15px;
            border-radius: 15px;
            background-color: #c9e0ed;
            padding-left: 20px;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-right: 0px;
            -moz-box-shadow: 10px 10px 5px #888;
            -webkit-box-shadow: 10px 10px 5px #888;
            box-shadow: 10px 10px 5px #888;
        }
    </style>
<?php

$days = $dh->createDateRangeArray();
//die(var_dump($days));
$formatter = new formatDateFor_helper();
//die(var_dump($data));
if ($data == null) {
    ?>
    <h1>No Time Sheets</h1>
    <p>You have not yet created any time sheets that you can view or submit,
        please use the operations menu to "Create a Time Sheet".</p>
<?php
} else {
    ?>
    <table class="centre" border="1" style="width: 450px;">
        <tbody>
        <tr class="topbar">
            <td>
                <form action="index.php?r=timesheets/backonemonth" method="post">
                    <?php echo CHtml::imageButton('images/arrow-left.png'); ?>
                </form>
            </td>
            <td></td>
            <td>

            </td>
            <td></td>
            <td>
                <form action="index.php?r=timesheets/forwardonemonth" method="post">
                    <?php echo CHtml::imageButton('images/arrow-right.png'); ?>
                </form>
            </td>
        </tr>
        <tr class="header">
            <td>Date</td>
            <td>Calls
            <td>Hours</td>
            <?php
            if ($runningTotal) {
                ?>
                <td>Running Total</td>
            <?php
            }
            ?>
            <td>Status</td>
            <td></td>
        </tr>
        <?php
        if ($runningTotal) {
            $hours = 0;
        }
        foreach ($data as $day) {
            if ($day->calls == 0 && $dh->testPrevious($day->date)) {
                    continue;
                }
            $submitted = in_array($day->date, $submissionDays);
            if ($submitted) {
                ?>
                <tr class="highlight">
            <?php
            } else {
                ?>
                <tr class="border_bottom">
            <?php
            }

            if ($submitted) {
                ?>
                <td><?php echo $day->date; ?> </td>
            <?php
            } else {
                ?>
                <td><?php echo CHtml::link($formatter->Viewing($day->date), array('timesheets/update',
                        'date' => $day->date)); ?></td>
            <?php
            }
            ?>
            <td><?php echo  $day->calls; ?></td>
            <td><?php echo $day->hours; ?></td>
            <?php
            if ($runningTotal) {
                $hours += $day->hours;
                ?>
                <td><?php echo $hours; ?></td>
            <?php
            }
            if ($submitted) {
                ?>
                <td><?php echo Chtml::image('images/submitted.png'); ?></td>
            <?php
            } else {
                $editLink = CHtml::link($editImage, array('timesheets/update',
                    'date' => $day->date));
                ?>
                <td><?php echo $editLink; ?></td>
            <?php
            }
            ?>
            </tr>
        <?php
        }
        if ($runningTotal) {
            ?>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td><?php echo $hours; ?> Total</td>
                <td></td>
            </tr>
        <?php
        }
        ?>
    </table>
<?php
}
?>