<?php

class ClientController extends Controller
{
    public function actionActivate()
    {
        $this->render('activate');
    }

    public function actionAdd()
    {
        $model = new CidTable;

        // uncomment the following code to enable ajax-based validation
        /*
        if(isset($_POST['ajax']) && $_POST['ajax']==='cid-table-_form-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        */

        if (isset($_POST['CidTable'])) {
            $model->attributes = $_POST['CidTable'];
            $model->added = date('Y-m-d');
            $model->updated = date('Y-m-d');
            if ($model->validate()) {
                if ($model->save()) {
                    $this->redirect(array('/administration/client/index'));
                } else {

                }
            }
        }
        $this->render('add', array('model' => $model));
    }

    public function actionEdit()
    {
        try {
                $model = $this->loadModel($_GET['id']);
                if(empty($model) || $model == NULL)
                {
                    throw new Exception('Unable to Load Client Model');
                }
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }

            $this->redirect(array('selecttimesheetday', array('menu' => $this->menu)));
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);



        $this->render('update', array(
            'model' => $model, 'menu' => $this->menu,
        ));
    }

    public function actionDeactivate()
    {
        $this->render('deactivate');
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionRecalculateMileage($model)
    {
        //$controller->calculateFromClientToOthers($model);
        //$controller->calculateFromOthersToClient($model);
    }

    /**
     * To Remove user would include removing many things,
     * Time Sheet Entries
     * Mileage Entries
     * @TODO Add Much More Functionality to this Function.
     * @param int $id
     */
    public function actionRemove($id)
    {
        //$this->loadModel($id)->delete();
        $this->render('remove');
    }

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
    public function loadModel($id)
    {
        $model = CidTable::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
}