<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 29/08/13
 * Time: 23:28
 * To change this template use File | Settings | File Templates.
 */

class timesheet_helper {

    public function fetchMiles($call_ref)
    {
        $mileage = Mileage::model()->find('call_ref=:call_ref', array(':call_ref' => $call_ref));
        return $mileage;
    }
    public function fetchClientIds()
    {
        $result = Yii::app()->db->createCommand()
            ->select('Ident')
            ->from('cid_table')
            ->order('Ident')
            ->QueryAll();

        $ids = array();
        foreach($result as $client_id)
        {
            $ids[$client_id['Ident']] = $client_id['Ident'];
        }
        return $ids;
    }
    public function fetchClientIds_new()
    {
        $result = Yii::app()->db->createCommand()
            ->select('id, surname')
            ->from('client')
            ->order('surname')
            ->QueryAll();

        $ids = array();
        foreach($result as $client_id)
        {
            $ids[$client_id['id']] = $client_id['id'];
        }
        return $ids;
    }
    public function getDistance($num,$pcode,$num2,$pcode2)
    {
       // http://dev.virtualearth.net/REST/v1/Routes?wp.1=Cambridge , UK&wp.2=Church St, Newcastle-under-Lyme, UK&key=ENTERYOURBINGMAPSKEYHERE

       // http://dev.virtualearth.net/REST/v1/Routes? wayPoint.1=wayPoint1&viaWaypoint.2=viaWaypoint2&waypoint.3=wayPoint3&wayPoint.n=wayPointn&distanceUnit=distanceUnit&key=BingMapsKey

        $request = 'http://dev.virtualearth.net/REST/v1/Routes?wayPoint.1=';
        if($num != null)
            $request .= $num . ',' . $pcode;
        else
            $request += $pcode;
        $request .= '&waypoint.2=';
        if($num2 != null)
            $request .= $num2 . ',' . $pcode2;
        else
            $request .= $pcode2;
        $request .= '&distanceUnit=mi';
        $request .= '&key=AuN7W7PVUtwPmNA7meZjLuICaP7Q57R0r5WOZV0nEaTFV9mlYdDDfM8d2FGYygSk';

        $curl = curl_init($request);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        $dec = json_decode($result);
        return round($dec->resourceSets[0]->resources[0]->travelDistance,1);


    }
    public function cleanTime($time)
    {
        $time_bits = explode(':', $time);
        return $time_bits[0].':'.$time_bits[1];
    }
}