<?php

/**
 * This is the model class for table "timesheets".
 *
 * The followings are the available columns in table 'timesheets':
 * @property integer $id
 * @property string $date
 * @property string $start_time
 * @property string $end_time
 * @property integer $user_id
 * @property integer $client_id
 * @property integer $call_type_id
 * @property string shour
 * @property string smin
 * @property string ehour
 * @property string emin
 */
class Timesheets extends CActiveRecord {
    public $dateErrorAdded = false;
    public $hours;
    public $smin;
    public $shour;
    public $emin;
    public $ehour;
    public $client_id;
    public $added;
    public $updated;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Timesheets the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'timesheets';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        $this->start_time = $this->shour . ':' . $this->smin;
        $this->end_time = $this->ehour . ':' . $this->emin;
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('call_type_id', 'numerical', 'integerOnly' => true),
            array('start_time', 'checkForSameTime', 'compare' => 'end_time'),
            //array('start_time', 'checkStartBeforeEnd', 'compare' => 'end_time'),
            //array('start_time', 'checkMoreThanFifteenMinutes', 'compare' => 'end_time'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id,  date, call_type_id, hours, minutes', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'date' => 'Date',
            'client_id' => 'Client',
            'call_type_id' => 'Call Type',
            'shour' => 'Start',
            'ehour' => 'End',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id, true);
        $criteria->compare('client_id', $this->client_id, true);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('call_type_id', $this->call_type_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

/*
   public function zeroMinutes($attribute,$params) {
        if ($this->$attribute + $this->$params['hrs'] == 0)
        {
            $this->addError($attribute, 'No minutes or hours selected');
            if (!$this->dateErrorAdded)
            {
            $this->addError($this->date,'Please Re-Select the Date');
            $this->dateErrorAdded = true;
            }
        }
    }
    public function zeroHours($attribute,$params) {
        if ($this->$attribute + $this->$params['mins'] == 0)
        {
            $this->addError($attribute, 'No hours or minutes selected');
            if (!$this->dateErrorAdded)
            {
            $this->addError($this->date,'Please Re-Select the Date');
            $this->dateErrorAdded = true;
            }
        }
    }
    */

    public function checkForSameTime($attribute,$params)
    {
        if ($this->$attribute === $this->$params['compare'])
        {
            $this->addError($attribute, 'You cannot start and finish a call at the same time!');
        }
    }
    public function checkStartBeforeEnd($attribute,$params)
    {
        if(strtotime($this->$attribute) > strtotime($this->$params['compare']))
        {
            $this->addError($attribute, 'You cannot start a call after you finish a call!');
        }
    }
    public function checkMoreThanFifteenMinutes($attribute,$params)
    {
        if((strtotime($this->$params['compare']) - strtotime($this->$attribute)) /60 < 15)
        {
            $this->addError($attribute, 'Minimum call length is 15 minutes!');
        }
    }
    public function checkForStartAtMidnight($attribute,$params)
    {
        if($this->$attribute === '00:00')
        {
            $this->addError($attribute, 'No calls start at midnight!');
        }
    }
}