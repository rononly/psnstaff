<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 19/05/14
 * Time: 21:43
 * @var $model ClientSocialSupportReportModel
 * @var $form CActiveForm
 * @var $this ClientsController
 */
?>
<div class="form" id="form-page">
    <?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'generate-report-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
));?>

<?php echo $form->errorSummary($model); ?>
<table id="outer_table" class="centre">
    <tr>
        <td id="left" style="width: 210px">
            <?php echo $form->labelEx($model, 'clients', array('class' => 'staff')); ?>
            <?php echo $form->listBox($model, 'clients', CHtml::listData($clients, 'id', 'ident'),
                array('multiple' => 'multiple', 'size' => 21, 'style' => 'width:200px')); ?>
            <?php echo $form->error($model, 'clients'); ?>
        </td>
        <td id="right" style="width: 390px">
            <table id="right-outer-table">
                <tr>
                    <td>
                        <table>
                            <tr class="border_bottom2">
                                <td>
                                    <?php echo $form->labelEx($model, 'all_clients', array('class' => 'allStaff')); ?>
                                    <?php echo $form->checkBox($model, 'all_clients') ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table id="job-role-table">
                            <tr class="border_bottom2">
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr class="border_bottom2">
                                <td style="width: 100px"><?php
                                    echo $form->labelEx($model, 'start_date', array('class' => 'dates'));
                                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        'model' => $model,
                                        'attribute' => 'start_date',
                                        'flat' => false, //remove to hide the datepicker
                                        'options' => array(
                                            'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                                            'dateFormat' => 'yy-mm-dd',
                                        ),
                                        'htmlOptions' => array(
                                            'class'=> 'dates',
                                        ),
                                    ));
                                    ?></td>
                                <td style="width: 100px"><?php
                                    echo $form->labelEx($model, 'end_date', array('class' => 'dates'));
                                    $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                                        'model' => $model,
                                        'attribute' => 'end_date',
                                        'flat' => false, //remove to hide the datepicker
                                        'options' => array(
                                            'showAnim' => 'slide', //'slide','fold','slideDown','fadeIn','blind','bounce','clip','drop'
                                            'dateFormat' => 'yy-mm-dd',
                                        ),
                                        'htmlOptions' => array(
                                            'class'=> 'dates',
                                        ),
                                    ));
                                    ?></td>
                            </tr>
                            <tr>
                                <td>
                                    <?php echo $form->labelEx($model, 'seperate_pages', array('class' => 'allStaff')); ?>
                                    <?php echo $form->checkBox($model, 'seperate_pages') ?>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center"><?php echo CHtml::submitButton('Generate Social Support Report'); ?></td>
                    <td>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php $this->endWidget(); ?>
</div><!-- form -->