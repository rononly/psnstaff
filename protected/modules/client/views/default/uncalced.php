<table class="centre">
    <tr class="header">
        <td>From</td>
        <td>To</td>
        <td>Set Zero</td>
    </tr>
<?php
foreach($uncalced as $needCalc)
{
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'user-form',
        'enableAjaxValidation' => false,
    ));
echo $form->hiddenField($model, 'from_id', array('value'=>$needCalc->from_client_id));
echo $form->hiddenField($model, 'to_id', array('value'=>$needCalc->to_client_id));
?>
    <tr class="border_bottom">
    <td><?php echo $needCalc->from_property_number . ' '.$needCalc->from_postcode?></td>
    <td><?php echo $needCalc->to_property_number . ' '.$needCalc->to_postcode?></td>
        <td><?php echo CHtml::submitButton('Set');?></td>
    </tr>
<?php
$this->endWidget();
}
?>
    </table>