<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 19/01/14
 * Time: 01:05
 */
$this->breadcrumbs=array(
    'Manage Users'=>array('index'),
    'Edit User',
);
?>
<?php $compare = new SelectUser; ?>
<?php if($model == $compare) {?>
    <?php echo $this->renderPartial('_selectUser', array('model'=>$model,'users'=>$users)); ?>
<?php } else { //Is The Edit Form ?>
    <?php echo $this->renderPartial('_jobForm', array('model'=>$model)); ?>
<?php }?>