<?php
class ClientsController extends Controller
{

    private $_time_limit = 240;

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionSocialSupport()
    {
        $model = new ClientSocialSupportReportModel();

        if (isset($_POST['ClientSocialSupportReportModel'])) {
            if ($_POST['ClientSocialSupportReportModel']['all_clients'] != 1) {
                $model->clients = $_POST['ClientSocialSupportReportModel'];
            } else $model->all_clients = 1;
            $model->start_date = $_POST['ClientSocialSupportReportModel']['start_date'];
            $model->end_date = $_POST['ClientSocialSupportReportModel']['end_date'];
            $model->seperate_pages = $_POST['ClientSocialSupportReportModel']['seperate_pages'];
            if ($model->validate()) {
                Yii::app()->session['ss_model'] = $model;
                $this->redirect(array('display'));
            }
            //Model Validate
        }

        $criteria = new CDbCriteria();
        $criteria->order = 'surname ASC';
        $clients = Client::model()->findAll($criteria);
        $idents = array();
        $idents_index = 0;
        foreach ($clients as $client) {
            $idents[$idents_index]['id'] = $client['id'];
            $idents[$idents_index]['ident'] = $client['surname'] . ' ' . $client['initials'] . ' (' .
                $client['postcode'] . ')';
            $idents_index++;
        }

        $this->render('socialsupport', array('model' => $model, 'clients' => $idents));
    }

    public function actionDisplay()
    {
        $model = Yii::app()->session['ss_model'];
        //Gen Report
        $client_data = array();
        $client_id_list = array();

        if ($model->all_clients == 1) {
            $criteria = new CDbCriteria();
            $criteria->select = 'id';
            $criteria->order = 'surname ASC';
            $clientIdList = Client::model()->findAll($criteria);
            foreach ($clientIdList as $client_id) {
                $client_id_list[] = $client_id['id'];
            }
        } else {
            foreach ($model->clients['clients'] as $client) {
                $client_id_list[] = $client;
            }
        }
        /*
         * We Now have a formatted array of all the client ids we need to report on.
         * Now we need to gather all the call references for each client where the call type is social support (3)
         * between the start and end dates.
         * We will store them in the client_data array via client_data[client_id][calls][call_reference]
         */
        $report_data = array();
        foreach ($client_id_list as $client) {

            $criteria = new CDbCriteria();
            $criteria->select = 'id, date, user_id';
            $criteria->condition = 'client_id =:client_id AND date BETWEEN :start_date AND :end_date AND call_type_id = 3';
            $criteria->params = array(':client_id' => $client, ':start_date' => $model->start_date, ':end_date' => $model->end_date);
            $references = Timesheets::model()->findAll($criteria);
            $uh = new getUserName_helper();

            //Create object for client
            $client_object = new stdObject();
            $client_object->id = $client;
            $client_object->name = $uh->lookupClientName($client);
            $client_object->calls = array();

            if (count($references) > 0) {
                foreach ($references as $reference) {
                    $data = SocialSupportMileage::model()->find('call_ref=:call_ref', array(':call_ref' => $reference['id']));

                    if (!is_null($data)) {
                        if ($data['mileage'] > 0) {
                            $call = new stdObject();
                            $call->reference = $reference['id'];
                            $call->carer = $uh->makePrettyName($uh->lookUp($reference['user_id']));
                            $call->date = $reference['date'];
                            $call->mileage = $data['mileage'];
                            $client_object->total += $data['mileage'];
                        }
                        array_push($client_object->calls, $call);
                    }
                }
                array_push($report_data, $client_object);
            }
        }
        $this->render('display', array('data'=>$report_data));
    }
}
class stdObject
{
    public function __construct(array $arguments = array())
    {
        if (!empty($arguments)) {
            foreach ($arguments as $property => $argument) {
                $this->{$property} = $argument;
            }
        }
    }
}