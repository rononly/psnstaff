<?php
class administration_timesheet_helpers
{
    public function getDisplaySelectByCall($day, $user_id)
    {
        $days = Yii::app()->db->createCommand()
            ->select('*')
            ->from('timesheets')
            ->where(array('and', 'date=:date', 'user_id =:user_id'),
                array(':date' => $day, ':user_id' => $user_id))
            ->queryAll();
        return $days;
    }
    public function getAllCallsForDate($date)
    {
        $calls = Yii::app()->db->createCommand()
            ->select('*')
            ->from('timesheets')
            ->where('date=:date', array(':date'=>$date))
            ->queryAll();
        return $calls;
    }

    public function getDisplaySelectByWeekCommencing($week_commencing, $user_id)
    {
        $calls = Yii::app()->db->createCommand()
            ->select('*')
            ->from('timesheets')
            ->where(array('and', 'week_commencing =:week_commencing', 'user_id =:user_id'),
                array(':week_commencing' => $week_commencing, ':user_id' => $user_id))
            ->order('date DESC, id ASC')
            ->queryAll();
        return $calls;
    }

    public function getDisplayCallsByWeekCommencing($week_commencing, $user_id)
    {
        $calls = Yii::app()->db->createCommand()
            ->select('*')
            ->from('timesheets')
            ->where(array('and', 'week_commencing =:week_commencing', 'user_id =:user_id'),
                array(':week_commencing' => $week_commencing, ':user_id' => $user_id))
            ->order('date ASC')
            ->queryAll();
        return $calls;
    }

    public function getDisplayCallsByDay($date, $user_id)
    {

    }
    public function getDisplaySelectByDay($week_commencing, $user_id)
    {
        $display_days = array();
        $day_disp_obj = new day_display_object();
        $mins = 0;

        $days = Yii::app()->db->createCommand()
            ->select('date')
            ->from('timesheets')
            ->where(array('and', 'week_commencing=:week_commencing', 'user_id =:user_id'),
                array(':week_commencing' => $week_commencing, ':user_id' => $user_id))
            ->queryAll();

        $dates = array();
        foreach ($days as $day) {
            $dates[] = $day['date'];
        }
        $dates = array_values(array_unique($dates));

        foreach ($dates as $day) {
            $calls = Yii::app()->db->createCommand()
                ->select('minutes')
                ->from('timesheets')
                ->where(array('and', 'date=:date', 'user_id=:user_id'), array(':date' => $day, 'user_id' => $user_id))
                ->queryAll();

            foreach ($calls as $call) {
                $mins += $call['minutes'];
            }
            $day_disp_obj->date = $day;
            $day_disp_obj->calls = count($calls);
            $day_disp_obj->hours = $mins / 60;
            $mins = 0;
            $display_days[] = $day_disp_obj;
            $day_disp_obj = new day_display_object();
        }
        return $display_days;
    }
    public function getDisplaySelectByDayRange($user_id)
    {
        $display_days = array();
        $day_disp_obj = new day_display_object();
        $mins = 0;
        $dh = new date_helper();
        $dates = $dh->createDateRangeArray();

        foreach ($dates as $day) {
            $calls = Yii::app()->db->createCommand()
                ->select('minutes')
                ->from('timesheets')
                ->where(array('and', 'date=:date', 'user_id=:user_id'), array(':date' => $day, 'user_id' => $user_id))
                ->queryAll();

            foreach ($calls as $call) {
                $mins += $call['minutes'];
            }
            $day_disp_obj->date = $day;
            $day_disp_obj->calls = count($calls);
            $day_disp_obj->hours = $mins / 60;
            $mins = 0;
            $display_days[] = $day_disp_obj;
            $day_disp_obj = new day_display_object();
        }
        return $display_days;
    }
    public function getDisplaySelectByWeek($user_id)
    {
        $display_weeks = array();
        $week_disp_obj = new week_display_object();
        $mins = 0;

        $weeks = Yii::app()->db->createCommand()
            ->select('week_commencing')
            ->from('timesheets')
            ->where('user_id =:user_id', array(':user_id' => $user_id))
            ->order('week_commencing ASC')
            ->queryAll();


        $week_commencing = array();
        foreach ($weeks as $week) {
            $week_commencing[] = $week['week_commencing'];
        }
        $week_commencing = array_values(array_unique($week_commencing));

        foreach ($week_commencing as $week) {
            $days = Yii::app()->db->createCommand()
                ->select('date')
                ->from('timesheets')
                ->where(array('and', 'week_commencing =:week_commencing', 'user_id =:user_id'), array(':week_commencing' => $week, ':user_id' => $user_id))
                ->order('date ASC')
                ->queryAll();

            $dates = array();
            foreach ($days as $day) {
                $dates[] = $day['date'];
            }
            $dates = array_values(array_unique($dates));
            $callCount = 0;
            foreach ($dates as $day) {
                $calls = Yii::app()->db->createCommand()
                    ->select('minutes')
                    ->from('timesheets')
                    ->where(array('and', 'date=:date', 'user_id=:user_id'),
                        array(':date' => $day, ':user_id' => $user_id))
                    ->queryAll();

                foreach ($calls as $call) {
                    $mins += $call['minutes'];
                    $callCount++;
                }
            }
            $week_disp_obj->week_commencing = $week;
            $week_disp_obj->days = count($dates);
            $week_disp_obj->calls = $callCount;
            $week_disp_obj->hours = $mins / 60;
            $mins = 0;
            $display_weeks[] = $week_disp_obj;
            $week_disp_obj = new week_display_object();
        }
        return $display_weeks;
    }

    public function displayTimesheetTabular($week)
    {
        $getUsername = new getUserName_helper();
        $getJobRoles = new getJobRoles_helper();
        $getCallTypes = new getCallTypes_helper();
        $timesheetEntries = Yii::app()->db->createCommand()
            ->select('*')
            ->from('timesheets')
            ->where(array('and', 'week_commencing =:week_commencing', 'user_id =:user_id'), array(':week_commencing' => $week, ':user_id' => Yii::app()->user->id))
            ->queryAll();
        //We should now have all entries for the user for this week given.
        $summedTime = 0;
        echo '<h3>Name:<strong> ' . $getUsername->lookUp(Yii::app()->user->id)
            . '</strong> Job Role:<strong> ' . $getJobRoles->lookUp($getJobRoles->userLookup(Yii::app()->user->id)) . '</strong></h3>';

        echo '<table><tr><td><strong>Date</strong></td><td><strong>Initials and Postcode</strong>';
        echo '</td><td><strong>Description</strong></td><td><strong>Hours</strong></td><td><strong>';
        echo 'Total Hours</strong></td></tr>';

        foreach ($timesheetEntries as $entry) {
            echo '<tr>';
            echo '<td>';
            echo $entry['date'];
            echo '</td>';
            echo '<td>';
            echo $entry['client_initials'] . ' ' . $entry['client_postcode'];
            echo '</td>';
            echo '<td>';
            echo $getCallTypes->lookUp($entry['call_type_id']);
            echo '</td>';
            echo '<td>';
            echo ($entry['minutes'] / 60);
            $summedTime += ($entry['minutes']);
            echo '</td>';
            echo '<td>';
            echo $summedTime / 60;
            echo '</td>';
            echo '</tr>';
        }
        echo '</table>';
        echo '<h3>Total Hours on this Timesheet = ' . $summedTime / 60 . '</h3>';
    }

    public function getSubmittedDaysForUserInDateRange($user_id)
    {
        $dh = new date_helper();
        $dateRange = $dh->createDateRangeArray();
        $calls = array();
        $dates = array();
        foreach($dateRange as $date)
        {
            $matches = Yii::app()->db->createCommand()
                ->select('date')
                ->from('timesheets')
                ->where(array('and', 'user_id=:user_id', 'date=:date', 'submitted=:submitted'),
                    array(':user_id' => $user_id, ':date' => $date, ':submitted' => 1))
                ->queryAll();
            foreach($matches as $match)
            {
                array_push($calls, $match);
            }
        }
        //now clean out duplicates.
        foreach($calls as $call)
        {
                array_push($dates,$call['date']);
        }
        $calls = array_unique($dates);
        $calls = array_values($calls);
        return $calls;
    }

    public function recordSubmission($user_id, $week_commencing)
    {
        $record_submission = Yii::app()->db->createCommand()
            ->insert('timesheet_submissions', array(
                'user_id' => $user_id,
                'week_commencing' => $week_commencing,
            ));
    }

    public function submitCall($record_id)
    {
        Yii::app()->db->createCommand()
            ->update('timesheets',
                array('submitted' => '1'),
                'id=:id', array(':id' => $record_id)
            );
    }


}
