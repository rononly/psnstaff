<?php

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionFindUnOwnedCalls()
    {
        $model = new SelectUser();
        $user_selects = $this->getSelectList();

        $unowned = array();
        //get user id list.
        $ids = $this->getUser_ids();
        $calls = Timesheets::model()->findAll();
        foreach ($calls as $call) {
            if (!in_array($call['user_id'], $ids, true)) {
                $unowned[] = $call;
            }

        }

        if (isset($_POST['SelectUser'])) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'user_id=:from';
            $criteria->params = array(':from' => $_POST['SelectUser']['from_id']);
            $calls = Timesheets::model()->findAll($criteria);

            foreach ($calls as $call) {
                Timesheets::model()->updateByPk($call->id, array(
                    'user_id' => $_POST['SelectUser']['to_id'],
                ));
            }

            $guh = new getUserName_helper;
            $user_helper = new getUserName_helper();
            $from_user = $guh->lookUp($_POST['SelectUser']['from_id']);
            if ($from_user == null) {
                $from_user = 'unknown';
            }
            $to_user = $guh->lookUp($_POST['SelectUser']['to_id']);
            if ($to_user == null) {
                $to_user = 'unknown';
            }
            Yii::app()->user->setFlash('success', 'Calls Reasigned from ' .
                $user_helper->makePrettyName($from_user) . ' (' .
                $_POST['SelectUser']['from_id'] . ') to ' .
                $user_helper->makePrettyName($to_user) . ' (' .
                $_POST['SelectUser']['to_id'] . ').');
        }

        $this->render('unowned', array('data' => $unowned, 'model' => $model, 'users' => $user_selects));
    }

    public function actionShowOldClientTableClientsWithoutNewId()
    {
        $model = new SelectClientMatch();

        if (isset($_POST['SelectClientMatch'])) {
            CidTable::model()->updateByPk($_POST['SelectClientMatch']['from_ident'],
                array('id' => $_POST['SelectClientMatch']['to_id'],
                ));
            $this->redirect(array('showoldclienttableclientswithoutnewid'));

        }

        $unmatched_old = Yii::app()->db->createCommand()
            ->select('*')
            ->from('cid_table')
            ->where('id=0')
            ->queryAll();

        $new_client = Yii::app()->db->createCommand()
            ->select('*')
            ->from('client')
            ->queryAll();

        $un_matched = array();

        foreach ($new_client as $client) {
            $match = CidTable::model()->findByAttributes(array('id' => $client['id']));
            if ($match == null) {
                $un_matched[] = $client;
            }
        }

        $idents = array();
        $idents_index = 0;
        foreach ($un_matched as $need_match) {
            $idents[$idents_index]['id'] = $need_match['id'];
            $idents[$idents_index]['ident'] = $need_match['initials'] . ' ' .
                $need_match['surname'] . ' ' . '(' . $need_match['postcode'] . ')';
            $idents_index++;
        }
//die(var_dump($idents));
        $this->render('unmatched', array('model' => $model, 'clients_old' => $unmatched_old, 'idents' => $idents));
    }

    function getUser_ids()
    {
        $ids = array();
        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $idList = User::model()->findAll($criteria);
        foreach ($idList as $id) {
            $ids[] = $id['id'];
        }
        return $ids;
    }

    function getUser_ids_from_timesheets()
    {
        $criteria = new CDbCriteria();
        $criteria->select = 'user_id';
        $timesheet_ids = Timesheets::model()->findAll($criteria);
        $ids = array();
        foreach ($timesheet_ids as $id) {
            $ids[] = $id['user_id'];
        }
        array_unique($ids);
        return $ids;
    }

    function getSelectList()
    {
        $user_helper = new getUserName_helper();
        $ids = $this->getUser_ids_from_timesheets();
        $ids2 = $this->getUser_ids();
        foreach ($ids2 as $id_single) {
            if (!in_array($id_single, $ids, true)) {
                $ids[] = $id_single;
            }
        }
        $select_users = array();
        foreach ($ids as $id) {
            $user = User::model()->findByPk($id);
            if ($user) {
                $user->username = $user_helper->makePrettyName($user->username) . ' ' . $id;
                $select_users[] = $user;
            } else {
                $new_user = new User();
                $new_user->id = $id;
                $new_user->username = 'UNKNOWN' . ' ' . $id;
                $select_users[] = $new_user;
            }
        }
        return $select_users;
    }

    public function actionUpdateOldClientTableWithNewUserIds()
    {

        $unmatched = CidTable::model()->findAll('id=0'); //Old Table

        $newTable = Yii::app()->db->createCommand()
            ->select('*')
            ->from('client')
            ->queryAll();

        foreach ($unmatched as $needMatch) {
            foreach ($newTable as $clientel) {

                $possIdent1 = $clientel['initials'] . $clientel['surname'][0] . ' ' . $clientel['postcode'];
                if ($needMatch->ident == $possIdent1) {
                    CidTable::model()->updateByPk($needMatch->ident, array(
                        'id' => $clientel['id'],
                    ));
                }
            }
        }

        $this->redirect(array('showoldclienttableclientswithoutnewid'));
    }

    public function actionClientIdMatchTimesheetEntries()
    {
        $timesheet_entries = Timesheets::model()->findAll();
        $clients = CidTable::model()->findAll();
        foreach ($timesheet_entries as $timesheet) {
            foreach ($clients as $client) {
                if ($timesheet->client_initials . ' ' . $timesheet->client_postcode == $client->ident
                    || ($timesheet->client_initials == 'ONCALL' && $client->ident == 'ONCALL')
                    || ($timesheet->client_initials == 'OFFICE' && $client->ident == 'OFFICE')
                ) {
                    Timesheets::model()->updateByPk($timesheet->id, array(
                        'client_id' => $client->id,
                    ));
                }
            }
        }
        Yii::app()->user->setFlash('success', "Timesheet Entries Matched to Client Id's!");
        $this->render('index');
    }

    public function actionTimesheetEntriesNoClientId()
    {
        $model = new SelectClientMatch();

        if (isset($_POST['SelectClientMatch'])) {

            $timesheet_id = $_POST['SelectClientMatch']['id'];
            $timesheet = Timesheets::model()->findByPk($timesheet_id);
            $allRelatedTimesheets = Timesheets::model()->findAllByAttributes(
                array('client_initials'=>$timesheet->client_initials,
                'client_postcode'=>$timesheet->client_postcode));
            $client_id = $_POST['SelectClientMatch']['to_id'];
            foreach($allRelatedTimesheets as $timesheet)
            {
                Timesheets::model()->updateByPk($timesheet->id,
                    array(
                        'client_id' => $client_id,
                    ));
            }
            }


        $cls = CidTable::model()->findAll(array('order' => 'ident'));
        $idents = array();
        $idents_index = 0;
        foreach ($cls as $need_match) {
            $idents[$idents_index]['id'] = $need_match['id'];
            $idents[$idents_index]['ident'] = '(' . $need_match['postcode'] . ')'. ' ' .
                $need_match['ident'];
            $idents_index++;
        }

        $timesheets = Timesheets::model()->findAllByAttributes(array('client_id' => 0));
        $this->render('unmatchedtimesheetclientids', array('model' => $model, 'timesheets' => $timesheets, 'idents' => $idents));
    }

    public function actionStripSpacesFromNewClientTablePostcodes()
    {
        $clients = Client::model()->findAll();
        foreach($clients as $client)
        {
            Client::model()->updateByPk($client->id,
            array('postcode'=>strtoupper(str_replace(' ', '', $client->postcode))
            ));
        }
        Yii::app()->user->setFlash('success', "Client table Postcodes Normalized (All UpperCase no Spaces).");
        $this->render('index');
    }

    public function actionMissingInitialsAndPostcode()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'client_initials=:initials AND client_postcode=:postcode';
        $criteria->params = array(':initials'=>NULL, ':postcode'=>NULL);
        $timesheets = Timesheets::model()->findAll($criteria);

        $this->render('missingintialsandpostcode', array('timesheets'=>$timesheets));
    }

    public function actionFixClientDetails()
    {
        $timesheets = Timesheets::model()->findAll();

        foreach($timesheets as $timesheet)
        {
            $client = Client::model()->findByPk($timesheet['client_id']);

            $model = Timesheets::model()->findByPk($timesheet['id']);
            $model->client_initials = $client['initials'];
            $model->client_postcode = $client['postcode'];
            $model->update();
        }
    }
}