<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 19/05/14
 * Time: 21:28
 */
?>
<h3>Client Reports</h3>
<?php
$this->widget('zii.widgets.jui.CJuiButton', array(
    'buttonType'=>'link',
    'name'=>'socialsupport',
    'caption'=>'Social Support',
    'options'=>array('icons'=>'js:{primary:"ui-icon-arrowrefresh-1-s"}'),
    'url'=>$this->createUrl('/reports/clients/socialsupport'),
));
?>