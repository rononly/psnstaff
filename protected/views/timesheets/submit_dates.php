<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 22/08/13
 * Time: 12:34
 * To change this template use File | Settings | File Templates.
 */

$this->breadcrumbs = array('Time Sheets' => array('selecttimesheetweek'),'Select Dates',);


$this->menu = $menu;
?>

<h2>Choose the dates you wish to submit.</h2>

<?php echo $this->renderPartial('_form_date_range', array('model' => $model)); ?>