<?php
/* @var $this JobRolesController */
/* @var $model JobRoles */

$this->breadcrumbs=array(
	'Job Roles'=>array('index'),
	'Assign',
);

$this->menu=array(
	array('label'=>'List JobRoles', 'url'=>array('index')),
	array('label'=>'Manage JobRoles', 'url'=>array('admin')),
);
?>

<h1>Assign Job Roles</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>