<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of getUserNamesList_helper
 *
 * @author Ron
 */
class getUserNamesList_helper
{
    public function getUserNamesList($makePretty)
    {
        $names = Yii::app()->db->createCommand()
            ->select('id, username, email')
            ->from('tbl_user')
            ->order('username ASC')
            ->queryAll();

        if($makePretty)
        {
          //Make Names Pretty
            foreach($names as &$name)
            {
                $pretty = array();
                $pretty = explode(' ', $name['username']);

                foreach($pretty as &$prty)
                {
                    $prty = ucfirst($prty);
                }
                unset($prty);
                $name['username'] = implode(' ', $pretty);
            }
            unset($name);
            return $names;

        }
        return $names;
    }

}

?>
