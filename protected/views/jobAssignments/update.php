<?php
/* @var $this JobAssignmentsController */
/* @var $model JobAssignments */

$this->breadcrumbs=array(
	'Job Assignments'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List JobAssignments', 'url'=>array('index')),
	array('label'=>'Create JobAssignments', 'url'=>array('create')),
	array('label'=>'View JobAssignments', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage JobAssignments', 'url'=>array('admin')),
);
?>

<h1>Update Job Assignments <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>