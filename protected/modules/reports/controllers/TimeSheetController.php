<?php

class TimeSheetController extends Controller
{
    private $_time_limit = 240;

	public function actionIndex()
	{
        set_time_limit($this->_time_limit);
        $model = new TimeSheetReport();
        if (isset($_POST['TimeSheetReport'])) {

            $model->all_staff = $_POST['TimeSheetReport']['all_staff'];
            $model->staff = $_POST['TimeSheetReport']['staff'];
            $model->start_date = $_POST['TimeSheetReport']['start_date'];
            $model->end_date = $_POST['TimeSheetReport']['end_date'];
            $model->summary = $_POST['TimeSheetReport']['summary'];
            $model->summary_only = $_POST['TimeSheetReport']['summary_only'];
            $model->submit_data = $_POST['TimeSheetReport']['submit_data'];

            if($model->staff == NULL)
            {
                $criteria = new CDbCriteria();
                $criteria->select = 'id';
                $ids = User::model()->findAll($criteria);
                foreach ($ids as &$id) {
                    $id = $id->id;
                }
                $model->staff = $ids;
            }

            if($model->start_date == null || $model->end_date == null)
            {
                $date_helper = new date_helper();
                //Find and set start_date and end_date
                $model->start_date = date('Y-m-d', strtotime('-' . $date_helper->daysSince21st() . ' days', strtotime('today')));
                $model->end_date = date('Y-m-d', strtotime('+' . $date_helper->daysTill20th() . ' days', strtotime('today')));
            }

            if ($model->Validate()) {
                $all_html = '';
                $call_type_helpers = new administration_call_type_helpers();
                $carer_helpers = new administration_carer_helpers;
                $date_helper = new formatDateFor_helper;
                $ts_helpers = new timesheet_helpers;
                //$this->generateTimeSheetReport($model);
                $data = array();
                foreach ($model->staff as $staff_member) {
                    $criteria = new CDbCriteria();
                    $criteria->condition = 'user_id=:user_id AND date BETWEEN :start_date AND :end_date';
                    $criteria->params = array(':user_id'=>$staff_member, ':start_date'=>$model->start_date, ':end_date'=>$model->end_date);
                    $criteria->order = 'date ASC, start_time ASC';
                    $calls = Timesheets::model()->findAll($criteria);
                    foreach ($calls as $call) {
                        $calls_array[(string)$staff_member][$call->date][] = $call;
                    }
                    array_push($data, $calls_array);
                    unset($calls_array);
                }

                foreach ($data as $carer) {

                    if (count($carer) == 0) {
                        continue;
                    }
                    $name = $carer_helpers->getNameById(key($carer));
                    $name = $carer_helpers->makePrettyName($name['username']);
                    $totalHours = 0;

                    $html = '<h2>' . $name . '</h2>';
                    $html .= '<table class="middle" style="width: 500px;">';
                    $html .= '<tbody>';
                    $html .= '<tr>';
                    $html .= '<td>Date</td>';
                    $html .= '<td>Client ID</td>';
                    $html .= '<td>Call Type</td>';
                    $html .= '<td>Call Length</td>';
                    $html .= '<td>Total</td>';
                    $html .= '</tr>';

                    foreach ($carer as $days) {

                        foreach ($days as $calls) {

                            foreach ($calls as $call) {
                                $html .= '<tr>';
                                $html .= '<td>' . $call['date'] . '</td>';
                                $client = Client::model()->findByPk($call['client_id']);
                                $html .= '<td>' . $client->initials. ' ' . $client->surname . '</td>';
                                $html .= '<td>' . $call_type_helpers->lookUp($call['call_type_id']) . '</td>';
                                $html .= '<td>' . round($this->minDiff($call['start_time'],$call['end_time'])/60,2) . '</td>';
                                $totalHours += $this->minDiff($call['start_time'],$call['end_time']);
                                $html .= '<td>' . round($totalHours / 60, 2) . '</td>';
                                $html .= '</tr>';
                            }
                        }
                    }

                    $html .= '</tbody>';
                    $html .= '</table>';
                    $html .= '<h3>Total hours for period '
                        .$date_helper->Viewing($model->start_date)
                        . ' to '
                        .$date_helper->Viewing($model->end_date)
                        .' for <strong>'
                        . $name
                        . '</strong> : <strong>'
                        . round($totalHours / 60, 2)
                        . '</strong></h3>';
                    $summary[] = array('name' => $name, 'total' => round($totalHours / 60, 2));
                    $all_html .= $html;
                }

                $html .= '<h3 style="text-align: center"><strong>Summary</strong></h3>';
                $html .= '<table class="middle" style="width: 400;">';
                $html .= '<tbody>';
                $html .= '<tr>';
                $html .= '<td>Name</td>';
                $html .= '<td>Hours</td>';
                $html .= '</tr>';
                $total = 0.0;
                foreach ($summary as $summed) {
                    $html .= '<tr>';
                    $html .= '<td>' . $summed['name'] . '</td>';
                    $html .= '<td>' . $summed['total'] . '</td>';
                    $html .= '</tr>';
                    $total += $summed['total'];
                }
                $html .= '<tr>';
                $html .= '<td><strong>Total Hours Entered</strong></td>';
                $html .= '<td><strong>' . $total . '</strong></td>';
                $html .= '</tr>';
                $html .= '</tbody>';
                $html .= '</table>';
                $all_html .= $html;


                $timesheet_helper = new timesheet_helpers;
                $dh = new date_helper();
                $dates = $dh->createDateRangeArrayByDates($model->start_date, $model->end_date);
                if ($model->submit_data == 1) {
                    foreach ($model->staff as $user_id) {
                        foreach ($dates as $date) {
                            $timesheet_helper->submitUserDay($user_id, $date);
                        }
                    }
                }
                die($all_html);
            }

        }
        $this->render('index', array('model' => $model));
	}

    public function generateTimeSheetReport($model)
    {

        $data = array();
        foreach ($model->staff as $staff_member) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'user_id=:user_id AND date BETWEEN :start_date AND :end_date + INTERVAL 1 DAY';
            $criteria->params = array(':user_id'=>$staff_member, ':start_date'=>$model->start_date, ':end_date'=>$model->end_date);
            $criteria->order = 'date ASC, start_time ASC';
            $calls = Timesheets::model()->findAll($criteria);
            foreach ($calls as $call) {
                $calls_array[$staff_member][$call->date][] = $call;
            }
            array_push($data, $calls_array);
            unset($calls_array);
        }
        Yii::app()->session['_user_generated_data'] = $data;
        Yii::app()->session['_user_generated_report'] = 'Time Sheet';
        Yii::app()->session['_user_generated_report_model'] = $model;
        $dh = new date_helper();
        $dates = $dh->createDateRangeArrayByDates($model->start_date, $model->end_date);
        die(var_dump($model->submit_data));
        if ($model->submit_data == 1) {
            foreach ($model->staff as $user_id) {
                foreach ($dates as $date) {
                    $timesheet_helper->submitUserDay($user_id, $date);
               }
           }
        }
        $this->redirect(array('DisplayReport'));
    }

    public function minDiff($start,$end)
    {
        $start = strtotime($start);
        $end = strtotime($end);
        $diff = $end - $start;
        $diff = $diff /60; //Converts difference from seconds to minutes.
        if($diff <= 0.0)
        {
            $diff = 1440 + $diff;
        }
        return $diff;
    }

    public function beforeAction($action)
    {

        if (parent::beforeAction($action)) {

            $baseUrl = Yii::app()->baseUrl;

            $cs = Yii::app()->clientScript;
            $cs->registerScriptFile($baseUrl . '/js/time_sheet_reports.js');
            return true;
        }
        return false;
    }
}