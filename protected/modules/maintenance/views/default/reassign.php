<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 18/01/14
 * Time: 21:15
 * @users = all users stored.
 * @model = SelectUser Model.
 */
$this->breadcrumbs=array(
    'Maintenance'=>array('index'),
    'Reassign Calls',
);
?>
<?php echo $this->renderPartial('_select_form', array('model'=>$model,'users'=>$users)); ?>