function toggleListBox(){
    if (document.getElementById("TimeSheetReport_all_staff").checked == true) {
    staffListBoxDisable();
    jobRolesDisable();
    }
else if (document.getElementById("TimeSheetReport_all_staff").checked == false) {
    staffListBoxEnable();
    jobRolesEnable();
    }
}
function toggleListBoxAndJobRoles(){
    if(document.getElementById("TimeSheetReport_all_staff").checked == true)
    {
    staffListBoxDisable();
    jobRolesDisable();
    }
else
        {
            staffListBoxEnable();
            jobRolesEnable();
            }
}
function toggleListBoxAndAllStaffCheckBox(){
    var validCheck = false;
    var checkBoxes = document.getElementsByName("TimeSheetReport[job_role][]");
    for (var i = 0; i < checkBoxes.length; i++) {
    if (checkBoxes[i].checked == true) {
    validCheck = true;
    }
}
if (validCheck == true) {
    staffListBoxDisable();
    allStaffCheckboxDisable();
    }
else {
    staffListBoxEnable();
    allStaffCheckboxEnable();
    }
}


function allStaffCheckboxEnable(){
    document.getElementById('TimeSheetReport_all_staff').disabled = false;
    document.getElementById('TimeSheetReport_all_staff').style.opacity = 1;
    var allStaffElements = document.getElementsByClassName('allStaff');
    for(var allStaff = 0; allStaff < allStaffElements.length; allStaff++)
    {
    allStaffElements[allStaff].disabled = false;
    allStaffElements[allStaff].style.opacity = 1;
    }
}
function checkAllStaffCheckBox()
{
    document.getElementById('TimeSheetReport_all_staff').checked = true;
}
function allStaffCheckboxDisable(){
    document.getElementById('TimeSheetReport_all_staff').disabled = true;
    document.getElementById('TimeSheetReport_all_staff').style.opacity = 0.2;
    var allStaffElements = document.getElementsByClassName('allStaff');
    for(var allStaff = 0; allStaff < allStaffElements.length; allStaff++)
    {
    allStaffElements[allStaff].disabled = true;
    allStaffElements[allStaff].style.opacity = 0.2;
    }
}
function staffListBoxEnable(){
    document.getElementById('TimeSheetReport_staff').disabled = false;
    document.getElementById('TimeSheetReport_staff').style.opacity = 1;
    var staffElements = document.getElementsByClassName('staff');
    for(var staff = 0; staff < staffElements.length; staff++)
    {
    staffElements[staff].disabled = false;
    staffElements[staff].style.opacity = 1;
    }
}
function staffListBoxDisable(){
    document.getElementById('TimeSheetReport_staff').disabled = true;
    document.getElementById('TimeSheetReport_staff').style.opacity = 0.2;
    document.getElementById('TimeSheetReport_staff').selectedIndex = -1;
    var staffElements = document.getElementsByClassName('staff');
    for(var staff = 0; staff < staffElements.length; staff++)
    {
    staffElements[staff].disabled = true;
    staffElements[staff].style.opacity = 0.2;
    }
}
function jobRolesEnable(){
    var JobRoleElements = document.getElementsByClassName('jobRole');
    for (var jobRoles = 0; jobRoles < JobRoleElements.length; jobRoles++)
    {
    JobRoleElements[jobRoles].disabled = false;
    JobRoleElements[jobRoles].style.opacity = 1;
    }
}
function jobRolesDisable(){
    var JobRoleElements = document.getElementsByClassName('jobRole');
    for (var jobRoles = 0; jobRoles < JobRoleElements.length; jobRoles++)
    {
    JobRoleElements[jobRoles].disabled = true;
    JobRoleElements[jobRoles].style.opacity = 0.2;
    }
}

function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }

    var pre = document.createElement('pre');
    pre.innerHTML = out;
    document.body.appendChild(pre)
}
function checkForDates(value){
    if(value == 'chooseDates')
    {
        showDateBoxes();
    }
    else
    {
        hideDateBoxes();
    }
}