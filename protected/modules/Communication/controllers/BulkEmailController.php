<?php

class BulkEmailController extends Controller
{

	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

    public function actionSend()
    {
        //Load $model
        $model = new SendBulkEmail();

        $bulkEmails=BulkEmail::model()->findAll(array('order' => 'name ASC'));
        if(isset($_POST['SendBulkEmail']))
        {
            $model->recipients = ($_POST['SendBulkEmail']['recipients']);
            $model->name = ($_POST['SendBulkEmail']['name']);
            $model->subject = ($_POST['SendBulkEmail']['subject']);
            //
            //$model->getAttributes($_POST['SendBulkEmail']);
            if($model->validate())
            {
                //Switch gear for cycling through the list of recipients, replacing any
                //tags that need changing and send each a mail.
                /*
                 * Available ShortTags
                 * ^{user}^
                 * ^{user_no_format}^
                 */
                //Record Failed
                $failed = array();
                //Record Sent
                $sent = array();

                foreach($model->recipients as $user)
                {
                    $data = BulkEmail::model()->find('id=:name', array(':name' => $model->name));
                    $user = User::model()->find('email=:email', array(':email' => $user));

                    $names = explode(' ', $user['username']);
                    $first_name = ucfirst($names[0]);
                    $data['html'] = str_replace('^{user}^', $first_name, $data['html']);
                    $data['html'] = str_replace('^{user_no_format}^', $user['username'], $data['html']);

                    $mail = new YiiMailer();
                    $mail->IsSMTP();
                    $mail->Host = Yii::app()->params['smtp']['relay_host'];
                    $mail->Port = Yii::app()->params['smtp']['relay_port'];

                    $mail->SMTPAuth = Yii::app()->params['smtp']['relay_authenticate'];
                    $mail->Username = Yii::app()->params['smtp']['relay_username'];
                    $mail->Password = Yii::app()->params['smtp']['relay_password'];

                    $mail->SMTPDebug = Yii::app()->params['smtp']['relay_debug'];

                    $mail->ConfirmReadingTo = Yii::app()->params['smtp']['relay_read_receipt'];

                    $mail->From = Yii::app()->params['smtp']['relay_from'];
                    $mail->FromName = Yii::app()->params['smtp']['relay_from_name'];
                    $mail->Subject = $model->subject;
                    $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
                    $mail->IsHTML(true);
                    $mail->MsgHTML($data['html']);
                    $mail->AddAddress($user['email']);
                    if (!$mail->Send()) {
                        array_push($failed,$user['email']);
                    } else {
                        //record email sent
                        Yii::app()->db->createCommand()
                            ->insert('bulkemail_log', array('staff_id' => $user['id'], 'be_id' => $model->name,
                            'timestamp' => date('Y-m-d h:i:s', time())));
                        array_push($sent,$user['email']);
                    }
                }
                //render status update screen.....
                $this->render('emailstatus', array('sent' => $sent, 'failed' => $failed));
            }
        }

        $this->render('send', array('model' => $model, 'data' => $bulkEmails));
    }

    public function actionShowDescription()
    {
        $data=BulkEmail::model()->find('id=:id',
        array(':id' =>(int) $_POST['SendBulkEmail']['name']));
        echo $data['description'];
    }
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new BulkEmail;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BulkEmail']))
		{
			$model->attributes=$_POST['BulkEmail'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BulkEmail']))
		{
			$model->attributes=$_POST['BulkEmail'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('BulkEmail');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new BulkEmail('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BulkEmail']))
			$model->attributes=$_GET['BulkEmail'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return BulkEmail the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=BulkEmail::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param BulkEmail $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bulk-email-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
