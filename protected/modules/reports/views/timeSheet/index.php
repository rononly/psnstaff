<?php
/* @var $this TimeSheetController */
/* @var $model GenerateReport */

$this->breadcrumbs=array(
    'Reports'=>array('/reports/staffentered'),
    'Time Sheet Report',
);
?>
<?php $this->renderPartial('_time_sheet_form', array('model' => $model)); ?>
