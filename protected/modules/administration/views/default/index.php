<?php
/* @var $this DefaultController */
// //calling the css
//<?php echo Yii::app()->controller->module->registerCss('main.css');
$this->breadcrumbs=array(
	ucfirst($this->module->id).' Dashboard',
);
$timesheetImage = CHtml::image($this->module->assetsURL . '/images/0043.png', "Administer Timesheets");
$mileageImage = CHtml::image($this->module->assetsURL . '/images/car_sedan_blue.png', 'Administer Mileage');
$userImage = CHtml::image($this->module->assetsURL . '/images/users4.png', 'Administer Users');
$jobroleImage = CHtml::image($this->module->assetsURL . '/images/surgeon1.png', 'Administer Job Roles');
$calltypeImage = CHtml::image($this->module->assetsURL . '/images/houses.png', 'Administer Call Types');
$reportImage = CHtml::image($this->module->assetsURL . '/images/0541.png', 'Generate Reports', array('width' => '64', 'height' => '64'));

?>
<style type="text/css" media="screen">
    td.boxify {
        height: 70px;
        min-width: 96px;
        max-width: 96px;
        border: 1px solid #d0d2d0;
        text-align: center;
        -moz-border-radius: 5px;
        border-radius: 5px;
        border-spacing: 10px;
        -moz-box-shadow: 2px 2px 5px #888;
        -webkit-box-shadow: 2px 2px 5px #888;
        box-shadow: 2px 2px 5px #888;
    }
    td.label {
        width: 100px;
        font-family: Roboto;
        text-align: center;
        border-bottom: 1px solid #139ff7;
    }
    table {
        margin-left: auto;
        margin-right: auto;
        -moz-border-radius: 15px;
        border-radius: 15px;
        width: 420px;
    }
    tr.section_header {
        text-align: center;
        font-weight: bolder;
        font-size: larger;
        text-decoration-line: underline;
    }
</style>

<table><tbody>
    <tr class="section_header"><td>Reports</td><td></td><td></td><td></td></tr>
    <tr>
        <td class="boxify"><?php echo CHtml::link($reportImage,array('timesheets/index')); ?></td>
        <td class="boxify"><?php echo CHtml::link($mileageImage,array('mileage/index'),array('onclick'=>'return false;','style'=>'opacity:0.4;
filter:alpha(opacity=40);')); ?></td>
        <td class="boxify"><?php echo CHtml::link($userImage,array('user/index'),array('onclick'=>'return false;','style'=>'opacity:0.4;
filter:alpha(opacity=40);')); ?></td>
        <td class="boxify"></td>
    </tr>

    <tr>
        <td class="label">Timesheets</td>
        <td class="label">Mileage</td>
        <td class="label">Users</td>
        <td class="label"></td></tr>
    <tr></tr>
    <tr class="section_header"><td>Settings</td><td></td><td></td><td></td></tr>
    <tr>
        <td class="boxify"><?php echo CHtml::link($reportImage,array('timesheets/settings'),array('onclick'=>'return false;','style'=>'opacity:0.4;
filter:alpha(opacity=40);')); ?></td>
        <td class="boxify"><?php echo CHtml::link($mileageImage,array('mileage/settings'),array('onclick'=>'return false;','style'=>'opacity:0.4;
filter:alpha(opacity=40);')); ?></td>
        <td class="boxify"><?php echo CHtml::link($userImage,array('user/settings'),array('onclick'=>'return false;','style'=>'opacity:0.4;
filter:alpha(opacity=40);')); ?></td>
        <td class="boxify"></td>
    </tr>

    <tr>
        <td class="label">Timesheets</td>
        <td class="label">Mileage</td>
        <td class="label">Users</td>
        <td class="label"></td>
    </tr>
    <tr class="section_header"><td>Alterations</td><td></td><td></td><td></td></tr>
    <tr>
        <td class="boxify"><?php echo CHtml::link($jobroleImage,array('jobroles/alterations'),array('onclick'=>'return false;','style'=>'opacity:0.4;filter:alpha(opacity=40);')); ?></td>
        <td class="boxify"><?php echo CHtml::link($calltypeImage,array('calltypes/alterations'),array('onclick'=>'return false;','style'=>'opacity:0.4;filter:alpha(opacity=40);')); ?></td>
        <td class="boxify"><?php echo CHtml::link($userImage,array('user/alterations'),array('onclick'=>'return false;','style'=>'opacity:0.4;filter:alpha(opacity=40);')); ?></td>
        <td class="boxify"></td>
    </tr>

    <tr>
        <td class="label">Job Roles</td>
        <td class="label">Call Types</td>
        <td class="label">Users</td>
        <td class="label"></td>
    </tr>

</tbody></table>