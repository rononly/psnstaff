<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'change_mobile',
        'enableAjaxValidation' => false,
    )); ?>
    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->label($model, 'mobile'); ?>
        <?php echo $form->textField($model, 'mobile') ?>
        <?php echo $form->error($model, 'mobile'); ?>
    </div>

    <div class="row submit">
        <?php echo CHtml::submitButton('Change Mobile'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->