<?php
/**
 * Created by PhpStorm.
 * User: PSN2
 * Date: 21/01/14
 * Time: 11:52
 */
$this->breadcrumbs=array(
    'Manage Users'=>array('index'),
    'Activate User',
);
?>
<?php $compare = new SelectUser; ?>
<?php if($model == $compare) {?>
    <?php echo $this->renderPartial('_selectUser', array('model'=>$model,'users'=>$users)); ?>
<?php } else { //Is The Edit Form ?>
    <?php echo $this->renderPartial('_form', array('model'=>$model,'update'=>true)); ?>
<?php }?>