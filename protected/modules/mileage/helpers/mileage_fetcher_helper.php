<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 29/08/13
 * Time: 23:28
 * To change this template use File | Settings | File Templates.
 */

class mileage_fetcher_helper
{

    public function fetchMiles($call_ref)
    {
        $mileage = Mileage::model()->find('call_ref=:call_ref', array(':call_ref' => $call_ref));
        return $mileage;
    }

    public function fetchExtraMiles($call_ref)
    {
        $mileage = C2cExtraMileage::model()->find('call_ref=:call_ref', array(':call_ref' => $call_ref));
        return $mileage;
    }

    public function fetchExtraMilesDate($date)
    {
        $timesheets = Timesheets::model()->findAll('date=:date AND user_id=:user_id', array(':date' => $date, ':user_id' => Yii::app()->user->id));
        $miles_total = 0.0;
        foreach ($timesheets as $timesheet) {
            $mileage = C2cExtraMileage::model()->find('call_ref=:call_ref', array(':call_ref' => $timesheet['id']));
            $miles_total += $mileage->mileage;
        }
        return $miles_total;
    }

    public function fetchSSMiles($call_ref)
    {
        $mileage = SocialSupportMileage::model()->find('call_ref=:call_ref', array(':call_ref' => $call_ref));
        return $mileage;
    }

    public function fetchSSMilesDate($date)
    {
        $timesheets = Timesheets::model()->findAll('date=:date AND user_id=:user_id', array(':date' => $date, ':user_id' => Yii::app()->user->id));
        $miles_total = 0.0;
        foreach ($timesheets as $timesheet) {
            $mileage = SocialSupportMileage::model()->find('call_ref=:call_ref', array(':call_ref' => $timesheet['id']));
            $miles_total += $mileage->mileage;
        }
        return $miles_total;
    }

    public function fetchClientIds()
    {
        $result = Yii::app()->db->createCommand()
            ->select('Ident')
            ->from('cid_table')
            ->order('Ident')
            ->QueryAll();

        $ids = array();
        foreach ($result as $client_id) {
            $ids[$client_id['Ident']] = $client_id['Ident'];
        }
        return $ids;
    }

    public function getDistance($num, $pcode, $num2, $pcode2)
    {
        // http://dev.virtualearth.net/REST/v1/Routes?wp.1=Cambridge , UK&wp.2=Church St, Newcastle-under-Lyme, UK&key=ENTERYOURBINGMAPSKEYHERE

        // http://dev.virtualearth.net/REST/v1/Routes? wayPoint.1=wayPoint1&viaWaypoint.2=viaWaypoint2&waypoint.3=wayPoint3&wayPoint.n=wayPointn&distanceUnit=distanceUnit&key=BingMapsKey
        $request = 'http://dev.virtualearth.net/REST/v1/Routes?wayPoint.1=';
        if ($num != null)
            $request .= $num . ',' . $pcode;
        else
            $request += $pcode;
        $request .= '&waypoint.2=';
        if ($num2 != null)
            $request .= $num2 . ',' . $pcode2;
        else
            $request .= $pcode2;
        $request .= '&distanceUnit=mi';
        $request .= '&key=AuN7W7PVUtwPmNA7meZjLuICaP7Q57R0r5WOZV0nEaTFV9mlYdDDfM8d2FGYygSk';

        $curl = curl_init($request);
        here:
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        $dec = json_decode($result);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($httpCode == 500) {
            goto here;
        }

        return round($dec->resourceSets[0]->resources[0]->travelDistance, 1);
    }

    public function obtainGeocodes($addresses)
    {
        $bingKey = 'AuN7W7PVUtwPmNA7meZjLuICaP7Q57R0r5WOZV0nEaTFV9mlYdDDfM8d2FGYygSk';
        // Setup a credentials ‘object’ to be included as a parameter
        $geocodeServiceWsdl = 'http://dev.virtualearth.net/webservices/v1/metadata/geocodeservice/geocodeservice.wsdl';
        $credentials = array('ApplicationId' => $bingKey);
        // Create a SOAP client for the Bing Maps SOAP Geocode service using its WSDL
        $geocodeClient = new SoapClient($geocodeServiceWsdl, array('trace' => 1));

        // Construct address 'object'
        $address = array(
            'AddressLine' => $addresses['house_number_from'],
            'PostalCode' => $addresses['house_postcode_from'],
        );
        // Construct a Geocode request "object" using an address instead of a query string
        $geocodeRequest = array(
            'Credentials' => $credentials,
            'Address' => $address

        );

        // Send the Geocode request to the web service and receive a response
        try {
            $geocodeResponse = $geocodeClient->Geocode(array('request' => $geocodeRequest, 'Count' => 1));
        } catch (SoapFault $e) {
            die('Fault occurred using Web Service: ' . $e->getMessage());
        }

        // Ouput geocoded latitude and longitude of provided address
        if (is_array($geocodeResponse->GeocodeResult->Results->GeocodeResult)) {
            if (is_array($geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
                Locations->GeocodeLocation)
            ) {
                $lat = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
                    Locations->GeocodeLocation[0]->Latitude;
                $lon = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
                    Locations->GeocodeLocation[0]->Longitude;
            } else {
                $lat = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
                    Locations->GeocodeLocation->Latitude;
                $lon = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
                    Locations->GeocodeLocation->Longitude;
            }
        } else {
            $lat = $geocodeResponse->GeocodeResult->Results->GeocodeResult->
                Locations->GeocodeLocation->Latitude;
            $lon = $geocodeResponse->GeocodeResult->Results->GeocodeResult->
                Locations->GeocodeLocation->Longitude;
        }

        $originLocation = array(
            'Latitude' => $lat,
            'Longitude' => $lon
        );

        //Location 2
        $geocodeClient = new SoapClient($geocodeServiceWsdl, array('trace' => 1));

        // Construct address 'object'
        $address = array(
            'AddressLine' => $addresses['house_number_from'],
            'PostalCode' => $addresses['house_postcode_from'],
        );

        // Construct a Geocode request "object" using an address instead of a query string
        $geocodeRequest = array(
            'Credentials' => $credentials,
            'Address' => $address
        );

        // Send the Geocode request to the web service and receive a response
        try {
            $geocodeResponse = $geocodeClient->Geocode(array('request' => $geocodeRequest, 'Count' => 1));
        } catch (SoapFault $e) {
            die('Fault occurred using Web Service: ' . $e->getMessage());
        }

        // Ouput geocoded latitude and longitude of provided address
        if (is_array($geocodeResponse->GeocodeResult->Results->GeocodeResult)) {
            if (is_array($geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
                Locations->GeocodeLocation)
            ) {
                $lat = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
                    Locations->GeocodeLocation[0]->Latitude;
                $lon = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
                    Locations->GeocodeLocation[0]->Longitude;
            } else {
                $lat = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
                    Locations->GeocodeLocation->Latitude;
                $lon = $geocodeResponse->GeocodeResult->Results->GeocodeResult[0]->
                    Locations->GeocodeLocation->Longitude;
            }
        } else {
            $lat = $geocodeResponse->GeocodeResult->Results->GeocodeResult->
                Locations->GeocodeLocation->Latitude;
            $lon = $geocodeResponse->GeocodeResult->Results->GeocodeResult->
                Locations->GeocodeLocation->Longitude;
        }
        $destLocation = array(
            'Latitude' => $lat,
            'Longitude' => $lon
        );

        $converted_addresses = array(
            'from' => $originLocation,
            'to' => $destLocation
        );

        return $converted_addresses;
    }

    public function obtainDistance($addresses)
    {
        $DEBUG = true;
        $bingKey = 'key=AtPKu-E8hgIWKz5fJH-cotVWFT12kD6SbpKp-HfJphP8VayhXSbrDA19Jzyl9mgn';
        $restRouteUrl = 'http://dev.virtualearth.net/REST/v1/Routes?';
        //waypoints from entry.
        $waypoints = 'wayPoint.1=';
        if ($addresses['house_number_from'] != null) {
            $waypoints .= $addresses['house_number_from'] . ',';
        }
        $waypoints .= $addresses['house_postcode_from'];
        $waypoints .= '&waypoint.2=';
        if ($addresses['house_number_to'] != null) {
            $waypoints .= $addresses['house_number_to'] . ',';
        }
        $waypoints .= $addresses['house_postcode_to'] . '&';
        $optimize = 'optimise=distance';
        $maxSolutions = 'maxSolutions=3';
        $routePathOutput = 'routePathOutput=none';
        $distanceUnit = 'distanceUnit=mi';

        //Build Request
        if ($DEBUG) {
            print_r('Distance Lookup ::');
        }
        $request = $restRouteUrl . $waypoints . $optimize . '&' . $maxSolutions . '&' .
            $routePathOutput . '&' . $distanceUnit . '&' . $bingKey;
        if ($DEBUG) {
            print_r('Request = :: ' . $request);
        }
        $curl = curl_init($request);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        $dec = json_decode($result);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if ($httpCode != 200) {
            if ($DEBUG) {
                print_r('Distance Status Code returned was :: ' . $httpCode);
            }

        }
        if ($httpCode == 500) {
            return null;
        }
        $results = array();
        foreach($dec->resourceSets as $rSet)
        {
            foreach($rSet->resources as $rSources)
            {
                $results[] = round($rSources->travelDistance, 1);
            }
        }
        return $results;
    }
    public function obtainShortestDistance($addresses)
    {
        $distances = array();
        $time = $this->obtainDistanceTime($addresses);
        if (!empty($time)) {
            foreach($time as $routeDistance)
            {
                $distances[] = $routeDistance;
            }
        }
        $dist = $this->obtainDistance($addresses);
        if (!empty($dist)) {
            foreach($dist as $routeDistance)
            {
                $distances[] = $routeDistance;
            }
        }
        $timewt = $this->obtainDistanceTimeWithTraffic($addresses);
        if (!empty($timewt)) {
            foreach($timewt as $routeDistance)
            {
                $distances[] = $routeDistance;
            }
        }
        $shortestDistance = 50000;
        foreach($distances as $distance)
        {
            if($distance < $shortestDistance)
            {
                $shortestDistance = $distance;
            }
        }
        return round($shortestDistance,1);
    }
    public function obtainDistanceTimeWithTraffic($addresses)
    {
        $DEBUG = true;
        $bingKey = 'key=AtPKu-E8hgIWKz5fJH-cotVWFT12kD6SbpKp-HfJphP8VayhXSbrDA19Jzyl9mgn';
        $restRouteUrl = 'http://dev.virtualearth.net/REST/v1/Routes?';
        //waypoints from entry.
        $waypoints = 'wayPoint.1=';
        if ($addresses['house_number_from'] != null) {
            $waypoints .= $addresses['house_number_from'] . ',';
        }
        $waypoints .= $addresses['house_postcode_from'];
        $waypoints .= '&waypoint.2=';
        if ($addresses['house_number_to'] != null) {
            $waypoints .= $addresses['house_number_to'] . ',';
        }
        $waypoints .= $addresses['house_postcode_to'] . '&';
        $optimize = 'optimise=timeWithTraffic';
        $maxSolutions = 'maxSolutions=3';
        $routePathOutput = 'routePathOutput=none';
        $distanceUnit = 'distanceUnit=mi';

        //Build Request
        if ($DEBUG) {
            print_r('Distance Lookup ::');
        }
        $request = $restRouteUrl . $waypoints . $optimize . '&' . $maxSolutions . '&' .
            $routePathOutput . '&' . $distanceUnit . '&' . $bingKey;
        if ($DEBUG) {
            print_r('Request = :: ' . $request);
        }
        $curl = curl_init($request);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        $dec = json_decode($result);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if ($httpCode != 200) {
            if ($DEBUG) {
                print_r('Distance Status Code returned was :: ' . $httpCode);
            }

        }
        if ($httpCode == 500) {
            return null;
        }
        $results = array();
        foreach($dec->resourceSets as $rSet)
        {
            foreach($rSet->resources as $rSources)
            {
                $results[] = round($rSources->travelDistance, 1);
            }
        }
        return $results;
    }
    public function obtainDistanceTime($addresses)
    {
        $DEBUG = true;
        $bingKey = 'key=AtPKu-E8hgIWKz5fJH-cotVWFT12kD6SbpKp-HfJphP8VayhXSbrDA19Jzyl9mgn';
        $restRouteUrl = 'http://dev.virtualearth.net/REST/v1/Routes?';
        //waypoints from entry.
        $waypoints = 'wayPoint.1=';
        if ($addresses['house_number_from'] != null) {
            $waypoints .= $addresses['house_number_from'] . ',';
        }
        $waypoints .= $addresses['house_postcode_from'];
        $waypoints .= '&waypoint.2=';
        if ($addresses['house_number_to'] != null) {
            $waypoints .= $addresses['house_number_to'] . ',';
        }
        $waypoints .= $addresses['house_postcode_to'] . '&';
        $optimize = 'optimise=time';
        $maxSolutions = 'maxSolutions=3';
        $routePathOutput = 'routePathOutput=none';
        $distanceUnit = 'distanceUnit=mi';

        //Build Request
        if ($DEBUG) {
            print_r('Distance Lookup ::');
        }
        $request = $restRouteUrl . $waypoints . $optimize . '&' . $maxSolutions . '&' .
            $routePathOutput . '&' . $distanceUnit . '&' . $bingKey;
        if ($DEBUG) {
            print_r('Request = :: ' . $request);
        }
        $curl = curl_init($request);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        $dec = json_decode($result);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if ($httpCode != 200) {
            if ($DEBUG) {
                print_r('Distance Status Code returned was :: ' . $httpCode);
            }

        }
        if ($httpCode == 500) {
            return null;
        }
        $results = array();
        foreach($dec->resourceSets as $rSet)
        {
            foreach($rSet->resources as $rSources)
            {
                $results[] = round($rSources->travelDistance, 1);
            }
        }
        return $results;
    }

    /**
     * Find mileage from this client to all other clients
     * @param $params
     */
    public function calculateFromClientToOthers($params)
    {

        $clients_information = CidTable::model()->findAll();

        foreach ($clients_information as $client) {
            if ($params['ident'] == $client->ident || $client->ident == 'ONCALL')
                continue;

            $reference = C2cMileageRefTime::model()->findByAttributes(array(
                'client_from' => $params['ident'], 'client_to' => $client->ident));

            if ($reference == null) {
                if (!empty($client->property_no)) {
                    $addresses = array(
                        'house_number_from' => $params['property_no'],
                        'house_postcode_from' => $params['postcode'],
                        'house_number_to' => $client->property_no,
                        'house_postcode_to' => $client->postcode,
                    );
                }
                if (empty($client->property_no)) {
                    $addresses = array(
                        'house_number_from' => $params['property_no'],
                        'house_postcode_from' => $params['postcode'],
                        'house_postcode_to' => $client->postcode,
                    );
                }
                $distance = $this->obtainShortestDistance($addresses);
                $c2c_mileage_ref = new C2cMileageRefTime();
                $c2c_mileage_ref->client_from = $params['ident'];
                $c2c_mileage_ref->client_to = $client->ident;
                $c2c_mileage_ref->mileage = $distance;
                $c2c_mileage_ref->save();
            }
        }
    }

    /**
     * Find mileage from all clients to this client
     * @param $params
     */
    public function calculateFromOthersToClient($params)
    {
        $clients_information = CidTable::model()->findAll();

        foreach ($clients_information as $client) {
            if ($params['ident'] == $client->ident || $client->ident == 'ONCALL')
                continue;

            $reference = C2cMileageRefTime::model()->findByAttributes(array(
                'client_from' => $client->ident, 'client_to' => $params['ident']));

            if ($reference == null) {
                if (!empty($params['property_no'])) {
                    $addresses = array(
                        'house_number_from' => $client->property_no,
                        'house_postcode_from' => $client->postcode,
                        'house_number_to' => $params['property_no'],
                        'house_postcode_to' => $params['postcode'],
                    );
                }
                if (empty($params['property_no'])) {
                    $addresses = array(
                        'house_number_from' => $client->property_no,
                        'house_postcode_from' => $client->postcode,
                        'house_postcode_to' => $params['postcode'],
                    );
                }
                $distance = $this->obtainShortestDistance($addresses);
                $c2c_mileage_ref = new C2cMileageRefTime();
                $c2c_mileage_ref->client_from = $client->ident;
                $c2c_mileage_ref->client_to = $params['ident'];
                $c2c_mileage_ref->mileage = $distance;
                $c2c_mileage_ref->save();
            }
        }
    }
}