<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of date_helper
 *
 * @author Ron
 */
class administration_date_helpers
{

    public function daysSince21st()
    {

//If todays date is less than 21st
        if (date('d') < 21) {
//Get the month
            $month = date('m', strtotime('-1 month'));
        } else
            $month = date('m');
//And year
        $year = date('Y');
//die($year);
//Create the date of the 21st of last month
//But if now is January then the year should be last year
        if ($month == 01) {
            $year = date('Y', strtotime('-1 year'));
        }
        $lastTwentyFirst = sprintf('%d-%02d-21', $year, $month);
        $lastTwentyFirst = new DateTime($lastTwentyFirst);
//die($lastTwentyFirst);
//Get todays date
        $today = date('Y-m-d');
        $today = new DateTime($today);
//Calculate days since $lastTwentyFirst
        $daysSinceLast21st = $lastTwentyFirst->diff($today);

        return ($daysSinceLast21st->format('%a'));
    }

    public function daysTill20th()
    {
        if (date('d') > 20) {
            $month = date('m', strtotime('+1 month'));
        } else $month = date('m');
        if ($month == 12) {
            $year = date('Y', strtotime('+1 year'));
        } else $year = date('Y');
        //Construct next 20th
        $nextTwentyith = sprintf('%d-%02d-20', $year, $month);
        $nextTwentyith = new DateTime($nextTwentyith);
        $today = date('Y-m-d');
        $today = new DateTime($today);
        $daysTillNext20th = $today->diff($nextTwentyith);
        return ($daysTillNext20th->format('%a'));
    }

    public function createDateRangeArray()
    {
        $month_offset = 0;
        if (isset(Yii::app()->user->month_offset))
        {
            $month_offset = Yii::app()->user->month_offset;
        }
        $strDateFrom = strtotime('-' . $this->daysSince21st() . 'day', strtotime(date('Y-m-d')));
        $strDateTo = strtotime('+' . $this->daysTill20th() . 'day', strtotime(date('Y-m-d')));

        //any offset needs calculating here.
        if($month_offset < 0)
        {
            $strDateFrom = strtotime($month_offset.'month', $strDateFrom);
            $strDateTo = strtotime($month_offset.'month', $strDateTo);
        }
        $aryRange = array();

        $iDateFrom = $strDateFrom;//mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = $strDateTo;//mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        return $aryRange;
    }

    public function createDateRangeArrayByDates($strDateFrom,$strDateTo)
    {
        $strDateFrom = strtotime($strDateFrom);
        $strDateTo = strtotime($strDateTo);

        // die($strDateFrom . '     ' . $strDateTo);
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.

        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange = array();

        $iDateFrom = $strDateFrom;//mktime(1, 0, 0, substr($strDateFrom, 5, 2), substr($strDateFrom, 8, 2), substr($strDateFrom, 0, 4));
        $iDateTo = $strDateTo;//mktime(1, 0, 0, substr($strDateTo, 5, 2), substr($strDateTo, 8, 2), substr($strDateTo, 0, 4));

        if ($iDateTo >= $iDateFrom) {
            array_push($aryRange, date('Y-m-d', $iDateFrom)); // first entry
            while ($iDateFrom < $iDateTo) {
                $iDateFrom += 86400; // add 24 hours
                array_push($aryRange, date('Y-m-d', $iDateFrom));
            }
        }
        return $aryRange;
    }
}
?>