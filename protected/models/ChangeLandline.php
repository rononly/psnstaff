<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ChangePassword
 *
 * @author Ron
 */
class ChangeLandline extends CFormModel
{

    public $landline;

    public function rules()
    {
        return array(
            array('landline', 'length', 'min' => 10, 'max' => 11),
            array('landline', 'numerical', 'integerOnly' => true),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'landline' => 'New Landline Number',
        );
    }

}
?>
