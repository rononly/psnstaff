<style type="text/css" media="screen">
    div.loading {
        background-color: #eee;
        background-image: url('images/loading2.gif');
        background-position:  center center;
        background-repeat: no-repeat;
        opacity: 1;
        min-width: 600px;
        min-height: 400px;
    }
    div.loading * {
        opacity: .8;
    }
    div.error {
        background-color: #eee;
        background-image: url('images/Image2.gif');
        background-position:  center center;
        background-repeat: no-repeat;
        opacity: 1;
        min-width: 600px;
        min-height: 400px;
    }
    div.error * {
        opacity: .6;
    }
</style>
<script>
    function run()
    {
    $.ajax({
        url: '"' . <?php echo CController::createUrl('ReferenceAdmin/calculateuncalculated') ?> . '"',
        type: "POST",
        beforeSend: function(){$("#load").addClass("loading");},
        success: function(){$("#load").removeClass("loading");},
        error: run()
    })
    }
</script>
<?php
/* @var $this ReferenceAdminController */

$this->breadcrumbs=array(
	'Reference Admin',
);
?>
<div id="load">
<?php echo CHtml::link('Calculate Uncalculated', '#', array('onClick'=>'run()')); ?>
</div>