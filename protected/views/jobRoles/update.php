<?php
/* @var $this JobRolesController */
/* @var $model JobRoles */

$this->breadcrumbs=array(
	'Job Roles'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List JobRoles', 'url'=>array('index')),
	array('label'=>'Create JobRoles', 'url'=>array('create')),
	array('label'=>'View JobRoles', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage JobRoles', 'url'=>array('admin')),
);
?>

<h1>Update Job Roles <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>