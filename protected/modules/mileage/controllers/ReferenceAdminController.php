<?php

class ReferenceAdminController extends Controller
{

    public function actionCalculateReferences()
    {
        if (isset($_POST)) {
            $client_ids = CidTable::model()->findAll();
        }
        $this->render('calculateReferences');
    }

    public function actionCalculateUncalculated()
    {

        $mfh = new mileage_fetcher_helper();
        $clients_information = CidTable::model()->findAll();

        foreach ($clients_information as $client_information) {

            foreach ($clients_information as $client_info) {

                if ($client_information['ident'] == $client_info['ident']
                    || $client_information['ident'] == 'ONCALL' || $client_info['ident'] == 'ONCALL'
                ) {
                    continue;
                } else {
                    $reference = C2cMileageRefTime::model()->findByAttributes(array(
                        'client_from' => $client_information['ident'], 'client_to' => $client_info['ident']));

                    if ($reference == null) {
                        $addresses = array(
                            'house_number_from' => $client_information['property_no'],
                            'house_postcode_from' => $client_information['postcode'],
                            'house_number_to' => $client_info['property_no'],
                            'house_postcode_to' => $client_info['postcode']
                        );

                        $distance = $mfh->obtainShortestDistance($addresses);

                        $c2c_mileage_ref = new C2cMileageRefTime();
                        $c2c_mileage_ref->client_from = $client_information['ident'];
                        $c2c_mileage_ref->client_to = $client_info['ident'];
                        $c2c_mileage_ref->mileage = $distance;
                        $c2c_mileage_ref->save();
                    }

                }
            }
        }
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionUpdateReference()
    {
        $this->render('updateReference');
    }

    public function actionStatistics()
    {
        $this->render('statistics');
    }
    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'inlineFilterName',
            array(
                'class'=>'path.to.FilterClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
            'action1'=>'path.to.ActionClass',
            'action2'=>array(
                'class'=>'path.to.AnotherActionClass',
                'propertyName'=>'propertyValue',
            ),
        );
    }
    */
}