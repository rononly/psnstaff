<?php
/* @var $this JobRolesController */
/* @var $model JobRoles */

$this->breadcrumbs = array(
    'Job Roles' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'List JobRoles', 'url' => array('index')),
    array('label' => 'Create JobRoles', 'url' => array('create')),
    array('label' => 'Update JobRoles', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete JobRoles', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage JobRoles', 'url' => array('admin')),
);
?>

    <h1>View Job Roles #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'job_role',
    ),
)); ?>