<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of getCallTypes_helper
 *
 * @author Ron
 */
class getCallTypes_helper
{
    public function getCallTypes()
    {
        $callTypes = Yii::app()->db->createCommand()
            ->select('*')
            ->from('call_types')
            ->order('call_type ASC')
            ->queryAll();
        return $callTypes;
    }

    public function lookUp($id)
    {
        $callType = Yii::app()->db->createCommand()
            ->select('call_type')
            ->from('call_types')
            ->where('id=:id', array(':id' => $id))
            ->queryRow();
        return $callType['call_type'];
        
    }
}

?>
