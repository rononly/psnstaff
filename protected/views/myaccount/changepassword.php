<?php
/* @var $this MyaccountController */
/* @var $model ChangePassword */

$this->breadcrumbs = array(
    'My Account' => array('/myaccount'),
    'Change Password',
);

$mh = new myaccount_helpers();
$this->menu = $mh->ControllerMenu();
?>

    <h1>Change Password</h1>

<?php echo $this->renderPartial('_CPForm', array('model' => $model)); ?>