<?php
/* @var $this StaffenteredController */

$this->breadcrumbs=array(
    'Staff-Entered'=>array('/reports/staffentered'),
    'Logon Report',
);
?>
<?php $this->renderPartial('_logon_report_form', array('model' => $model, 'data'=>$data, 'lastlogin'=>$lastlogin)); ?>