<?php
/**
 * Description of RBAC_helper
 *
 * @author Ron
 */
class RBAC_helper
{
    public function is($role)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'userid=:userid';
        $criteria->params = array(':userid'=>Yii::app()->user->id);

        $users_roles = Assignments::model()->findAll($criteria);

        $roles = array();
        foreach ($users_roles as $existing_role)
            $roles[] = $existing_role->itemname;
        if (in_array($role, $roles))
        {
            return true;
        }

        return false;
    }
}