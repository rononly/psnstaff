<?php
/* @var $this MileageController */
/* @var $model Mileage */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mileage-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'call_ref'); ?>
		<?php echo $form->textField($model,'call_ref'); ?>
		<?php echo $form->error($model,'call_ref'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'client_initials_1'); ?>
		<?php echo $form->textField($model,'client_initials_1',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'client_initials_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'client_postcode_1'); ?>
		<?php echo $form->textField($model,'client_postcode_1',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'client_postcode_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'client_initials_2'); ?>
		<?php echo $form->textField($model,'client_initials_2',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'client_initials_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'client_postcode_2'); ?>
		<?php echo $form->textField($model,'client_postcode_2',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'client_postcode_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'c2c_miles'); ?>
		<?php echo $form->textField($model,'c2c_miles',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'c2c_miles'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ss_miles'); ?>
		<?php echo $form->textField($model,'ss_miles',array('size'=>3,'maxlength'=>3)); ?>
		<?php echo $form->error($model,'ss_miles'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ss_description'); ?>
		<?php echo $form->textField($model,'ss_description',array('size'=>60,'maxlength'=>1024)); ?>
		<?php echo $form->error($model,'ss_description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'approved'); ?>
		<?php echo $form->textField($model,'approved'); ?>
		<?php echo $form->error($model,'approved'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'approved_by'); ?>
		<?php echo $form->textField($model,'approved_by',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'approved_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->