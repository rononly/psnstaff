<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 03/09/13
 * Time: 10:26
 * To change this template use File | Settings | File Templates.
 */

class UserList extends CFormModel{
    public $userlist;

    public function rules()
    {
        return array(
            array('userlist', 'checkChecks'),
        );
    }

    public function checkChecks($attribute)
    {
        if(is_array($this->$attribute))
            return;
        else {
            $this->addError($attribute, 'You have not selected anyone!');
        }
    }
}