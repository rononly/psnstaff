<?php
/**
 * Created by PhpStorm.
 * User: Ron
 * Date: 18/01/14
 * Time: 21:41
 */
class SelectUser extends CFormModel {

    public $to_id;
    public $from_id;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function attributeLabels()
    {
        return array(
            'to_id' => 'To User_ID',
            'from_id' => 'From User_ID'
        );
    }
}