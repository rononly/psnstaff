<?php
/* @var $this MileageController */
/* @var $model Mileage */

$this->breadcrumbs=array(
	'Mileages'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Mileage', 'url'=>array('index')),
	array('label'=>'Create Mileage', 'url'=>array('create')),
	array('label'=>'Update Mileage', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Mileage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Mileage', 'url'=>array('admin')),
);
?>

<h1>View Mileage #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'call_ref',
		'c2c_miles',
		'date',
	),
)); ?>
