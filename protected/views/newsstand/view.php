<?php
/* @var $this NewsStandController */
/* @var $model NewsStand */

$this->breadcrumbs = array(
    'News Stands' => array('index'),
    $model->id,
);

$this->menu = array(
    array('label' => 'List NewsStand', 'url' => array('index')),
    array('label' => 'Create NewsStand', 'url' => array('create')),
    array('label' => 'Update NewsStand', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete NewsStand', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage NewsStand', 'url' => array('admin')),
);
?>

    <h1>View NewsStand #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'user_id',
        'news_id',
    ),
)); ?>