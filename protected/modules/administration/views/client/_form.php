<?php
/* @var $this CidTableController */
/* @var $model CidTable */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cid-table-_form-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>

    <table class="centre" style="max-width: 300px;">
	<tr class="border_bottom">
        <td>
		<?php echo $form->labelEx($model,'ident'); ?>
		<?php echo $form->textField($model,'ident'); ?>
		<?php echo $form->error($model,'ident'); ?>
        </td>
	</tr>

	<tr class="border_bottom">
        <td>
		<?php echo $form->labelEx($model,'postcode'); ?>
		<?php echo $form->textField($model,'postcode'); ?>
		<?php echo $form->error($model,'postcode'); ?>
        </td>
	</tr>

	<tr class="border_bottom">
        <td>
		<?php echo $form->labelEx($model,'property_no'); ?>
		<?php echo $form->textField($model,'property_no'); ?>
		<?php echo $form->error($model,'property_no'); ?>
        </td>
	</tr>


	<tr>
        <td style="text-align: center;">
		<?php echo CHtml::submitButton('Submit'); ?>
        </td>
	</tr>
    </table>
<?php $this->endWidget(); ?>

</div><!-- form -->