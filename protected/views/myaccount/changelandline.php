<?php
/* @var $this MyaccountController */
/* @var $model ChangePassword */

$this->breadcrumbs = array(
    'My Account' => array('/myaccount'),
    'Change Landline',
);

$mh = new myaccount_helpers();
$this->menu = $mh->ControllerMenu();
?>

    <h1>Change Landline</h1>

<?php echo $this->renderPartial('_CLForm', array('model' => $model)); ?>