<?php
/* @var $this DefaultController */
/* @var $model News */
/* @var $roles JobRole List for listbox*/

$this->breadcrumbs=array(
	'Add News',
);
?>
<?php echo $this->renderPartial('_news_form', array('model'=>$model, 'roles'=>$roles)); ?>