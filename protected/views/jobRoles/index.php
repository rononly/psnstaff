<?php
/* @var $this JobRolesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Job Roles',
);

$this->menu = array(
    array('label' => 'Create JobRoles', 'url' => array('create')),
    array('label' => 'Manage JobRoles', 'url' => array('admin')),
);
?>

    <h1>Job Roles</h1>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
)); ?>