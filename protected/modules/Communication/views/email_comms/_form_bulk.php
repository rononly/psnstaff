<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Ron
 * Date: 20/08/13
 * Time: 22:41
 * To change this template use File | Settings | File Templates.
 */
?>
<div class="form" style="width: 525px;">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'bulk_email-form',
        'enableAjaxValidation' => false,
    ));
    $getUserNameList = new getUserNamesList_helper();
    $users = $getUserNameList->getUserNamesList(true);
    ?>
    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row" style="float: left;">
        <?php echo $form->labelEx($model, 'users'); ?>
        <?php echo $form->listBox($model, 'users', CHtml::listData($users, 'email', 'username'), array('multiple' => 'multiple', 'size' => 18));?>
        <?php echo $form->error($model, 'users'); ?>
    </div>

    <div class="row" style="float: right;">
        <?php echo $form->labelEx($model, 'subject'); ?>
        <?php echo $form->textField($model, 'subject', array('size' => 60)); ?>
        <?php echo $form->error($model, 'subject'); ?>
    </div>

    <div class="row" style="float: right;">
        <?php echo $form->labelEx($model, 'message'); ?>
        <?php echo $form->textArea($model, 'message', array('style' => 'width: 387px; height: 200px;')); ?>
        <?php echo $form->error($model, 'message'); ?>
    </div>

    <div class="row buttons" style="float: right;">
        <?php echo CHtml::submitButton('Send Email'); ?>
    </div>

    <?php $this->endWidget();
    $this->widget('ext.editMe.widgets.ExtEditMe', array(
    'name'=>'example',
    'value'=>'',
    ));
?>


</div>