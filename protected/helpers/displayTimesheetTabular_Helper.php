<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of displayTimesheetTabular_Helper
 *
 * @author Ron
 */
class displayTimesheetTabular_Helper
{
    public function displayTimesheetTabular($week)
    {
        $getUsername = new getUserName_helper();
        $getJobRoles = new getJobRoles_helper();
        $getCallTypes = new getCallTypes_helper();
        $timeshetEntries = Yii::app()->db->createCommand()
            ->select('*')
            ->from('timesheets')
            ->where(array('and', 'week_commencing =:week_commencing', 'user_id =:user_id'), array(':week_commencing' => $week, ':user_id' => Yii::app()->user->id))
            ->queryAll();
        //We should now have all entries for the user for this week given.
        $summedTime = 0;
        echo '<h3>Name:<strong> ' . $getUsername->lookUp(Yii::app()->user->id)
            . '</strong> Job Role:<strong> ' . $getJobRoles->lookUp($getJobRoles->userLookup(Yii::app()->user->id)) . '</strong></h3>';

        echo '<table><tr><td><strong>Date</strong></td><td><strong>Initials and Postcode</strong>';
        echo '</td><td><strong>Description</strong></td><td><strong>Hours</strong></td><td><strong>';
        echo 'Total Hours</strong></td></tr>';

        foreach ($timeshetEntries as $entry) {
            echo '<tr>';
            echo '<td>';
            echo $entry['date'];
            echo '</td>';
            echo '<td>';
            echo $entry['client_initials'] . ' ' . $entry['client_postcode'];
            echo '</td>';
            echo '<td>';
            echo $getCallTypes->lookUp($entry['call_type_id']);
            echo '</td>';
            echo '<td>';
            echo ($entry['minutes'] / 60);
            $summedTime += ($entry['minutes']);
            echo '</td>';
            echo '<td>';
            echo $summedTime / 60;
            echo '</td>';
            echo '</tr>';

        }
        echo '</table>';
        echo '<h3>Total Hours on this Timesheet = ' . $summedTime / 60 . '</h3>';
    }
}

?>
