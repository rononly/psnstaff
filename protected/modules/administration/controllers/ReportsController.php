<?php

class ReportsController extends Controller
{
	public function actionByCarers()
	{
        $model = new UserList();

        $helper = new administration_carer_helpers();
        $carerNames = $helper->getNames();
		if(isset($_POST['UserList']))
        {
            $model->setAttributes($_POST['UserList']);
            if($model->validate())
            {
                $this->redirect('anywhere');
            }
        }
        $this->render('ByCarers', array( 'model' => $model, 'carers' => $carerNames));
	}

	public function actionByClients()
	{
		$this->render('ByClients');
	}

	public function actionByDateRange()
	{
        $model = new DateRange();

		$this->render('ByDateRange', array('model'=>$model));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}