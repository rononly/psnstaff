<?php
/* @var $this TimesheetsController */
/* @var $model Timesheets */

$this->breadcrumbs = array(
    'Time Sheets' => array('selecttimesheetday'),
    'View By Day' => array('selecttimesheetday'),
    'Submitted Calls',
);
$this->menu = $menu
?>
<div id="Sheet_Display">
    <?php
    $select = new timesheet_helpers();
    $data = $select->getDisplaySelectByCall($date, Yii::app()->user->id);
    $formatter = new formatDateFor_helper();
    $callTypeLU = new getCallTypes_helper();

    ?>
    <?php
    //die(var_dump($data));
    if ($data == null) {
        if (strpos(Yii::app()->request->urlReferrer, 'timesheets/update') !== false) {
            $this->redirect(array('/timesheets/selecttimesheetday'));
        }
    } else {
        echo '</br></br>';
        echo '<table border="1" cellpadding="1" cellspacing="1" style="width: 600px; padding-top: 10px;"><tbody>';
        echo '<tr><td>Date</td><td>Initials + Postcode<td>Call Type</td><td>
                        Hours</td><td>Total</td></tr>';
        $totalHours = 0;
        foreach ($data as $call) {
            echo '<tr>';
            echo '<td>' . CHtml::link($formatter->Viewing($call['date']), array('timesheets/update',
                'id' => $call['id'], 'date' => $call['date'])) . '</td>';
            echo '<td>' . $call['client_initials'] . ' ' . $call['client_postcode'] . '</td>';
            echo '<td>' . $callTypeLU->lookUp($call['call_type_id']) . '</td>';
            echo '<td>' . ($call['minutes'] / 60) . '</td>';
            $totalHours += $call['minutes'];
            echo '<td>' . $totalHours / 60 . '</td>';
            echo '</tr>';
        }
        echo '</table>';
    }
    ?>
</div>