<?php

/**
 * This is the model class for table "mileage_reference_for_creation".
 *
 * The followings are the available columns in table 'mileage_reference_for_creation':
 * @property integer $from_client_id
 * @property integer $from_property_number
 * @property string $from_postcode
 * @property integer $to_client_id
 * @property integer $to_property_number
 * @property string $to_postcode
 */
class Mileage_Reference_For_Creation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Mileage_Reference_For_Creation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'mileage_reference_for_creation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('from_client_id, from_postcode, to_client_id, to_postcode', 'required'),
			array('from_client_id, from_property_number, to_client_id, to_property_number', 'numerical', 'integerOnly'=>true),
			array('from_postcode, to_postcode', 'length', 'max'=>7),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('from_client_id, from_property_number, from_postcode, to_client_id, to_property_number, to_postcode', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'from_client_id' => 'From Client',
			'from_property_number' => 'From Property Number',
			'from_postcode' => 'From Postcode',
			'to_client_id' => 'To Client',
			'to_property_number' => 'To Property Number',
			'to_postcode' => 'To Postcode',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('from_client_id',$this->from_client_id);
		$criteria->compare('from_property_number',$this->from_property_number);
		$criteria->compare('from_postcode',$this->from_postcode,true);
		$criteria->compare('to_client_id',$this->to_client_id);
		$criteria->compare('to_property_number',$this->to_property_number);
		$criteria->compare('to_postcode',$this->to_postcode,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}